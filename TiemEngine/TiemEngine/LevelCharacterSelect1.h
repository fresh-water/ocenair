#pragma once
#include "Level.h"

class LevelCharacterSelect1 : public Level
{
private:
	vector<DrawableObject*> objectsList;
	vector<DrawableObject*> UIList;
	Camera* maincamera;
	UIObject* CharacterIconUI[6];
	UIObject* CharacterSpriteUI[6];
	UIObject* TitleUI;
	UIObject* NextUI;
	UIObject* BackUI;
	int mainstate;
	int characterCount;
	vector<int> selectedcharacter;
public:
	virtual void LevelLoad();
	virtual void LevelInit();
	virtual void LevelUpdate();
	virtual void LevelDraw();
	virtual void LevelFree();
	virtual void LevelUnload();

	virtual void HandleKey(char key);
	virtual void HandleMouse(int type, int x, int y);

	virtual void HandleMouseLeft(int x, int y);
	virtual void HandleMouseRight(int x, int y);
	virtual void HandleMouseMove(int x, int y);
	virtual void HandleMouseScroll(int amount);
};

