#include "Camera.h"

Camera::Camera()
{

}

void Camera::Init(int width, int height)
{
	minHeight = height - 1000;
	maxHeight = height + 1000;
	minWidth = width - 1000;
	maxWidth = width + 1000;
	maxzoompercent = 125;
	minzoompercent = 80;
	zoompercent = 100;
	winWidth = width;
	winHeight = height;
	Left = -width / 2;
	Right = width / 2;
	Down = -height / 2;
	Up = height / 2;
	center.x = 0;
	center.y = 0;
	GameEngine::GetInstance()->SetDrawArea(Left, Right, Down, Up);
}

void Camera::Init(int width, int height, float minwidth, float maxwidth, float minheight, float maxheight)
{
	minHeight = minheight;
	maxHeight = maxheight;
	minWidth = minwidth;
	maxWidth = maxwidth;
	maxzoompercent = 125;
	minzoompercent = 80;
	zoompercent = 100;
	winWidth = width;
	winHeight = height;
	Left = -width/2;
	Right = width / 2;
	Down = -height/2;
	Up = height / 2;
	center.x = 0;
	center.y = 0;
	GameEngine::GetInstance()->SetDrawArea(Left, Right, Down, Up);
}
void Camera::Update()
{
	if (isfollow)
	{
		SetCameraPosition(unitFollow->GetPosition().x, unitFollow->GetPosition().y);
	}
}
void Camera::CameraDrawArea()
{
	if (!isfollow)
	{
		CameraAdjust();
	}

	float percent = zoompercent / 100.0f;
	Left = center.x - ((winWidth / 2) / percent);
	Right = center.x + ((winWidth / 2) / percent);
	Down = center.y - ((winHeight / 2) / percent);
	Up = center.y + ((winHeight / 2) / percent);

	GameEngine::GetInstance()->SetDrawArea(Left, Right, Down, Up);
}
void Camera::CameraMove(float up, float right)
{
	if (Left + right >= minWidth && Right + right <= maxWidth && Down + up >= minHeight && Up + up <= maxHeight)
	{
		center.x += right;
		center.y += up;
	}
	else if (Left + right < minWidth)
	{
		center.x = minWidth + abs(Left - center.x);
	}
	else if (Right + right > maxWidth)
	{
		center.x = maxWidth - abs(Right - center.x);
	}
	else if (Down + up < minHeight)
	{
		center.y = minHeight + abs(Down - center.y);
	}
	else if (Up + up > maxHeight)
	{
		center.y = maxHeight - abs(Up - center.y);
	}
}
void Camera::SetCameraPosition(float x, float y)
{
	center.x = x;
	center.y = y;
}
void Camera::CameraSetZoom(float percent)
{
	if (percent >= minzoompercent && percent <= maxzoompercent)
	{
		zoompercent = percent;
	}
}
void Camera::CameraAdjust()
{
	if (Left < minWidth)
	{
		SetCameraPosition(minWidth + abs(Left - center.x), center.y);
	}
	if (Right > maxWidth)
	{
		SetCameraPosition(maxWidth - abs(Right - center.x), center.y);
	}
	if (Down < minHeight)
	{
		SetCameraPosition(center.x, minHeight + abs(Down - center.y));
	}
	if (Up > maxHeight)
	{
		SetCameraPosition(center.x, maxHeight - abs(Up - center.y));
	}

	float percent = zoompercent / 100.0f;
	Left = center.x - ((winWidth / 2) / percent);
	Right = center.x + ((winWidth / 2) / percent);
	Down = center.y - ((winHeight / 2) / percent);
	Up = center.y + ((winHeight / 2) / percent);
}

void Camera::SetFollowUnit(Unit* unit)
{
	this->unitFollow = unit;
}
void Camera::SetIsFollow(bool fact)
{
	this->isfollow = fact;
}
bool Camera::GetIsFollow()
{
	return this->isfollow;
}
void Camera::SetEnable(bool fact)
{
	this->enable = fact;
}
bool Camera::GetEnable()
{
	return enable;
}

glm::vec3 Camera::GetCenter()
{
	return center;
}
float Camera::GetZoomPercent()
{
	return zoompercent;
}
float Camera::GetZoomSize()
{
	return zoompercent/100.0f;
}
float Camera::GetLeft()
{
	return Left;
}
float Camera::GetRight()
{
	return Right;
}
float Camera::GetDown()
{
	return Down;
}
float Camera::GetUp()
{
	return Up;
}
float Camera::GetWindowWidth()
{
	return winWidth;
}
float Camera::GetWindowHeight()
{
	return winHeight;
}