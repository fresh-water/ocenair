#include "LevelMainMenu.h"

void Start(UIObject* button)
{
	cout << "Start" << endl;
	GameData::GetInstance()->gGameStateNext = GameState::GS_LEVELSELECT;
}

void Credit(UIObject* button)
{
	cout << "Credit" << endl;
}

void Option(UIObject* button)
{
	cout << "Option" << endl;
}

void Exit(UIObject* button)
{
	cout << "Exit" << endl;
	GameData::GetInstance()->gGameStateNext = GameState::GS_QUIT;
}

void LevelMainMenu::LevelLoad()
{
	SquareMeshVbo* square = new SquareMeshVbo();
	square->LoadData();
	GameEngine::GetInstance()->AddMesh(SquareMeshVbo::MESH_NAME, square);
}

void LevelMainMenu::LevelInit()
{
	GameDataStorage::GetInstance()->loadSaveFile();

	maincamera = new Camera;
	maincamera->Init(GameEngine::GetInstance()->GetWindowWidth(), GameEngine::GetInstance()->GetWindowHeight());
	maincamera->SetEnable(false);

	UIObject * bg = new UIObject();
	bg->SetMode(1);
	bg->SetDraw(true);
	bg->SetSize(2, -2);
	bg->SetPosition(glm::vec3(0.0f, 0.0f, 0.0f));
	bg->SetClickable(false);
	if (GameDataStorage::GetInstance()->getUnlocklevelnumber() < 12)
	{
		bg->setSprite("../Resource/Texture/NewTitle1.png", 1, 6, 0, 0);
		bg->SetAnimationLoop(0, 0, 6, 100);
	}
	else
	{
		bg->setSprite("../Resource/Texture/NewTitle2.png", 2, 7, 0, 0);
		bg->SetAnimationLoop(0, 0, 14, 100);
	}

	UIList.push_back(bg);

	for (int i=0;i<5;i++)
	{
		MainMenuUI[i] = new UIObject();
		MainMenuUI[i]->SetMode(1);
		MainMenuUI[i]->SetClickable(true);
		MainMenuUI[i]->SetDraw(true);
	}

	MainMenuUI[0]->SetSize(0.8, -1.4);
	MainMenuUI[0]->SetPosition(glm::vec3(-0.6f, 0.53f, 0.0f));
	//MainMenuUI[0]->SetSize(0.9, -0.4);
	//MainMenuUI[0]->SetPosition(glm::vec3(-0.5f, 0.53f, 0.0f));
	MainMenuUI[0]->SetClickable(false);
	MainMenuUI[0]->SetTexture("../Resource/Texture/UITitle2.png");

	MainMenuUI[1]->SetSize(0.2, -0.17);
	MainMenuUI[1]->SetPosition(glm::vec3(-0.735f, -0.1f, 0.0f));
	//MainMenuUI[1]->SetSize(1.5, -0.25);
	//MainMenuUI[1]->SetActiveFn(Start);
	MainMenuUI[1]->setSprite("../Resource/Texture/UIPlay.png", 2, 1, 0, 0);
	MainMenuUI[1]->SetClickOnAnim(true);
	MainMenuUI[1]->setButtontype(UI_START);

	MainMenuUI[2]->SetSize(0.27, -0.17);
	MainMenuUI[2]->SetPosition(glm::vec3(-0.732f, -0.1f, 0.0f));
	MainMenuUI[2]->SetActiveFn(Credit);
	MainMenuUI[2]->SetDraw(false);
	MainMenuUI[2]->SetTexture("../Resource/Texture/UICredit.png");

	MainMenuUI[3]->SetSize(0.27, -0.17);
	MainMenuUI[3]->SetPosition(glm::vec3(-0.7f, -0.4f, 0.0f));
	MainMenuUI[3]->setSprite("../Resource/Texture/UIOption1.png", 2, 1, 0, 0);
	MainMenuUI[3]->SetClickOnAnim(true);
	MainMenuUI[3]->setButtontype(UI_OPTION);

	MainMenuUI[4]->SetSize(0.2, -0.17);
	MainMenuUI[4]->SetPosition(glm::vec3(-0.735f, -0.7f, 0.0f));
	MainMenuUI[4]->setSprite("../Resource/Texture/UIExit3.png", 2, 1, 0, 0);
	MainMenuUI[4]->SetClickOnAnim(true);
	MainMenuUI[4]->SetActiveFn(Exit);
	MainMenuUI[4]->setButtontype(UI_NORMAL);
	
	for (int i = 0; i < 5; i++)
	{
		UIList.push_back(MainMenuUI[i]);
	}

	settingui.Init();
	for (int i = 0; i < 19; i++) {
		UIList.push_back(settingui.getAudioSUI(i));
	}
	for (int i = 0; i < 5; i++) {
		UIList.push_back(settingui.getTutorialUI(i));
	}
	GameEngine::GetInstance()->playMusic("../Resource/Sound/Background.mp3");
}

void LevelMainMenu::LevelUpdate()
{
	maincamera->CameraDrawArea();
	int deltaTime = GameEngine::GetInstance()->GetDeltaTime();
	for (DrawableObject* obj : objectsList) {
		obj->Update(deltaTime);
	}
}

void LevelMainMenu::LevelDraw()
{
	GameEngine::GetInstance()->Render(objectsList);
	GameEngine::GetInstance()->GetRenderer()->RenderUI(UIList);
}

void LevelMainMenu::LevelFree()
{
	for (DrawableObject* obj : objectsList) {
		if (obj != nullptr)
		{
			delete obj;
		}
	}
	objectsList.clear();
	for (DrawableObject* obj : UIList) {
		delete obj;
	}
	UIList.clear();
}

void LevelMainMenu::LevelUnload()
{
	GameEngine::GetInstance()->ClearMesh();
}

void LevelMainMenu::HandleKey(char key)
{
	switch (key)
	{
	case 'a':
		break;
	case 'd':
		break;
	case 'w':
		break;
	case 'I':
		break;
	case 'O':
		break;

	case 'E': GameData::GetInstance()->gGameStateNext = GameState::GS_QUIT; ; break;
	case 'r': GameData::GetInstance()->gGameStateNext = GameState::GS_RESTART; ; break;
	case 'e': GameData::GetInstance()->gGameStateNext = GameState::GS_LOSE; ; break;		//GS_WIN/GS_LOSE scene loading
	case 's': GameData::GetInstance()->gGameStateNext = GameState::GS_MAINMENU; ; break;
	case 'S':
		break;
	}
}

void LevelMainMenu::HandleMouse(int type, int x, int y)
{
	int scrolling = x;
	int realX, realY;
	realX = GameEngine::GetInstance()->ConvertX(x);
	realY = GameEngine::GetInstance()->ConvertY(y);

	if (type == MOUSEMOVE)//move
	{
		HandleMouseMove(realX, realY);
	}
	else if (type == MOUSELEFT)//left
	{
		HandleMouseLeft(realX, realY);
	}
	else if (type == MOUSERIGHT)//right
	{
		HandleMouseRight(realX, realY);
	}
	else if (type == MOUSESCROLL)//scroll
	{
		HandleMouseScroll(scrolling);
	}
}

void LevelMainMenu::HandleMouseLeft(int x, int y)
{
	for (int i = UIList.size()-1; i >= 0; i--)
	{
		UIObject* ui1 = dynamic_cast<UIObject*>(UIList[i]);
		if (ui1 != nullptr)
		{
			if (ui1->ClickOn(x, y))
			{
				if (ui1->GetClickable())
				{
					GameEngine::GetInstance()->playSound("../Resource/Sound/Click Laser (V-ktor).wav");
				}
				if (ui1->getButtontype() == UI_BACK_OPTION){
					settingui.closeAudioSUI();
				}	
				else if (ui1->getButtontype() == UI_OPTION) {
					settingui.showAudioSUI();
				}
				else if (ui1->getButtontype() == UI_SPEED_1){
					settingui.speedSelect(1);
				}
				else if (ui1->getButtontype() == UI_SPEED_2){
					settingui.speedSelect(2);
				}
				else if (ui1->getButtontype() == UI_SPEED_3){
					settingui.speedSelect(3);
				}
				else if (ui1->getButtontype() == UI_SKIP_ON) {
					settingui.SkipActivate(true);
				}
				else if (ui1->getButtontype() == UI_SKIP_OFF) {
					settingui.SkipActivate(false);
				}
				else if (ui1->getButtontype() == UI_START) {

				}
				else if (ui1->getButtontype() == UI_RESET) {
					settingui.showResaveMenu();
				}
				else if (ui1->getButtontype() == UI_BACK_RESAVE){
					settingui.closeResaveMenu();
				}
				else if (ui1->getButtontype() == UI_TUTORIAL) {
					settingui.showTutorialUI(settingui.getCurrentTutorial());
				}
				else if (ui1->getButtontype() == UI_BACK_TURORIAL) {
					settingui.closeTutorialUI();
				}
				else if (ui1->getButtontype() == UI_ARROW_LEFT_TURORIAL) {
					settingui.showTutorialUI(settingui.getCurrentTutorial()-1);
				}
				else if (ui1->getButtontype() == UI_ARROW_RIGHT_TURORIAL) {
					settingui.showTutorialUI(settingui.getCurrentTutorial()+1);
				}
				return;
			}
		}
	}
}
void LevelMainMenu::HandleMouseRight(int x, int y)
{
	
}
void LevelMainMenu::HandleMouseMove(int x, int y)
{
	bool hitbutton = false;
	for (int i = UIList.size() - 1; i >= 0; i--)
	{
		UIObject* ui1 = dynamic_cast<UIObject*>(UIList[i]);
		if (ui1 != nullptr)
		{
			if (ui1->OnMouseStay(x, y))
			{
				hitbutton = true;
				if (prevbutton != ui1)
				{
					prevbutton = ui1;
					GameEngine::GetInstance()->playSound("../Resource/Sound/Click.mp3");
				}
				break;
			}
		}
	}
	if (hitbutton == false)
	{
		prevbutton = nullptr;
	}
}
void LevelMainMenu::HandleMouseScroll(int amount)
{
	
}

