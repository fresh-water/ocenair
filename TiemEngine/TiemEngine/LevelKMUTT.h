#pragma once
#include "Level.h"

class LevelKMUTT : public Level
{
private:
	vector<DrawableObject*> objectsList;
	vector<DrawableObject*> UIList;
	Camera* maincamera;
	UIObject* cutsceneImage[1];
	int mainstate;
	int currindex;
	int numofimage;
	float curralpha = 0.0f;
public:
	virtual void LevelLoad();
	virtual void LevelInit();
	virtual void LevelUpdate();
	virtual void LevelDraw();
	virtual void LevelFree();
	virtual void LevelUnload();

	virtual void HandleKey(char key);
	virtual void HandleMouse(int type, int x, int y);

	virtual void HandleMouseLeft(int x, int y);
	virtual void HandleMouseRight(int x, int y);
	virtual void HandleMouseMove(int x, int y);
	virtual void HandleMouseScroll(int amount);
};
