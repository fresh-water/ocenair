#ifndef UNIT_H
#define UNIT_H
#include "GameObject.h"
#include "SpriteObject.h"
#include "Buff.h"
#include <string>
#include <iostream>
#include <fstream>
using namespace std;

class Unit : public GameObject {
private:
	glm::vec2 boardpos;
	int hp, atk, move, eva, def,row,col;
	int atkdis = 0;
	int cd;
	int maxhp;
	string name;
	string texture;
	string attacktexture;
	vector<Buff> buff;
	vector<glm::vec2> walkinganimpath;
	GameObject* miniHpbar[2];
public:
	virtual void Init(string name, int hp, int maxhp, int atk, int atkdis, int move, int x, int y, int eva, int def, int cd, string tex);

	virtual vector<glm::vec2> ActiveSkill(int** typearr, int sizex, int sizey);
	virtual string getName();
	virtual void setName(string name);
	virtual int getHp();
	virtual void setHp(int hp);
	virtual int getAtk();
	virtual void setAtk(int atk);
	virtual int getDef();
	virtual void setDef(int def);
	virtual int getEva();
	virtual void setEva(int eva);
	virtual int getAtkdis();
	virtual void setAtkdis(int atkdis);
	virtual void setMove(int move);
	virtual int getMove();
	virtual int getRow();
	virtual void setRow(int row);
	virtual int getCol();
	virtual void setCol(int col);
	virtual int getMaxHp();
	virtual void setCD(int cd);
	virtual int getCD();
	virtual string getatktexture();
	virtual void setatktexture(string tex);

	virtual void updateMiniHpbar();
	virtual GameObject* getminiHpbar(int index);

	virtual void setBoardPosition(int posx, int posy);
	virtual glm::vec2 getBoardPosition();
	virtual void PassTurn();
	virtual string getTexture();

	virtual int getBuffEffect(int type);
	virtual void addBuff(int type, int effect, int duration);
	virtual vector<Buff> getBuffVector();

	virtual void SetColor(float r, float g, float b);
	virtual void SetMode(int mode);
	virtual void SetPosition(glm::vec3 newPosition);

	virtual void SetTexture(string path);

	virtual void SetAnimation(int anim);

	virtual void setWalkingAnimPath(vector<glm::vec2> vec);
	virtual vector<glm::vec2> getWalkingAnimPath();

	Unit(string name, int hp, int maxhp, int atk, int atkdis, int move, int x, int y, int eva, int def, int cd, string tex);
	Unit();
};
#endif
