#include "LevelTest.h"

void LevelTest::LevelLoad()
{
	SquareMeshVbo* square = new SquareMeshVbo();
	square->LoadData();
	GameEngine::GetInstance()->AddMesh(SquareMeshVbo::MESH_NAME, square);
}

void LevelTest::LevelInit()
{
	mainboard = new Board();

	mainboard->BoardReadfile("../Resource/GameData/testboard1.txt", &BOARDSIZEROW, &BOARDSIZECOLUMN);

	maincamera = new Camera;
	maincamera->Init(GameEngine::GetInstance()->GetWindowWidth(), GameEngine::GetInstance()->GetWindowHeight(),
		-GameEngine::GetInstance()->GetWindowWidth() / 2 - CAMERAOFFSET, CAMERAOFFSET + BOARDSIZEROW * 100 - GameEngine::GetInstance()->GetWindowWidth() / 2,
		-GameEngine::GetInstance()->GetWindowHeight() / 2 - CAMERAOFFSET, CAMERAOFFSET + BOARDSIZECOLUMN * 100 - GameEngine::GetInstance()->GetWindowHeight() / 2);

	mainboard->SetBoardDefault();

	mainboard->SetMapSprite();

	//Unit* unit2 = new Unit("ANY", 10, 10, 1, 2, 2, 0, 0, 0, 0, 0, "../Resource/Texture/AshTest.png");
	//unit2->setPath("../Resource/Texture/AshTest.png", 2, 4);
	//unit2->SetAnimationLoop(1, 0, 4, 100);
	//mainboard->AddUnit(unit2, 5, 3);

	//mainboard->SummonallUnit();

	mainboard->PushBackallobj(&objectsList, &UIList);

	mainboard->StartPlayerTurn();
	{
		//Character* unit1 = new Character();
		//unit1->readCharacterData("../Resource/GameData/Character Status.txt",2);
		//unit1->SetTexture(unit1->getTexture());
		///*Character* unit1 = new Character("Potae",10,10,2,1,2,0,0,0,0,0,"dw",10,3,5);
		//unit1->SetTexture("../Resource/Texture/character2.png");*/
		//mainboard->AddUnit(unit1,3,1);

		//Submarine* sub1 = new Submarine();
		//sub1->readCharacterData("../Resource/GameData/Character Status.txt", 1);
		//sub1->SetTexture("../Resource/Texture/sub1.png");
		///*Character* unit1 = new Character("Potae",10,10,2,1,2,0,0,0,0,0,"dw",10,3,5);
		//unit1->SetTexture("../Resource/Texture/character2.png");*/
		//mainboard->AddUnit(sub1, 5, 1);

		//Unit* unit2 = new Unit("ANY",10,10,1,2, 2,0,0,0,0,0,"tex");
		//unit2->setPath("../Resource/Texture/image0.png", 8, 10);
		//unit2->SetAnimationLoop(4, 0, 10, 50);
		//mainboard->AddUnit(unit2, 1, 3);

		//Character* unit2 = new Character();
		//unit2->readCharacterData("../Resource/GameData/Character Status.txt", 3);
		//unit2->SetTexture(unit2->getTexture());
		//mainboard->AddUnit(unit2, 1, 3);

		//Enemy* unit3 = new Enemy();
		//unit3->readCharacterData("../Resource/GameData/Enemy Status.txt",1);
		//unit3->SetTexture(unit3->getTexture());
		///*Enemy* unit3 = new Enemy("��Ҵء",20,20,1,1,2,0,0,0,0,0,"ANYTEX");
		//unit3->SetTexture("../Resource/Texture/enemy1.png");*/
		//mainboard->AddUnit(unit3, 10, 1);

		//cout << "Unit1pos = ("<< unit1->GetPosition().x << "," << unit1->GetPosition().y << ")" << endl;



		/*UIObject* ui1 = new UIObject();
		ui1->SetSize(0.25, 0.20);
		ui1->SetPosition(glm::vec3(-0.75f,-0.75f,0.0f));
		ui1->SetTexture("../Resource/Texture/Infotemp.png");
		ui1->SetMode(1);
		ui1->SetClickable(false);
		ui1->SetDraw(true);
		UIList.push_back(ui1);*/
	}
	//cout << "Init Level" << endl;
}

void LevelTest::LevelUpdate()
{
	mainboard->BoardUpdate(maincamera);
	maincamera->CameraDrawArea();
	int deltaTime = GameEngine::GetInstance()->GetDeltaTime();
	for (DrawableObject* obj : objectsList) {
		obj->Update(deltaTime);
	}
	if (mainboard->GetMainState() == BS_ANIM || mainstate == LS_SELECTED || mainboard->GetMainState() == BS_PAUSE)
	{
		maincamera->SetEnable(false);
	}
	else
	{
		maincamera->SetEnable(true);
	}
	//cout << "Update Level" << endl;
}

void LevelTest::LevelDraw()
{
	GameEngine::GetInstance()->Render(objectsList);
	GameEngine::GetInstance()->GetRenderer()->RenderUI(UIList);
	//cout << "Draw Level" << endl;
}

void LevelTest::LevelFree()
{
	for (DrawableObject* obj : objectsList) {
		if (obj != nullptr)
		{
			delete obj;
		}
	}
	objectsList.clear();
	for (DrawableObject* obj : UIList) {
		delete obj;
	}
	UIList.clear();
	//cout << "Free Level" << endl;
}

void LevelTest::LevelUnload()
{
	GameEngine::GetInstance()->ClearMesh();
	//cout << "Unload Level" << endl;
}

int LevelTest::WorldtoBoardX(float x)
{
	int result;
	float fresult;
	fresult = (float)(x + (maincamera->GetCenter().x * maincamera->GetZoomSize()) + ((maincamera->GetWindowWidth() * maincamera->GetZoomSize()) / 2.0f)) / maincamera->GetZoomSize();
	fresult /= 100.0f;
	result = floorf(fresult);
	return result;
}
int LevelTest::WorldtoBoardY(float y)
{
	int result;
	float fresult;
	fresult = (float)(y + (maincamera->GetCenter().y * maincamera->GetZoomSize()) + ((maincamera->GetWindowHeight() * maincamera->GetZoomSize()) / 2.0f)) / maincamera->GetZoomSize();
	fresult /= 100.0f;
	result = floorf(fresult);
	return result;
}

void LevelTest::HandleKey(char key)
{
	switch (key)
	{
	case 'a':
		for (int i = 0;i < mainboard->GetAmountUnit(); i++)
		{
			mainboard->GetUnitVector(i)->SetAnimation(IDLE);
		}
		break;
	case 'd':
		for (int i = 0; i < mainboard->GetAmountUnit(); i++)
		{
			mainboard->GetUnitVector(i)->SetAnimation(WALK);
		}
		break;
	case 'w': GameData::GetInstance()->gGameStateNext = GameState::GS_LEVEL3; break;
	case 'I':
		maincamera->SetCameraPosition(0, 0);
		break;
	case 'O':
		maincamera->CameraSetZoom(100);
		break;

	case 'E': 
		if (mainboard->GetBoardstate() != BS_PAUSE)
		{
			mainboard->OpenPauseUI();
			maincamera->SetEnable(false);
			mainboard->GetBoardstate() = BS_PAUSE;
		}
		else
		{
			mainboard->ClosePauseUI();
			mainboard->ClickHandle(MOUSERIGHT, 0, 0, mainstate, maincamera);
		}
		break;
	case 'r': GameData::GetInstance()->gGameStateNext = GameState::GS_RESTART; ; break;
	case 'e': GameData::GetInstance()->gGameStateNext = GameState::GS_WIN; ; break;		//GS_WIN/GS_LOSE scene loading
	case 's': GameData::GetInstance()->gGameStateNext = GameState::GS_MAINMENU; ; break;
	case 'S': 
		mainboard->StartEnemyTurn();
		break;
	}
}

void LevelTest::HandleMouse(int type, int x, int y)
{
	int scrolling = x;
	int realX, realY;
	realX = GameEngine::GetInstance()->ConvertX(x);
	realY = GameEngine::GetInstance()->ConvertY(y);

	if (type == MOUSEMOVE)//move
	{
		HandleMouseMove(realX, realY);
	}
	else if (type == MOUSELEFT)//left
	{
		HandleMouseLeft(realX, realY);
	}
	else if (type == MOUSERIGHT)//right
	{
		HandleMouseRight(realX, realY);
	}
	else if (type == MOUSESCROLL)//scroll
	{
		HandleMouseScroll(scrolling);
	}

}

void LevelTest::HandleMouseLeft(int x, int y)
{
	float realX = (float)x;
	float realY = (float)y;

	//cout << "(x,y) = (" << x << "," << y << ")" << endl;

	int beforeLevelState = mainstate;
	int beforeBoardState = mainboard->GetBoardstate();

	for (int i = UIList.size() - 1; i >= 0; i--)
	{
		UIObject* ui1 = dynamic_cast<UIObject*>(UIList[i]);
		if (ui1 != nullptr)
		{
			if (ui1->ClickOn(x, y,mainstate,mainboard->GetBoardstate()))
			{
				if (mainstate == LS_SELECTED)
				{
					maincamera->SetEnable(false);
				}
				else if (mainstate == LS_NORMAL)
				{
					maincamera->SetEnable(true);
				}
				else if (mainstate == LS_UNPAUSING)
				{
					mainboard->ClosePauseUI();
					mainboard->ClickHandle(MOUSERIGHT, 0, 0, mainstate, maincamera);
				}
				return;
			}
		}
	}
	
	mainboard->CloseMenuUI();
	maincamera->SetEnable(true);

	int BX, BY;//board X,Y

	BX = WorldtoBoardX(realX);
	BY = WorldtoBoardY(realY);

	if (BX >= 0 && BX < BOARDSIZEROW && BY >= 0 && BY < BOARDSIZECOLUMN)
	{
		mainboard->ClickHandle(MOUSELEFT, BX, BY,mainstate,maincamera);
	}
}
void LevelTest::HandleMouseRight(int x, int y)
{
	mainboard->ClickHandle(MOUSERIGHT, x, y, mainstate, maincamera);
}
void LevelTest::HandleMouseMove(int x, int y)
{
	for (int i = UIList.size() - 1; i >= 0; i--)
	{
		UIObject* ui1 = dynamic_cast<UIObject*>(UIList[i]);
		if (ui1 != nullptr)
		{
			if (ui1->OnMouseStay(x, y))
			{

			}
		}
	}

	if (y > (maincamera->GetWindowHeight() / 2.0f - 50.0f))
	{
		//cout << "up" << endl;
		maincamera->CameraMove(CAMERASPEED, 0);
	}
	else if (y < -(maincamera->GetWindowHeight() / 2.0f - 50.0f))
	{
		//cout << "down" << endl;
		maincamera->CameraMove(-CAMERASPEED, 0);
	}
	if (x > (maincamera->GetWindowWidth() / 2.0f - 50.0f))
	{
		//cout << "right" << endl;
		maincamera->CameraMove(0, CAMERASPEED);
	}
	else if (x < -(maincamera->GetWindowWidth() / 2.0f - 50.0f))
	{
		//cout << "left" << endl;
		maincamera->CameraMove(0, -CAMERASPEED);
	}

	int BX, BY;//board X,Y

	BX = WorldtoBoardX(x);
	BY = WorldtoBoardY(y);

	if (BX >= 0 && BX < BOARDSIZEROW && BY >= 0 && BY < BOARDSIZECOLUMN)
	{
		mainboard->ClickHandle(MOUSEMOVE, BX, BY, mainstate, maincamera);
	}
}
void LevelTest::HandleMouseScroll(int amount)
{
	if (amount > 0) //scroll up
	{
		maincamera->CameraSetZoom(maincamera->GetZoomPercent() - 10.0f);
	}
	else if (amount < 0)//scroll down
	{
		maincamera->CameraSetZoom(maincamera->GetZoomPercent() + 10.0f);
	}
}

