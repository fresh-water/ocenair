#include "LevelBoard.h"
#include "UIsetting.h"

void LevelBoard::LevelLoad()
{
	SquareMeshVbo* square = new SquareMeshVbo();
	square->LoadData();
	GameEngine::GetInstance()->AddMesh(SquareMeshVbo::MESH_NAME, square);
}
void LevelBoard::LevelInit()
{
	mainboard = new Board();

	cout << "Load Map : " << GameDataStorage::GetInstance()->getMapFileName(GameDataStorage::GetInstance()->getCurrlevelnumber()) << endl;

	mainboard->BoardReadfile(GameDataStorage::GetInstance()->getMapFileName(GameDataStorage::GetInstance()->getCurrlevelnumber()), &BOARDSIZEROW, &BOARDSIZECOLUMN);

	maincamera = new Camera;
	maincamera->Init(GameEngine::GetInstance()->GetWindowWidth(), GameEngine::GetInstance()->GetWindowHeight(),
		-GameEngine::GetInstance()->GetWindowWidth() / 2 - CAMERAOFFSET, CAMERAOFFSET + BOARDSIZEROW * 100 - GameEngine::GetInstance()->GetWindowWidth() / 2,
		-GameEngine::GetInstance()->GetWindowHeight() / 2 - CAMERAOFFSET, CAMERAOFFSET + BOARDSIZECOLUMN * 100 - GameEngine::GetInstance()->GetWindowHeight() / 2);

	mainboard->SetBoardDefault();

	mainboard->SetMapSprite();

	mainboard->setCamera(maincamera);

	mainboard->StartGame();

	mainboard->PushBackallobj(&objectsList, &UIList);

	settingui.Init();
	for (int i = 0; i < 19; i++) {
		UIList.push_back(settingui.getAudioSUI(i));
	}
	for (int i = 0; i < 5; i++) {
		UIList.push_back(settingui.getTutorialUI(i));
	}

	if (GameDataStorage::GetInstance()->getCurrlevelnumber() == 1)
	{
		settingui.showTutorialUI(1);
	}
	else if (GameDataStorage::GetInstance()->getCurrlevelnumber() == 2)
	{
		settingui.showTutorialUI(2);
	}
	else if (GameDataStorage::GetInstance()->getCurrlevelnumber() == 4)
	{
		settingui.showTutorialUI(3);
	}

	if (GameDataStorage::GetInstance()->getCurrlevelnumber() <= 6)
	{
		GameEngine::GetInstance()->playMusic("../Resource/Sound/Background.mp3");
	}
	else if(GameDataStorage::GetInstance()->getCurrlevelnumber() <= 12)
	{
		GameEngine::GetInstance()->playMusic("../Resource/Sound/Biom2.mp3");
	}
	else if (GameDataStorage::GetInstance()->getCurrlevelnumber() < 16)
	{
		GameEngine::GetInstance()->playMusic("../Resource/Sound/Biom3.mp3");
	}
	else if (GameDataStorage::GetInstance()->getCurrlevelnumber() == 16)
	{
		GameEngine::GetInstance()->playMusic("../Resource/Sound/biom3 Boss.mp3");
	}

	mainboard->StartPlayerTurn();
}
void LevelBoard::LevelUpdate()
{
	mainboard->BoardUpdate(maincamera);
	maincamera->Update();
	maincamera->CameraDrawArea();
	int deltaTime = GameEngine::GetInstance()->GetDeltaTime();
	for (DrawableObject* obj : objectsList) {
		obj->Update(deltaTime);
	}
	for (DrawableObject* obj : UIList) {
		obj->Update(deltaTime);
	}
}
void LevelBoard::LevelDraw()
{
	GameEngine::GetInstance()->Render(objectsList);
	GameEngine::GetInstance()->GetRenderer()->RenderUI(UIList);
}
void LevelBoard::LevelFree()
{
	for (DrawableObject* obj : objectsList) {
		if (obj != nullptr)
		{
			delete obj;
		}
	}
	objectsList.clear();
	for (DrawableObject* obj : UIList) {
		delete obj;
	}
	UIList.clear();
}
void LevelBoard::LevelUnload()
{
	GameEngine::GetInstance()->ClearMesh();
}

int LevelBoard::WorldtoBoardX(float x)
{
	int result;
	float fresult;
	fresult = (float)(x + (maincamera->GetCenter().x * maincamera->GetZoomSize()) + ((maincamera->GetWindowWidth() * maincamera->GetZoomSize()) / 2.0f)) / maincamera->GetZoomSize();
	fresult /= 100.0f;
	result = floorf(fresult);
	return result;
}
int LevelBoard::WorldtoBoardY(float y)
{
	int result;
	float fresult;
	fresult = (float)(y + (maincamera->GetCenter().y * maincamera->GetZoomSize()) + ((maincamera->GetWindowHeight() * maincamera->GetZoomSize()) / 2.0f)) / maincamera->GetZoomSize();
	fresult /= 100.0f;
	result = floorf(fresult);
	return result;
}

void LevelBoard::HandleKey(char key)
{
	if (mainboard->GetBoardstate() == BS_ENEMYANIM || mainboard->GetBoardstate() == BS_ENEMY || mainboard->GetMainState() == BS_ANIM)
	{
		return;
	}
	switch (key)
	{
	case 'a':
		maincamera->SetFollowUnit(mainboard->GetUnitVector(10));
		maincamera->SetIsFollow(true);
		break;
	case 'd':
		maincamera->SetIsFollow(false);
		break;
	case 'w':
		if (GameDataStorage::GetInstance()->getCurrlevelnumber() > 16)
		{
			GameDataStorage::GetInstance()->setNextlevelnumber(0);
		}
		else
		{
			GameDataStorage::GetInstance()->setNextlevelnumber(GameDataStorage::GetInstance()->getCurrlevelnumber() + 1);
		}
		GameData::GetInstance()->gGameStateNext = GameState::GS_CHARACTERSELECT;
		break;
	case 's':
		if (GameDataStorage::GetInstance()->getCurrlevelnumber() < 0)
		{
			GameDataStorage::GetInstance()->setNextlevelnumber(16);
		}
		else
		{
			GameDataStorage::GetInstance()->setNextlevelnumber(GameDataStorage::GetInstance()->getCurrlevelnumber() - 1);
		}
		GameData::GetInstance()->gGameStateNext = GameState::GS_CHARACTERSELECT;
		break;
	case 'v':
		if (GameDataStorage::GetInstance()->getOpenvision())
		{
			GameDataStorage::GetInstance()->setOpenvision(false);
		}
		else
		{
			GameDataStorage::GetInstance()->setOpenvision(true);
		}
		mainboard->VisionUpdate();
		mainboard->ResetBoard();
		break;
	case 'E':
		if (mainboard->GetBoardstate() != BS_PAUSE)
		{
			mainboard->OpenPauseUI();
			mainboard->GetBoardstate() = BS_PAUSE;
		}
		else
		{
			mainboard->ClosePauseUI();
			mainboard->ResetBoard();
		}
		break;
	case 'r': GameData::GetInstance()->gGameStateNext = GameState::GS_RESTART; break;
	case 'e': GameData::GetInstance()->gGameStateNext = GameState::GS_WIN; break;		//GS_WIN/GS_LOSE scene loading
	case 'S':
		if (mainboard->GetMainState() != BS_ENEMY && mainboard->GetMainState() != BS_ENEMYANIM && mainboard->GetMainState() != BS_ENEMYATKANIM)
		{
			mainboard->StartEnemyTurn();
		}
		break;
	}
}
void LevelBoard::HandleMouse(int type, int x, int y)
{
	if (mainboard->GetBoardstate() == BS_ANIM || mainboard->GetBoardstate() == BS_ENEMY ||
		mainboard->GetBoardstate() == BS_ENEMYANIM || mainboard->GetBoardstate() == BS_ENEMYATKANIM ||
		mainboard->GetBoardstate() == BS_ATKANIM)
	{
		return;
	}

	int scrolling = x;
	int realX, realY;
	realX = GameEngine::GetInstance()->ConvertX(x);
	realY = GameEngine::GetInstance()->ConvertY(y);

	if (type == MOUSEMOVE)//move
	{
		HandleMouseMove(realX, realY);
	}
	else if (type == MOUSELEFT)//left
	{
		HandleMouseLeft(realX, realY);
	}
	else if (type == MOUSERIGHT)//right
	{
		HandleMouseRight(realX, realY);
	}
	else if (type == MOUSESCROLL)//scroll
	{
		HandleMouseScroll(scrolling);
	}

}
void LevelBoard::HandleMouseLeft(int x, int y)
{
	float realX = (float)x;
	float realY = (float)y;

	for (int i = UIList.size() - 1; i >= 0; i--)
	{
		UIObject* ui1 = dynamic_cast<UIObject*>(UIList[i]);
		if (ui1 != nullptr)
		{
			if (ui1->ClickOn(x, y))
			{
				if (ui1->GetClickable())
				{
					GameEngine::GetInstance()->playSound("../Resource/Sound/Click Laser (V-ktor).wav");
				}
				if (ui1->getButtontype() == UI_PAUSE)
				{
					maincamera->SetEnable(false);
					mainboard->GetUIManager().showPauseUI();
				}
				else if (ui1->getButtontype() == UI_RESUME)
				{
					maincamera->SetEnable(true);
					mainboard->GetUIManager().closePauseUI();
					mainstate = LS_NORMAL;
				}
				else if (ui1->getButtontype() == UI_MOVE && ui1->GetClickable() == true)
				{
					maincamera->SetEnable(true);
					mainboard->GetUIManager().closeMoveMenuUI();
					mainboard->GetBoardstate() = BS_MOVE;
					mainboard->SelectedUnitOpenSpace(AT_MOVE);
					mainstate = LS_NORMAL;
				}
				else if (ui1->getButtontype() == UI_ATK && ui1->GetClickable() == true)
				{
					maincamera->SetEnable(true);
					mainboard->GetUIManager().closeMoveMenuUI();
					mainboard->GetBoardstate() = BS_ATTACK;
					mainboard->SelectedUnitOpenSpace(AT_ATK);
					mainstate = LS_NORMAL;
				}
				else if (ui1->getButtontype() == UI_SKILL && ui1->GetClickable() == true)
				{
					maincamera->SetEnable(false);
					Submarine* s1 = dynamic_cast<Submarine*>(mainboard->GetSelectedUnit());
					if (s1 != nullptr && s1->getDraw() == true)
					{
						mainboard->GetUIManager().showSubSkillUI(s1, maincamera);
					}
					else
					{
						mainboard->GetUIManager().showSkillMenuUI(mainboard->GetSelectedUnit(), maincamera);
						mainboard->SelectedUnitOpenSpace(AT_SKILL);
					}
					mainstate = LS_SELECTED;
				}
				else if (ui1->getButtontype() == UI_SUBSKILL1 && ui1->GetClickable() == true)
				{
					maincamera->SetEnable(false);
					mainboard->GetSelectedUnit()->setName("Jin(Ship)1");
					mainboard->GetUIManager().showSkillMenuUI(mainboard->GetSelectedUnit(), maincamera);
					mainboard->GetBoardstate() = BS_SKILLAREA;
					mainboard->SelectedUnitOpenSpace(AT_SKILL);
					mainstate = LS_SELECTED;
				}
				else if (ui1->getButtontype() == UI_SUBSKILL2 && ui1->GetClickable() == true)
				{
					maincamera->SetEnable(true);
					mainboard->GetSelectedUnit()->setName("Jin(Ship)2");
					mainboard->GetUIManager().showSkillMenuUI(mainboard->GetSelectedUnit(), maincamera);
					mainboard->GetBoardstate() = BS_SKILLSINGLE;
					mainboard->SelectedUnitOpenSpace(AT_SKILL);
					mainstate = LS_NORMAL;
				}
				else if (ui1->getButtontype() == UI_SUBSKILL3 && ui1->GetClickable() == true)
				{
					maincamera->SetEnable(false);
					mainboard->GetSelectedUnit()->setName("Jin(Ship)3");
					mainboard->GetUIManager().showSkillMenuUI(mainboard->GetSelectedUnit(), maincamera);
					mainboard->GetBoardstate() = BS_SKILLAREA;
					mainboard->SelectedUnitOpenSpace(AT_SKILL);
					mainstate = LS_SELECTED;
				}
				else if (ui1->getButtontype() == UI_USE)
				{
					maincamera->SetEnable(true);
					mainboard->GetUIManager().closeMoveMenuUI();
					mainboard->UseSelectedUnitSkill();
					mainstate = LS_NORMAL;
				}
				else if (ui1->getButtontype() == UI_CANCEL)
				{
					maincamera->SetEnable(true);
					mainboard->GetUIManager().closeMoveMenuUI();
					mainboard->ResetBoard();
					mainboard->GetBoardstate() = BS_NORMAL;
					mainstate = LS_NORMAL;
				}
				else if (ui1->getButtontype() == UI_INFO)
				{
					maincamera->SetEnable(false);
					mainboard->GetUIManager().closeMoveMenuUI();
					mainstate = LS_NORMAL;
					mainboard->GetUIManager().showInfoUI(mainboard->GetSelectedUnit());

				}

				if (ui1->getButtontype() == UI_BACK_OPTION) {
					settingui.closeAudioSUI();
				}
				else if (ui1->getButtontype() == UI_OPTION) {
					settingui.showAudioSUI();
				}
				else if (ui1->getButtontype() == UI_SPEED_1) {
					settingui.speedSelect(1);
				}
				else if (ui1->getButtontype() == UI_SPEED_2) {
					settingui.speedSelect(2);
				}
				else if (ui1->getButtontype() == UI_SPEED_3) {
					settingui.speedSelect(3);
				}
				else if (ui1->getButtontype() == UI_RESET) {
					settingui.showResaveMenu();
				}
				else if (ui1->getButtontype() == UI_BACK_RESAVE) {
					settingui.closeResaveMenu();
				}
				else if (ui1->getButtontype() == UI_BACK_INFO) {
					maincamera->SetEnable(true);
					mainboard->GetUIManager().closeInfoUI();
				}
				else if (ui1->getButtontype() == UI_TUTORIAL) {
					settingui.showTutorialUI(settingui.getCurrentTutorial());
				}
				else if (ui1->getButtontype() == UI_BACK_TURORIAL) {
					settingui.closeTutorialUI();
				}
				else if (ui1->getButtontype() == UI_ARROW_LEFT_TURORIAL) {
					settingui.showTutorialUI(settingui.getCurrentTutorial() - 1);
				}
				else if (ui1->getButtontype() == UI_ARROW_RIGHT_TURORIAL) {
					settingui.showTutorialUI(settingui.getCurrentTutorial() + 1);
				}

				return;
			}
		}
	}

	maincamera->SetEnable(true);
	mainboard->GetUIManager().closeMoveMenuUI();
	mainstate = LS_NORMAL;

	int BX, BY;//board X,Y

	BX = WorldtoBoardX(realX);
	BY = WorldtoBoardY(realY);

	if (BX >= 0 && BX < BOARDSIZEROW && BY >= 0 && BY < BOARDSIZECOLUMN)
	{
		mainboard->ClickHandle(MOUSELEFT, BX, BY, mainstate, maincamera);
	}
	else
	{
		mainboard->ClickHandle(MOUSERIGHT, x, y, mainstate, maincamera);
	}
}
void LevelBoard::HandleMouseRight(int x, int y)
{
	maincamera->SetEnable(true);
	mainboard->GetUIManager().closeMoveMenuUI();
	mainstate = LS_NORMAL;
	mainboard->ClickHandle(MOUSERIGHT, x, y, mainstate, maincamera);
}
void LevelBoard::HandleMouseMove(int x, int y)
{
	bool hitbutton = false;
	for (int i = UIList.size() - 1; i >= 0; i--)
	{
		UIObject* ui1 = dynamic_cast<UIObject*>(UIList[i]);
		if (ui1 != nullptr)
		{
			if (ui1->OnMouseStay(x, y))
			{
				hitbutton = true;
				if (prevbutton != ui1)
				{
					prevbutton = ui1;
					GameEngine::GetInstance()->playSound("../Resource/Sound/Click.mp3");
				}
				break;
			}
		}
	}
	if (hitbutton == false)
	{
		prevbutton = nullptr;
	}
	if (maincamera->GetEnable() == true)
	{
		if (y > (maincamera->GetWindowHeight() / 2.0f - 50.0f))
		{
			//cout << "up" << endl;
			maincamera->CameraMove(CAMERASPEED, 0);
		}
		else if (y < -(maincamera->GetWindowHeight() / 2.0f - 50.0f))
		{
			//cout << "down" << endl;
			maincamera->CameraMove(-CAMERASPEED, 0);
		}
		if (x > (maincamera->GetWindowWidth() / 2.0f - 50.0f))
		{
			//cout << "right" << endl;
			maincamera->CameraMove(0, CAMERASPEED);
		}
		else if (x < -(maincamera->GetWindowWidth() / 2.0f - 50.0f))
		{
			//cout << "left" << endl;
			maincamera->CameraMove(0, -CAMERASPEED);
		}
	}

	int BX, BY;//board X,Y

	BX = WorldtoBoardX(x);
	BY = WorldtoBoardY(y);

	if (BX >= 0 && BX < BOARDSIZEROW && BY >= 0 && BY < BOARDSIZECOLUMN)
	{
		mainboard->ClickHandle(MOUSEMOVE, BX, BY, mainstate, maincamera);
	}
}
void LevelBoard::HandleMouseScroll(int amount)
{
	if (maincamera->GetEnable() == true)
	{
		if (amount > 0) //scroll up
		{
			maincamera->CameraSetZoom(maincamera->GetZoomPercent() - 10.0f);
		}
		else if (amount < 0)//scroll down
		{
			maincamera->CameraSetZoom(maincamera->GetZoomPercent() + 10.0f);
		}
	}
}

