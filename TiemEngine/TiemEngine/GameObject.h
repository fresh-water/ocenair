#pragma once

#include "DrawableObject.h"
#include "gtc/matrix_transform.hpp"
#include "gtc/type_ptr.hpp"
#include <string>
class GameObject :public DrawableObject
{
	glm::vec3 color;
	int fxtype = 0;
	unsigned int texture;
	int Mode = 0;//mode 0 = color , mode = 1 texture

	bool inCam = true;
	bool Draw = true;

	float alpha = 1.0f;
	glm::vec3 coloroffset;

	int rowMax;
	int columnMax;
	int currentRow;
	int currentColumn;
	float uv[8];
	int startRow;
	int startColumn;
	int loopMax;
	int loopCount;
	int animationTime;
	int timeCount;

	bool isAnim = false;
	bool Loop = true;

public:
	GameObject();
	~GameObject();
	virtual void SetFxtype(int type);
	virtual int GetFxtype();
	virtual void SetColor(float r, float g, float b);
	virtual void SetColor(float r, float g, float b, float a);
	glm::vec3 GetColor();
	virtual void SetColorOffset(float r, float g, float b);
	glm::vec3 GetColorOffset();
	virtual void SetMode(int mode);

	virtual void setDraw(bool fact);
	virtual bool getDraw();

	virtual void setInCam(bool fact);
	virtual bool getInCam();

	virtual void SetTexture(string path);
	virtual void SetTexture(unsigned int tex);
	virtual void Render(glm::mat4 globalModelTransform);

	virtual void Update(float deltaTime);
	virtual void GenUV();
	virtual void SetAnimationLoop(int startRow, int startColumn, int howManyFrame, int delayBetaweenFrame); // 1000 = 1 second
	virtual void NextAnimation();
	virtual void setPath(string fileName, int row, int column);
	virtual void setPath(unsigned int texture, int row, int column);
	virtual void setUVmap(unsigned int texture, int row, int column);
	virtual void setUVmap(int row, int column);
	virtual void setAnimState();
	virtual void setLoop(bool fact);
};

