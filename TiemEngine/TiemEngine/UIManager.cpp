#include "UIManager.h"
#include <sstream>

void BacktoMainMenu(UIObject* button)
{
	cout << "Exit" << endl;
	GameData::GetInstance()->gGameStateNext = GameState::GS_MAINMENU;
}

glm::vec2 FindNewCenterUI(Unit* unit, Camera* camera,float offsetx,float offsety)
{
	camera->CameraAdjust();
	glm::vec2 pos = unit->getBoardPosition();

	float Z = camera->GetZoomSize();

	float convertposX = (100 * (pos.x + offsetx) * Z) - (camera->GetCenter().x * Z) - (camera->GetWindowWidth() * Z / 2.0f);
	float convertposY = (100 * (pos.y + offsety) * Z) - (camera->GetCenter().y * Z) - (camera->GetWindowHeight() * Z / 2.0f);

	convertposX = convertposX / (camera->GetWindowWidth() / 2.0f);
	convertposY = convertposY / (camera->GetWindowHeight() / 2.0f);

	glm::vec2 result = glm::vec2(convertposX, convertposY);
	return result;
}

UIManager::UIManager()
{

}

void UIManager::Init()
{
	///////////////////////////// CUT-IN UI /////////////////////////////////
	{
		for (int i = 0; i < 8; i++)
		{
			cutInUI[i] = new UIObject();
			cutInUI[i]->SetMode(1);
			cutInUI[i]->SetClickable(false);
			cutInUI[i]->SetDraw(false);
		}

		cutInUI[0]->SetSize(2, -1.2f);
		cutInUI[0]->SetPosition(glm::vec3(0.0f, 0.0f, 0));
		cutInUI[0]->SetTexture("../Resource/Texture/CutInPlayer.png");

		cutInUI[1]->SetSize(-1.0f, -1.0f);
		cutInUI[1]->SetPosition(glm::vec3(-0.5f, 0.0f, 0));
		cutInUI[1]->SetTexture("../Resource/Texture/CharacterSprite/Wong_Atk.png");

		cutInUI[2]->SetSize(0.65f, -1.0f);
		cutInUI[2]->SetPosition(glm::vec3(0.5f, 0.0f, 0));
		cutInUI[2]->setSprite("../Resource/Texture/CharacterSprite/Wong.png", 3, 4, HURT, 0);

		cutInUI[3]->SetSize(0.65f, -1.0f);
		cutInUI[3]->SetPosition(glm::vec3(0.5f, 0.0f, 0));
		cutInUI[3]->setPath("../Resource/Texture/CharacterSprite/Gray_AtkEffect.png", 1, 4);

		cutInUI[4]->SetSize(0.2, -0.16);
		cutInUI[4]->SetPosition(glm::vec3(0.5f - 0.11f, 0.0f, 0));
		cutInUI[4]->SetColorOffset(-1, 0, -1);
		cutInUI[4]->setSprite("../Resource/Texture/UINumber.png", 1, 10, 1, 0);

		cutInUI[5]->SetSize(0.2, -0.16);
		cutInUI[5]->SetPosition(glm::vec3(0.5f + 0.00f, 0.0f, 0));
		cutInUI[5]->SetColorOffset(-1, 0, -1);
		cutInUI[5]->setSprite("../Resource/Texture/UINumber.png", 1, 10, 1, 0);

		cutInUI[6]->SetSize(0.2, -0.16);
		cutInUI[6]->SetPosition(glm::vec3(0.5f + 0.11f, 0.0f, 0));
		cutInUI[6]->SetColorOffset(-1, 0, -1);
		cutInUI[6]->setSprite("../Resource/Texture/UINumber.png", 1, 10, 1, 0);

		cutInUI[7]->SetSize(0.85f, -1.1f);
		cutInUI[7]->SetPosition(glm::vec3(0.0f, 0.0f, 0));
		cutInUI[7]->setSprite("../Resource/Texture/CharacterSprite/Wong.png", 3, 4, 2, 2);
	}
	//////////////////////////INTERFACEUI////////////////////////
	{
		for (int i = 0; i < 11; i++)
		{
			interFaceUI[i] = new UIObject();
			interFaceUI[i]->SetMode(1);
			interFaceUI[i]->SetClickable(true);
			interFaceUI[i]->SetDraw(true);
		}

		interFaceUI[0]->SetSize(0.20, -0.20);
		interFaceUI[0]->SetPosition(glm::vec3(0.0f, 0.9f, 0));
		interFaceUI[0]->setSprite("../Resource/Texture/UIgameplay/PauseButton.png", 2, 1, 0, 0);
		interFaceUI[0]->SetClickOnAnim(true);
		interFaceUI[0]->SetClickable(true);
		interFaceUI[0]->setButtontype(UI_PAUSE);

		interFaceUI[1]->SetSize(0.2, -0.1);
		interFaceUI[1]->SetPosition(glm::vec3(-0.75f - 0.015f, 0.75f, 0));
		interFaceUI[1]->SetTexture("../Resource/Texture/UIgameplay/enemyleft.png");
		interFaceUI[1]->SetColorOffset(0,-1,-1);
		interFaceUI[1]->SetClickable(false);
		interFaceUI[1]->SetDraw(false);

		interFaceUI[2]->SetSize(0.05, -0.06);
		interFaceUI[2]->SetPosition(glm::vec3(-0.9f - 0.01f - 0.015f, 0.75f, 0));
		interFaceUI[2]->setSprite("../Resource/Texture/UINumber.png", 1, 10, 1, 0);
		interFaceUI[2]->SetColorOffset(0, -1, -1);
		interFaceUI[2]->SetClickable(false);
		interFaceUI[2]->SetDraw(false);

		interFaceUI[3]->SetSize(0.05, -0.06);
		interFaceUI[3]->SetPosition(glm::vec3(-0.9f + 0.02f - 0.015f, 0.75f, 0));
		interFaceUI[3]->setSprite("../Resource/Texture/UINumber.png", 1, 10, 1, 0);
		interFaceUI[3]->SetColorOffset(0, -1, -1);
		interFaceUI[3]->SetClickable(false);
		interFaceUI[3]->SetDraw(false);

		interFaceUI[4]->SetSize(2, -2);
		interFaceUI[4]->SetPosition(glm::vec3(0, 0, 0));
		interFaceUI[4]->setSprite("../Resource/Texture/UIgameplay/player_turn_anim.png",3,4,0,0);
		interFaceUI[4]->setLoop(false);
		interFaceUI[4]->SetClickable(false);
		interFaceUI[4]->SetColor(0, 0, 0, 1.0);

		interFaceUI[5]->SetSize(0.3, -0.2);
		interFaceUI[5]->SetPosition(glm::vec3(-0.80f, 0.9f, 0));
		interFaceUI[5]->SetMode(0);
		interFaceUI[5]->SetClickable(false);
		interFaceUI[5]->SetColor(0, 0, 0, 0.7);
		interFaceUI[5]->SetDraw(true);

		interFaceUI[6]->SetSize(0.3, -0.2);
		interFaceUI[6]->SetPosition(glm::vec3(-0.80f, 0.9f, 0));
		interFaceUI[6]->SetTexture("../Resource/Texture/UIicon/Questtemp1.png");
		interFaceUI[6]->SetClickable(false);
		interFaceUI[6]->SetDraw(true);

		interFaceUI[7]->SetSize(0.6, -0.2);
		//interFaceUI[7]->SetPosition(glm::vec3(-0.80f, 0.65f, 0));
		interFaceUI[7]->SetPosition(glm::vec3(-0, 0.65f, 0));
		interFaceUI[7]->SetTexture("../Resource/Texture/UIgameplay/passturnSB.png");
		interFaceUI[7]->SetClickable(false);
		interFaceUI[7]->SetColorOffset(-1, 0, -1);
		interFaceUI[7]->SetColor(0, 0, 0, 0.7);
		interFaceUI[7]->SetDraw(true);

		interFaceUI[8]->SetSize(2, 2);
		interFaceUI[8]->SetPosition(glm::vec3(0, 0, 0));
		interFaceUI[8]->SetMode(0);
		interFaceUI[8]->SetClickable(false);
		interFaceUI[8]->SetColor(0, 0, 0, 0.7);
		interFaceUI[8]->SetDraw(false);

		interFaceUI[9]->SetSize(0.6, -1.7);
		interFaceUI[9]->SetPosition(glm::vec3(0.0f, 0, 0));
		interFaceUI[9]->SetTexture("../Resource/Texture/Ash_Info.png");
		interFaceUI[9]->SetClickable(false);
		interFaceUI[9]->SetDraw(false);

		interFaceUI[10]->SetSize(0.25, -0.15);
		interFaceUI[10]->SetPosition(glm::vec3(0.15f, -0.8f, 0));
		interFaceUI[10]->setSprite("../Resource/Texture/CSback.png",2,1,0,0);
		interFaceUI[10]->SetClickOnAnim(true);
		interFaceUI[10]->SetClickable(true);
		interFaceUI[10]->setButtontype(UI_BACK_INFO);
		interFaceUI[10]->SetDraw(false);
	}
	//////////////////////////////////// PAUSE MENU //////////////////////////////////////
	{
		for (int i = 0; i < 5; i++)
		{
			PauseMenu[i] = new UIObject();
			PauseMenu[i]->SetMode(1);
			PauseMenu[i]->SetClickable(true);
			PauseMenu[i]->SetDraw(false);
		}

		PauseMenu[0]->SetMode(0);
		PauseMenu[0]->SetSize(2, 2);
		PauseMenu[0]->SetPosition(glm::vec3(0, 0, 0));
		PauseMenu[0]->SetColor(0.0f, 0.0f, 0.0f, 0.5f);
		PauseMenu[0]->SetClickable(false);

		PauseMenu[1]->SetSize(0.8, -0.45);
		PauseMenu[1]->SetPosition(glm::vec3(0, 0.6f, 0));
		PauseMenu[1]->SetTexture("../Resource/Texture/UIPause.png");
		PauseMenu[1]->SetClickable(false);

		PauseMenu[2]->SetSize(0.5, -0.3);
		PauseMenu[2]->SetPosition(glm::vec3(0, 0.15f, 0));
		PauseMenu[2]->SetTexture("../Resource/Texture/UIResume.png");
		PauseMenu[2]->setButtontype(UI_RESUME);

		PauseMenu[3]->SetSize(0.5, -0.3);
		PauseMenu[3]->SetPosition(glm::vec3(0, -0.2f, 0));
		PauseMenu[3]->SetTexture("../Resource/Texture/UIOption.png");
		PauseMenu[3]->setButtontype(UI_OPTION);

		PauseMenu[4]->SetSize(0.5, -0.3);
		PauseMenu[4]->SetPosition(glm::vec3(0, -0.55f, 0));
		PauseMenu[4]->SetTexture("../Resource/Texture/UIExit.png");
		PauseMenu[4]->SetActiveFn(BacktoMainMenu);
	}
	////////////////////////////////////////MOVEUI/////////////////////////////////////
	{
		for (int i = 0; i < 7; i++)
		{
			MoveMenuUI[i] = new UIObject();
			MoveMenuUI[i]->SetSize(0.125, -0.25);
			MoveMenuUI[i]->SetPosition(glm::vec3(0.0f, 0.0f, 0.0f));
			MoveMenuUI[i]->SetMode(1);
			MoveMenuUI[i]->SetClickOnAnim(true);
			MoveMenuUI[i]->SetClickable(true);
			MoveMenuUI[i]->SetDraw(false);
		}
		MoveMenuUI[0]->setSprite("../Resource/Texture/UIicon/moveicon.png", 2, 1, 0, 0);
		MoveMenuUI[0]->setButtontype(UI_MOVE);
		MoveMenuUI[1]->setSprite("../Resource/Texture/UIicon/atkicon.png", 2, 1, 0, 0);
		MoveMenuUI[1]->setButtontype(UI_ATK);
		MoveMenuUI[2]->setSprite("../Resource/Texture/UIicon/SIWong.png", 2, 1, 0, 0);
		MoveMenuUI[2]->setButtontype(UI_SKILL);
		MoveMenuUI[3]->setSprite("../Resource/Texture/UIicon/infoicon.png", 2, 1, 0, 0);
		MoveMenuUI[3]->setButtontype(UI_INFO);
		MoveMenuUI[4]->setSprite("../Resource/Texture/UINumber.png", 1, 10, 1, 0);
		MoveMenuUI[4]->SetClickable(false);
		MoveMenuUI[4]->SetSize(0.1, -0.15);
		MoveMenuUI[4]->SetColorOffset(0, -1, -1);
		//for sub
		MoveMenuUI[5]->setSprite("../Resource/Texture/UINumber.png", 1, 10, 1, 0);
		MoveMenuUI[5]->SetClickable(false);
		MoveMenuUI[5]->SetSize(0.1, -0.15);
		MoveMenuUI[5]->SetColorOffset(0, -1, -1);
		MoveMenuUI[6]->setSprite("../Resource/Texture/UINumber.png", 1, 10, 1, 0);
		MoveMenuUI[6]->SetClickable(false);
		MoveMenuUI[6]->SetSize(0.1, -0.15);
		MoveMenuUI[6]->SetColorOffset(0, -1, -1);
	}
	///////////////////////////////////////////////PLAYER/////////////////////////////////////////////////
	{
		for (int i = 0; i < 20; i++)
		{
			ProfilePlayer[i] = new UIObject();
			ProfilePlayer[i]->SetMode(1);
			ProfilePlayer[i]->SetClickable(false);
			ProfilePlayer[i]->SetDraw(false);

			ProfileEnemy[i] = new UIObject();
			ProfileEnemy[i]->SetMode(1);
			ProfileEnemy[i]->SetClickable(false);
			ProfileEnemy[i]->SetDraw(false);
		}

		ProfileEnemy[4]->SetDraw(false);
		ProfileEnemy[11]->SetDraw(false);
		ProfileEnemy[12]->SetDraw(false);

		ProfilePlayer[0]->SetSize(0.60, -0.35);
		ProfilePlayer[0]->SetPosition(glm::vec3(-1.0f + 0.3f + 0.125f, -0.75f - 0.05f, 0.0f));
		ProfilePlayer[0]->SetTexture("../Resource/Texture/UIgameplay/status_Character.png");

		ProfilePlayer[1]->SetSize(0.30, -0.55);
		ProfilePlayer[1]->SetPosition(glm::vec3(-0.85f + 0.01f, -0.70f - 0.01f, 0.0f));
		ProfilePlayer[1]->SetTexture("../Resource/Texture/UIgameplay/profile_Character.png");

		ProfilePlayer[2]->SetSize(0.1, -0.08);
		ProfilePlayer[2]->SetSize(0.0, -0.00);
		ProfilePlayer[2]->SetPosition(glm::vec3(-0.5f - 0.15f, -0.65f - 0.05f, 0.0f));
		ProfilePlayer[2]->SetTexture("../Resource/Texture/UIHP.png");
		///NUMBER O2
		ProfilePlayer[3]->SetSize(0.04, -0.06);
		ProfilePlayer[3]->SetPosition(glm::vec3(-0.322f - 0.01f, -0.915f, 0.0f));
		ProfilePlayer[3]->SetColorOffset(-1, 0, -1);
		ProfilePlayer[3]->setSprite("../Resource/Texture/UINumber.png", 1, 10, 1, 0);

		ProfilePlayer[4]->SetSize(0.04, -0.06);
		ProfilePlayer[4]->SetPosition(glm::vec3(-0.322f + 0.015f, -0.915f, 0.0f));
		ProfilePlayer[4]->SetColorOffset(-1, 0, -1);
		ProfilePlayer[4]->setSprite("../Resource/Texture/UINumber.png", 1, 10, 1, 0);
		///HP Bar
		ProfilePlayer[5]->SetSize(0.33, -0.05);
		ProfilePlayer[5]->SetPosition(glm::vec3(-0.535f, -0.786f, 0.0f));
		ProfilePlayer[5]->SetMode(0);
		ProfilePlayer[5]->SetColorOffset(0, 0.5, 0);

		ProfilePlayer[6]->SetSize(0.33, -0.05);
		ProfilePlayer[6]->SetPosition(glm::vec3(-0.535f, -0.786f, 0.0f));
		ProfilePlayer[6]->SetMode(0);
		ProfilePlayer[6]->SetColorOffset(0, 1, 0);
		//??
		ProfilePlayer[7]->SetSize(0.3, -0.08);
		ProfilePlayer[7]->SetSize(0.0, -0.00);
		ProfilePlayer[7]->SetPosition(glm::vec3(-0.55f + 0.05f, -0.65f - 0.05f, 0.0f));
		ProfilePlayer[7]->SetTexture("../Resource/Texture/Cursor.png");
		///NUMBER HP
		ProfilePlayer[8]->SetSize(0.04, -0.06);
		ProfilePlayer[8]->SetPosition(glm::vec3(-0.322f - 0.02f, -0.786f, 0.0f));
		ProfilePlayer[8]->SetColorOffset(-1, 0, -1);
		ProfilePlayer[8]->setSprite("../Resource/Texture/UINumber.png", 1, 10, 1, 0);

		ProfilePlayer[9]->SetSize(0.04, -0.06);
		ProfilePlayer[9]->SetPosition(glm::vec3(-0.322f, -0.786f, 0.0f));
		ProfilePlayer[9]->SetColorOffset(-1, 0, -1);
		ProfilePlayer[9]->setSprite("../Resource/Texture/UINumber.png", 1, 10, 1, 0);

		ProfilePlayer[10]->SetSize(0.04, -0.06);
		ProfilePlayer[10]->SetPosition(glm::vec3(-0.322f + 0.02f, -0.786f, 0.0f));
		ProfilePlayer[10]->SetColorOffset(-1, 0, -1);
		ProfilePlayer[10]->setSprite("../Resource/Texture/UINumber.png", 1, 10, 1, 0);
		///O2 Bar
		ProfilePlayer[11]->SetSize(0.25, -0.05);
		ProfilePlayer[11]->SetPosition(glm::vec3(-0.495f, -0.915f, 0.0f));
		ProfilePlayer[11]->SetMode(0);
		ProfilePlayer[11]->SetColorOffset(0, 0.5, 0);

		ProfilePlayer[12]->SetSize(0.25, -0.05);
		ProfilePlayer[12]->SetPosition(glm::vec3(-0.495f, -0.915f, 0.0f));
		ProfilePlayer[12]->SetMode(0);
		ProfilePlayer[12]->SetColorOffset(0, 1, 0);
		///ICON
		ProfilePlayer[13]->SetSize(0.20, -0.35);
		ProfilePlayer[13]->SetPosition(glm::vec3(-0.85f + 0.01f, -0.685f, 0.0f));
		///BUFF
		ProfilePlayer[14]->SetSize(0.06, -0.1);
		ProfilePlayer[14]->SetPosition(glm::vec3(-0.5f, -0.65f, 0.0f));
		ProfilePlayer[14]->setSprite("../Resource/Texture/UIicon/UI_Buff.png", 1, 6, 1, 0);

		ProfilePlayer[15]->SetSize(0.06, -0.1);
		ProfilePlayer[15]->SetPosition(glm::vec3(-0.5f + 0.07f, -0.65f, 0.0f));
		ProfilePlayer[15]->setSprite("../Resource/Texture/UIicon/UI_Buff.png", 1, 6, 1, 1);

		ProfilePlayer[16]->SetSize(0.06, -0.1);
		ProfilePlayer[16]->SetPosition(glm::vec3(-0.5f + 0.14f, -0.65f, 0.0f));
		ProfilePlayer[16]->setSprite("../Resource/Texture/UIicon/UI_Buff.png", 1, 6, 1, 2);

		ProfilePlayer[17]->SetSize(0.06, -0.1);
		ProfilePlayer[17]->SetPosition(glm::vec3(-0.5f + 0.00f, -0.65f + 0.11f, 0.0f));
		ProfilePlayer[17]->setSprite("../Resource/Texture/UIicon/UI_Buff.png", 1, 6, 1, 0);

		ProfilePlayer[18]->SetSize(0.06, -0.1);
		ProfilePlayer[18]->SetPosition(glm::vec3(-0.5f + 0.07f, -0.65f + 0.11f, 0.0f));
		ProfilePlayer[18]->setSprite("../Resource/Texture/UIicon/UI_Buff.png", 1, 6, 1, 1);

		ProfilePlayer[19]->SetSize(0.06, -0.1);
		ProfilePlayer[19]->SetPosition(glm::vec3(-0.5f + 0.14f, -0.65f + 0.11f, 0.0f));
		ProfilePlayer[19]->setSprite("../Resource/Texture/UIicon/UI_Buff.png", 1, 6, 1, 2);
	}
	////////////////////////////////////////////////ARROWHITRATE????????????????????????????????????????
	{
		for (int i = 0; i < 5; i++)
		{
			ArrowHitRate[i] = new UIObject();
			ArrowHitRate[i]->SetMode(1);
			ArrowHitRate[i]->SetClickable(false);
			ArrowHitRate[i]->SetDraw(false);
		}
		ArrowHitRate[0]->SetSize(0.40, -0.30);
		ArrowHitRate[0]->SetPosition(glm::vec3(0.0f, -0.8f, 0.0f));
		ArrowHitRate[0]->SetTexture("../Resource/Texture/UIgameplay/Arrow_Green.png");

		ArrowHitRate[1]->SetSize(0.05, -0.10);
		ArrowHitRate[1]->SetPosition(glm::vec3(0.05f - 0.05f, -0.8f, 0.0f));
		ArrowHitRate[1]->SetTexture("../Resource/Texture/percent.png");

		ArrowHitRate[2]->SetSize(0.05, -0.10);
		ArrowHitRate[2]->SetPosition(glm::vec3(-0.06f - 0.05f, -0.8f, 0.0f));
		ArrowHitRate[2]->setSprite("../Resource/Texture/UINumber.png", 1, 10, 1, 0);
		ArrowHitRate[2]->SetColorOffset(-1, 0, -1);

		ArrowHitRate[3]->SetSize(0.05, -0.10);
		ArrowHitRate[3]->SetPosition(glm::vec3(-0.03f - 0.05f, -0.8f, 0.0f));
		ArrowHitRate[3]->setSprite("../Resource/Texture/UINumber.png", 1, 10, 1, 0);
		ArrowHitRate[3]->SetColorOffset(-1, 0, -1);

		ArrowHitRate[4]->SetSize(0.05, -0.10);
		ArrowHitRate[4]->SetPosition(glm::vec3(0.0f - 0.05f, -0.8f, 0.0f));
		ArrowHitRate[4]->setSprite("../Resource/Texture/UINumber.png", 1, 10, 1, 0);
		ArrowHitRate[4]->SetColorOffset(-1, 0, -1);
	}
	///////////////////////////////////////////////ENEMY/////////////////////////////////////////////////
	{
		ProfileEnemy[0]->SetSize(0.60, -0.35);
		ProfileEnemy[0]->SetPosition(glm::vec3(+1.0f - 0.3f - 0.125f, -0.75f - 0.05f, 0.0f));
		ProfileEnemy[0]->SetTexture("../Resource/Texture/UIgameplay/status_Enemy.png");

		ProfileEnemy[1]->SetSize(0.30, -0.55);
		ProfileEnemy[1]->SetPosition(glm::vec3(0.84f, -0.71f, 0.0f));
		ProfileEnemy[1]->SetTexture("../Resource/Texture/UIgameplay/profile_Enemy.png");

		ProfileEnemy[2]->SetSize(0.1, -0.08);
		ProfileEnemy[2]->SetSize(0.0, -0.0);
		ProfileEnemy[2]->SetPosition(glm::vec3(0.5f - 0.15f, -0.65f - 0.05f, 0.0f));
		ProfileEnemy[2]->SetTexture("../Resource/Texture/UIHP.png");

		ProfileEnemy[3]->SetSize(0.1, -0.08);
		ProfileEnemy[3]->SetSize(0.0, -0.0);
		ProfileEnemy[3]->SetPosition(glm::vec3(0.5f - 0.15f, -0.75f - 0.05f, 0.0f));
		ProfileEnemy[3]->SetTexture("../Resource/Texture/UIATK.png");

		ProfileEnemy[4]->SetSize(0.1, -0.08);
		ProfileEnemy[4]->SetSize(0.0, -0.0);
		ProfileEnemy[4]->SetPosition(glm::vec3(0.5f - 0.15f, -0.85f - 0.05f, 0.0f));
		ProfileEnemy[4]->SetTexture("../Resource/Texture/UIEVa.png");
		///HP bar
		ProfileEnemy[5]->SetSize(0.325, -0.05);
		ProfileEnemy[5]->SetPosition(glm::vec3(0.536f, -0.825f, 0.0f));
		ProfileEnemy[5]->SetColorOffset(0.5f,0,0);
		ProfileEnemy[5]->SetMode(0);

		ProfileEnemy[6]->SetSize(0.325, -0.05);
		ProfileEnemy[6]->SetPosition(glm::vec3(0.536f, -0.825f, 0.0f));
		ProfileEnemy[6]->SetColorOffset(1, 0, 0);
		ProfileEnemy[6]->SetMode(0);

		ProfileEnemy[7]->SetSize(0.325, -0.05);
		ProfileEnemy[7]->SetPosition(glm::vec3(0.5f + 0.05f, -0.65f - 0.05f, 0.0f));
		ProfileEnemy[7]->SetColor(1, 1, 1, 0.5f);
		ProfileEnemy[7]->SetMode(0);
		///HP num
		ProfileEnemy[8]->SetSize(0.04, -0.06);
		ProfileEnemy[8]->SetPosition(glm::vec3(0.32f - 0.02f, -0.825f, 0.0f));
		ProfileEnemy[8]->SetColorOffset(0, -1, -1);
		ProfileEnemy[8]->setSprite("../Resource/Texture/UINumber.png", 1, 10, 1, 0);

		ProfileEnemy[9]->SetSize(0.04, -0.06);
		ProfileEnemy[9]->SetPosition(glm::vec3(0.32f, -0.825f, 0.0f));
		ProfileEnemy[9]->SetColorOffset(0, -1, -1);
		ProfileEnemy[9]->setSprite("../Resource/Texture/UINumber.png", 1, 10, 1, 0);

		ProfileEnemy[10]->SetSize(0.04, -0.06);
		ProfileEnemy[10]->SetPosition(glm::vec3(0.32f + 0.02f, -0.825f, 0.0f));
		ProfileEnemy[10]->SetColorOffset(0, -1, -1);
		ProfileEnemy[10]->setSprite("../Resource/Texture/UINumber.png", 1, 10, 1, 0);
		///O2
		ProfileEnemy[11]->SetSize(0.1, -0.08);
		ProfileEnemy[11]->SetPosition(glm::vec3(0.5f + 0.05f, -0.85f - 0.05f, 0.0f));
		ProfileEnemy[11]->setSprite("../Resource/Texture/UINumber.png", 1, 10, 1, 0);

		ProfileEnemy[12]->SetSize(0.1, -0.08);
		ProfileEnemy[12]->SetPosition(glm::vec3(0.5f + 0.15f, -0.85f - 0.05f, 0.0f));
		ProfileEnemy[12]->setSprite("../Resource/Texture/UINumber.png", 1, 10, 1, 0);
		///
		ProfileEnemy[13]->SetSize(0.20, -0.35);
		ProfileEnemy[13]->SetPosition(glm::vec3(0.84f, -0.685f, 0.0f));
		///BUFF
		ProfileEnemy[14]->SetSize(0.05, -0.12);
		ProfileEnemy[14]->SetPosition(glm::vec3(0.5f - 0.1f, -0.75f + 0.2f, 0.0f));
		ProfileEnemy[14]->setSprite("../Resource/Texture/UIicon/UI_Buff.png", 1, 6, 1, 3);

		ProfileEnemy[15]->SetSize(0.05, -0.12);
		ProfileEnemy[15]->SetPosition(glm::vec3(0.5f + 0.0f, -0.75f + 0.2f, 0.0f));
		ProfileEnemy[15]->setSprite("../Resource/Texture/UIicon/UI_Buff.png", 1, 6, 1, 4);

		ProfileEnemy[16]->SetSize(0.05, -0.12);
		ProfileEnemy[16]->SetPosition(glm::vec3(0.5f + 0.1f, -0.75f + 0.2f, 0.0f));
		ProfileEnemy[16]->setSprite("../Resource/Texture/UIicon/UI_Buff.png", 1, 6, 1, 5);

		ProfileEnemy[17]->SetSize(0.05, -0.12);
		ProfileEnemy[17]->SetPosition(glm::vec3(0.5f + 0.2f, -0.75f + 0.2f, 0.0f));
		ProfileEnemy[17]->setSprite("../Resource/Texture/UIicon/UI_Buff.png", 1, 6, 1, 3);

		ProfileEnemy[18]->SetSize(0.1, -0.12);
		ProfileEnemy[18]->SetPosition(glm::vec3(0.5f + 0.3f, -0.75f + 0.2f, 0.0f));
		ProfileEnemy[18]->setSprite("../Resource/Texture/UIicon/UI_Buff.png", 1, 6, 1, 4);

		ProfileEnemy[19]->SetSize(0.1, -0.12);
		ProfileEnemy[19]->SetPosition(glm::vec3(0.5f + 0.4f, -0.75f + 0.2f, 0.0f));
		ProfileEnemy[19]->setSprite("../Resource/Texture/UIicon/UI_Buff.png", 1, 6, 1, 5);
	}
}
void UIManager::Update()
{
	if (countdownphase > 0)
	{
		countdownphase--;
	}
	if (countdownphase == 1)
	{
		interFaceUI[4]->SetDraw(false);
		//countdownphase--;
	}
	if (countdownatkanim > 0)
	{
		float speed = 0.0015f;
		float spdnum = 0.0035f;
		cutInUI[1]->SetPosition(glm::vec3(cutInUI[1]->GetPosition().x + speed, cutInUI[1]->GetPosition().y, cutInUI[1]->GetPosition().z));
		cutInUI[2]->SetPosition(glm::vec3(cutInUI[2]->GetPosition().x + speed, cutInUI[2]->GetPosition().y, cutInUI[2]->GetPosition().z));
		cutInUI[3]->SetPosition(glm::vec3(cutInUI[3]->GetPosition().x + speed, cutInUI[3]->GetPosition().y, cutInUI[3]->GetPosition().z));
		cutInUI[4]->SetPosition(glm::vec3(cutInUI[4]->GetPosition().x + spdnum, cutInUI[4]->GetPosition().y + spdnum, cutInUI[4]->GetPosition().z));
		cutInUI[5]->SetPosition(glm::vec3(cutInUI[5]->GetPosition().x + spdnum, cutInUI[5]->GetPosition().y + spdnum, cutInUI[5]->GetPosition().z));
		cutInUI[6]->SetPosition(glm::vec3(cutInUI[6]->GetPosition().x + spdnum, cutInUI[6]->GetPosition().y + spdnum, cutInUI[6]->GetPosition().z));
		cutInUI[7]->SetPosition(glm::vec3(cutInUI[7]->GetPosition().x - speed, cutInUI[7]->GetPosition().y, cutInUI[7]->GetPosition().z));
		countdownatkanim--;
	}
	if(countdownatkanim == 1)
	{
		for (int i = 0; i < 8; i++)
		{
			cutInUI[i]->SetDraw(false);
		}
	}
}
void UIManager::showCutinUI(bool hit, Unit* attacker, Unit* defender)
{
	cutInUI[1]->SetPosition(glm::vec3(-0.5f, 0.0f, 0));
	cutInUI[2]->SetPosition(glm::vec3(0.5f, 0.0f, 0));
	cutInUI[3]->SetPosition(glm::vec3(0.5f, 0.0f, 0));
	SpriteInfo si1 = GameDataStorage::GetInstance()->getEffectInfo(attacker->getName());
	cutInUI[3]->setPath(si1.texture, si1.row, si1.col);
	cutInUI[3]->setLoop(false);
	cutInUI[3]->SetAnimationLoop(0, 0, si1.col, 150);
	cutInUI[4]->SetPosition(glm::vec3(0.5f - 0.11f, 0.0f, 0));
	cutInUI[5]->SetSize(0.2, -0.16);
	cutInUI[5]->SetPosition(glm::vec3(0.5f + 0.00f, 0.0f, 0));
	cutInUI[6]->SetPosition(glm::vec3(0.5f + 0.11f, 0.0f, 0));

	Character* c1 = dynamic_cast<Character*>(attacker);
	if (c1 != nullptr && c1->getDraw() == true)
	{
		cutInUI[0]->SetTexture("../Resource/Texture/CutInPlayer.png");
		cutInUI[1]->SetTexture(c1->getatktexture());
	}
	Submarine* s1 = dynamic_cast<Submarine*>(attacker);
	if (s1 != nullptr && s1->getDraw() == true)
	{
		cutInUI[0]->SetTexture("../Resource/Texture/CutInPlayer.png");
		cutInUI[1]->SetTexture(s1->getatktexture());
		cutInUI[3]->setPath("../Resource/Texture/CharacterSprite/Gray_AtkEffect.png", 1, 4);
		cutInUI[3]->setLoop(false);
		cutInUI[3]->SetAnimationLoop(0, 0, 4, 150);
	}
	Enemy* e1 = dynamic_cast<Enemy*>(attacker);
	if (e1 != nullptr && e1->getDraw() == true)
	{
		cutInUI[0]->SetTexture("../Resource/Texture/CutInEnemy.png");
		cutInUI[1]->SetTexture(e1->getatktexture());
	}

	Character* c2 = dynamic_cast<Character*>(defender);
	if (c2 != nullptr && c2->getDraw() == true)
	{
		cutInUI[2]->setSprite(c2->getTexture(), c2->getRow(), c2->getCol(), HURT, 0);
	}
	Submarine* s2 = dynamic_cast<Submarine*>(defender);
	if (s2 != nullptr && s2->getDraw() == true)
	{
		cutInUI[2]->setSprite(s2->getTexture(), s2->getRow(), s2->getCol(), HURT, 0);
	}
	Enemy* e2 = dynamic_cast<Enemy*>(defender);
	if (e2 != nullptr && e2->getDraw() == true)
	{
		cutInUI[2]->setSprite(e2->getTexture(), e2->getRow(), e2->getCol(), HURT-1, 0);
	}

	for (int i = 0; i < 7; i++)
	{
		cutInUI[i]->SetDraw(true);
	}

	int amount = attacker->getAtk() - defender->getDef();
	if (hit == true)
	{
		setNumber(amount, cutInUI[4], cutInUI[5], cutInUI[6]);
	}
	else
	{
		cutInUI[4]->SetDraw(false);
		cutInUI[6]->SetDraw(false);
		cutInUI[5]->SetPosition(glm::vec3(0.5f + 0.00f, 0.0f, 0));
		cutInUI[5]->SetSize(0.6, -0.16);
		cutInUI[5]->setSprite("../Resource/Texture/UIMiss.png", 1, 1, 0, 0);
	}

	countdownatkanim = 100;
}
void UIManager::showSkillCutinUI(Unit* skilluser)
{
	cutInUI[0]->SetTexture("../Resource/Texture/CutInPlayer.png");
	cutInUI[0]->SetDraw(true);
	cutInUI[7]->SetPosition(glm::vec3(0.1f, 0.0f, 0));
	cutInUI[7]->setSprite(skilluser->getTexture(), skilluser->getRow(), skilluser->getCol(), 2, 2);
	cutInUI[7]->SetDraw(true);

	countdownatkanim = 100;
}
void UIManager::showPhaseChange(int type)
{
	int countdowntime = 100;
	countdownphase = countdowntime;
	if (type == CHARACTER)
	{
		interFaceUI[4]->setSprite("../Resource/Texture/UIgameplay/player_turn_anim.png", 3, 4, 0, 0);
		interFaceUI[4]->SetAnimationLoop(0, 0, 10, 50);
		interFaceUI[4]->SetDraw(true);
	}
	else if (type == ENEMY)
	{
		interFaceUI[4]->setSprite("../Resource/Texture/UIgameplay/enemy_turn_anim.png", 3, 4, 0, 0);
		interFaceUI[4]->SetAnimationLoop(0, 0, 10, 50);
		interFaceUI[4]->SetDraw(true);
	}
}

void UIManager::showProfileUI(Unit* unit)
{
	Character* c1 = dynamic_cast<Character*>(unit);
	if (c1 != nullptr && c1->getDraw() == true)
	{
		for (int i = 0; i < 20; i++)
		{
			ProfilePlayer[i]->SetDraw(true);
		}
		ProfilePlayer[13]->setSprite(unit->getTexture(),unit->getRow(),unit->getCol(),IDLE,0);
		setStatusbar(c1->getHp(),c1->getMaxHp(),CHARACTER, ProfilePlayer[6], ProfilePlayer[5]);
		ProfilePlayer[7]->SetDraw(false);
		setNumber(c1->getHp(), ProfilePlayer[8], ProfilePlayer[9], ProfilePlayer[10]);
		setStatusbar(c1->getOxygen(), 10, CHARACTER, ProfilePlayer[12], ProfilePlayer[11]);
		setNumber(c1->getOxygen(), nullptr, ProfilePlayer[3], ProfilePlayer[4]);
		for (int i = 0; i < 6; i++)
		{
			ProfilePlayer[i+14]->SetDraw(false);
		}
		for (int i = 0; i < c1->getBuffVector().size(); i++)
		{
			int type = c1->getBuffVector()[i].getType();
			int mode;
			if (c1->getBuffVector()[i].getEffect() > 0) { mode = 0; }
			else { mode = 1; }
			int index = i + 14;
			if (!ProfilePlayer[index]->GetDraw() && index < 20)
			{
				ProfilePlayer[index]->setSprite("../Resource/Texture/UIicon/UI_Buff.png", 2, 6, 0, type);
				ProfilePlayer[index]->SetDraw(true);
			}
		}
	}
	Submarine* s1 = dynamic_cast<Submarine*>(unit);
	if (s1 != nullptr && s1->getDraw() == true)
	{
		for (int i = 0; i < 20; i++)
		{
			ProfilePlayer[i]->SetDraw(true);
		}
		ProfilePlayer[3]->SetDraw(false);
		ProfilePlayer[4]->SetDraw(false);
		ProfilePlayer[11]->SetDraw(false);
		ProfilePlayer[12]->SetDraw(false);
		ProfilePlayer[13]->setSprite(unit->getTexture(), unit->getRow(), unit->getCol(), IDLE, 0);
		setStatusbar(s1->getHp(), s1->getMaxHp(), SUBMARINE, ProfilePlayer[6], ProfilePlayer[5]);
		ProfilePlayer[7]->SetDraw(false);
		setNumber(s1->getHp(), ProfilePlayer[8], ProfilePlayer[9], ProfilePlayer[10]);
		for (int i = 0; i < 6; i++)
		{
			ProfilePlayer[i + 14]->SetDraw(false);
		}
		for (int i = 0; i < s1->getBuffVector().size(); i++)
		{
			int type = s1->getBuffVector()[i].getType();
			int mode;
			if (s1->getBuffVector()[i].getEffect() > 0) { mode = 0; }
			else { mode = 1; }
			int index = i + 14;
			if (!ProfilePlayer[index]->GetDraw() && index < 20)
			{
				ProfilePlayer[index]->setSprite("../Resource/Texture/UIicon/UI_Buff.png", 2, 6, 0, type);
				ProfilePlayer[index]->SetDraw(true);
			}
		}
	}
	Enemy* e1 = dynamic_cast<Enemy*>(unit);
	if (e1 != nullptr && e1->getDraw() == true)
	{
		for (int i = 0; i < 20; i++)
		{
			ProfileEnemy[i]->SetDraw(true);
		}
		ProfileEnemy[4]->SetDraw(false);
		ProfileEnemy[11]->SetDraw(false);
		ProfileEnemy[12]->SetDraw(false);
		ProfileEnemy[13]->setSprite(unit->getTexture(), unit->getRow(), unit->getCol(), 0, 0);
		setStatusbar(e1->getHp(), e1->getMaxHp(), ENEMY, ProfileEnemy[6], ProfileEnemy[5]);
		ProfileEnemy[7]->SetDraw(false);
		setNumber(e1->getHp(), ProfileEnemy[8], ProfileEnemy[9], ProfileEnemy[10]);
		cout << ProfileEnemy[8]->GetColorOffset().r << endl;
		for (int i = 0; i < 6; i++)
		{
			ProfileEnemy[i + 14]->SetDraw(false);
		}
		for (int i = 0;i<e1->getBuffVector().size();i++)
		{
			int type = e1->getBuffVector()[i].getType();
			int mode;
			if (e1->getBuffVector()[i].getEffect() > 0) { mode = 0; }
			else { mode = 1; }
			int index = i + 14;
			if (!ProfileEnemy[index]->GetDraw() && index < 20)
			{
				ProfileEnemy[index]->setSprite("../Resource/Texture/UIicon/UI_Buff.png", 2, 6, mode, type);
				ProfileEnemy[index]->SetDraw(true);
			}
		}
	}
}
void UIManager::showProfileUI(Unit* unit, int hit, int damage)
{
	Enemy* e1 = dynamic_cast<Enemy*>(unit);
	if (e1 != nullptr && e1->getDraw() == true)
	{
		for (int i = 0; i < 20; i++)
		{
			ProfileEnemy[i]->SetDraw(true);
		}
		for (int i = 0; i < 5; i++)
		{
			ArrowHitRate[i]->SetDraw(true);
		}
		ProfileEnemy[13]->setSprite(unit->getTexture(), unit->getRow(), unit->getCol(), 0, 0);
		setStatusbar(e1->getHp(), e1->getMaxHp(), ENEMY, ProfileEnemy[6], ProfileEnemy[5]);
		setStatusbar(damage, e1->getHp(), CHARACTER, ProfileEnemy[7], ProfileEnemy[6]);
		setNumber(e1->getHp(), ProfileEnemy[8], ProfileEnemy[9], ProfileEnemy[10]);
		ProfileEnemy[11]->SetDraw(false);
		ProfileEnemy[12]->SetDraw(false);
		int hitrate = 100 - e1->getEva() + hit;
		if (hitrate > 100) { hitrate = 100; }
		setNumber(hitrate, ArrowHitRate[2], ArrowHitRate[3], ArrowHitRate[4]);
		for (int i = 0; i < 6; i++)
		{
			ProfileEnemy[i + 14]->SetDraw(false);
		}
		for (int i = 0; i < e1->getBuffVector().size(); i++)
		{
			int type = e1->getBuffVector()[i].getType();
			int mode;
			if (e1->getBuffVector()[i].getEffect() > 0) { mode = 0; }
			else { mode = 1; }
			int index = i + 14;
			if (!ProfileEnemy[index]->GetDraw() && index < 20)
			{
				ProfileEnemy[index]->setSprite("../Resource/Texture/UIicon/UI_Buff.png", 2, 6, 0, type);
				ProfileEnemy[index]->SetDraw(true);
			}
		}
	}
}
void UIManager::closeProfileUI()
{
	for (int i = 0; i < 20; i++)
	{
		ProfilePlayer[i]->SetDraw(false);
		ProfileEnemy[i]->SetDraw(false);
	}
	for (int i = 0; i < 5; i++)
	{
		ArrowHitRate[i]->SetDraw(false);
	}
}
void UIManager::showPauseUI()
{
	for (int i = 0; i < 5; i++)
	{
		PauseMenu[i]->SetDraw(true);
	}
}
void UIManager::closePauseUI()
{
	for (int i = 0; i < 5; i++)
	{
		PauseMenu[i]->SetDraw(false);
	}
}

void UIManager::showMoveMenuUI(Unit* unit, Camera* camera)
{
	for (int i = 0; i < 4; i++)
	{
		MoveMenuUI[i]->SetDraw(true);
		MoveMenuUI[i]->SetClickable(true);
	}

	MoveMenuUI[0]->setSprite("../Resource/Texture/UIicon/moveicon.png", 2, 1, 0, 0);
	MoveMenuUI[0]->setButtontype(UI_MOVE);
	MoveMenuUI[1]->setSprite("../Resource/Texture/UIicon/atkicon.png", 2, 1, 0, 0);
	MoveMenuUI[1]->setButtontype(UI_ATK);
	Submarine* s1 = dynamic_cast<Submarine*>(unit);
	if (s1 != nullptr && s1->getDraw() == true)
	{
		if (s1->getMovecost() <= 0)
		{
			MoveMenuUI[0]->SetClickable(false);
			MoveMenuUI[0]->SetColorOffset(-0.2, -0.2, -0.2);
		}
		if (s1->getAtkcost() <= 0)
		{
			MoveMenuUI[1]->SetClickable(false);
			MoveMenuUI[1]->SetColorOffset(-0.2, -0.2, -0.2);
		}
		MoveMenuUI[2]->setSprite("../Resource/Texture/UIicon/SISub0.png", 2, 1, 0, 0);
	}
	else
	{
		MoveMenuUI[2]->setSprite(GameDataStorage::GetInstance()->getSkillIcon(unit->getName()), 2, 1, 0, 0);
	}
	MoveMenuUI[2]->setButtontype(UI_SKILL);
	MoveMenuUI[3]->setSprite("../Resource/Texture/UIicon/infoicon.png", 2, 1, 0, 0);
	MoveMenuUI[3]->setButtontype(UI_INFO);

	Character* c1 = dynamic_cast<Character*>(unit);
	if (c1 != nullptr && c1->getDraw() == true)
	{
		if (c1->getCD() > 0)
		{
			MoveMenuUI[4]->SetDraw(true);
		}
		MoveMenuUI[4]->setSprite("../Resource/Texture/UINumber.png", 1, 10, 1, c1->getCD());

		if (c1->getMovecost() <= 0)
		{
			MoveMenuUI[0]->SetClickable(false);
			MoveMenuUI[0]->SetColorOffset(-0.2, -0.2, -0.2);
		}
		if (c1->getAtkcost() <= 0)
		{
			MoveMenuUI[1]->SetClickable(false);
			MoveMenuUI[1]->SetColorOffset(-0.2, -0.2, -0.2);
		}
	}


	glm::vec2 convertpos = FindNewCenterUI(unit, camera, 0.5f, 1.0f);
	MoveMenuUI[0]->SetPosition(glm::vec3(convertpos.x, convertpos.y - MoveMenuUI[0]->GetSize().y / 2, 0.0f));

	convertpos = FindNewCenterUI(unit, camera, 1.0f, 0.5f);
	MoveMenuUI[1]->SetPosition(glm::vec3(convertpos.x + MoveMenuUI[0]->GetSize().x / 2, convertpos.y, 0.0f));

	convertpos = FindNewCenterUI(unit, camera, 0.5f, 0.0f);
	MoveMenuUI[2]->SetPosition(glm::vec3(convertpos.x, convertpos.y + MoveMenuUI[0]->GetSize().y / 2, 0.0f));

	convertpos = FindNewCenterUI(unit, camera, 0.0f, 0.5f);
	MoveMenuUI[3]->SetPosition(glm::vec3(convertpos.x - MoveMenuUI[0]->GetSize().x / 2, convertpos.y, 0.0f));

	convertpos = FindNewCenterUI(unit, camera, 0.5f, 0.0f);
	MoveMenuUI[4]->SetPosition(glm::vec3(convertpos.x, convertpos.y + MoveMenuUI[0]->GetSize().y / 2, 0.0f));
	
	if (unit->getCD() > 0 && s1 == nullptr)
	{
		MoveMenuUI[2]->SetClickable(false);
		MoveMenuUI[2]->SetColorOffset(-0.2,-0.2,-0.2);
	}
	else
	{
		MoveMenuUI[2]->SetClickable(true);
	}
}
void UIManager::closeMoveMenuUI()
{
	for (int i = 0; i < 7; i++)
	{
		MoveMenuUI[i]->SetDraw(false);
	}
}

void UIManager::showSkillMenuUI(Unit* unit, Camera* camera)
{
	MoveMenuUI[1]->SetTexture("../Resource/Texture/UIicon/Cancelicon.png");
	MoveMenuUI[3]->SetTexture("../Resource/Texture/UIicon/Useicon.png");
	MoveMenuUI[1]->setButtontype(UI_CANCEL);
	MoveMenuUI[3]->setButtontype(UI_USE);

	glm::vec2 convertpos = FindNewCenterUI(unit, camera, 1.0f, 0.5f);
	MoveMenuUI[1]->SetPosition(glm::vec3(convertpos.x + MoveMenuUI[0]->GetSize().x / 2, convertpos.y, 0.0f));

	convertpos = FindNewCenterUI(unit, camera, 0.0f, 0.5f);
	MoveMenuUI[3]->SetPosition(glm::vec3(convertpos.x - MoveMenuUI[0]->GetSize().x / 2, convertpos.y, 0.0f));

	MoveMenuUI[0]->SetDraw(false);
	MoveMenuUI[1]->SetDraw(true);
	MoveMenuUI[1]->SetClickable(true);
	MoveMenuUI[2]->SetDraw(false);
	MoveMenuUI[3]->SetDraw(true);
	MoveMenuUI[3]->SetClickable(true);
	MoveMenuUI[4]->SetDraw(false);
	MoveMenuUI[5]->SetDraw(false);
	MoveMenuUI[6]->SetDraw(false);
}
void UIManager::showSubSkillUI(Submarine* unit, Camera* camera)
{
	MoveMenuUI[0]->SetTexture("../Resource/Texture/UIicon/SISub2.png");
	MoveMenuUI[1]->SetTexture("../Resource/Texture/UIicon/SISub3.png");
	MoveMenuUI[3]->SetTexture("../Resource/Texture/UIicon/SISub1.png");
	MoveMenuUI[0]->setButtontype(UI_SUBSKILL2);
	MoveMenuUI[1]->setButtontype(UI_SUBSKILL3);
	MoveMenuUI[3]->setButtontype(UI_SUBSKILL1);

	glm::vec2 convertpos = FindNewCenterUI(unit, camera, 0.5f, 1.0f);
	MoveMenuUI[0]->SetPosition(glm::vec3(convertpos.x, convertpos.y - MoveMenuUI[0]->GetSize().y / 2, 0.0f));

	convertpos = FindNewCenterUI(unit, camera, 1.0f, 0.5f);
	MoveMenuUI[1]->SetPosition(glm::vec3(convertpos.x + MoveMenuUI[0]->GetSize().x / 2, convertpos.y, 0.0f));

	convertpos = FindNewCenterUI(unit, camera, 0.0f, 0.5f);
	MoveMenuUI[3]->SetPosition(glm::vec3(convertpos.x - MoveMenuUI[0]->GetSize().x / 2, convertpos.y, 0.0f));

	convertpos = FindNewCenterUI(unit, camera, 0.5f, 1.0f);
	MoveMenuUI[4]->SetPosition(glm::vec3(convertpos.x, convertpos.y - MoveMenuUI[0]->GetSize().y / 2, 0.0f));

	convertpos = FindNewCenterUI(unit, camera, 1.0f, 0.5f);
	MoveMenuUI[5]->SetPosition(glm::vec3(convertpos.x + MoveMenuUI[0]->GetSize().x / 2, convertpos.y, 0.0f));

	convertpos = FindNewCenterUI(unit, camera, 0.0f, 0.5f);
	MoveMenuUI[6]->SetPosition(glm::vec3(convertpos.x - MoveMenuUI[0]->GetSize().x / 2, convertpos.y, 0.0f));

	int allCD[3] = { unit->getCD(),unit->getSubCD(0),unit->getSubCD(1) };
	int Skillindex[3] = { 3,0,1 };
	int numberIndex[3] = { 6,4,5 };
	for (int i=0;i<3;i++)
	{
		if (allCD[i] > 0) {
			MoveMenuUI[Skillindex[i]]->SetClickable(false);
			MoveMenuUI[Skillindex[i]]->SetColorOffset(-0.2, -0.2, -0.2);
			MoveMenuUI[numberIndex[i]]->setSprite("../Resource/Texture/UINumber.png", 1, 10, 1, allCD[i]);
			MoveMenuUI[numberIndex[i]]->SetDraw(true);
		}
		else {
			MoveMenuUI[Skillindex[i]]->SetClickable(true);
		}
	}

	MoveMenuUI[0]->SetDraw(true);
	MoveMenuUI[1]->SetDraw(true);
	MoveMenuUI[2]->SetDraw(false);
	MoveMenuUI[3]->SetDraw(true);
}

void UIManager::showInfoUI(Unit* unit)
{
	Character* c1 = dynamic_cast<Character*>(unit);
	if (c1 != nullptr && c1->getDraw() == true)
	{
		if (unit->getName() == "Ash")
		{
			interFaceUI[8]->setSprite("../Resource/Texture/Ash_Info.png", 1, 1, 0, 0);
		}
		else if (unit->getName() == "Might")
		{
			interFaceUI[8]->setSprite("../Resource/Texture/Might_Info.png", 1, 1, 0, 0);
		}
		else if (unit->getName() == "Gray")
		{
			interFaceUI[8]->setSprite("../Resource/Texture/Gray_Info.png", 1, 1, 0, 0);
		}
		else if (unit->getName() == "Todd")
		{
			interFaceUI[8]->setSprite("../Resource/Texture/Todd_Info.png", 1, 1, 0, 0);
		}
		else if (unit->getName() == "Wong")
		{
			interFaceUI[8]->setSprite("../Resource/Texture/Wong_Info.png", 1, 1, 0, 0);
		}
		else if (unit->getName() == "Kim")
		{
			interFaceUI[8]->setSprite("../Resource/Texture/Kim_Info.png", 1, 1, 0, 0);
		}
	}
	Submarine* s1 = dynamic_cast<Submarine*>(unit);
	if (s1 != nullptr && s1->getDraw() == true)
	{

	}
	for (int i = 8 ; i < 11 ; i++)
	{
		interFaceUI[i]->SetDraw(true);
	}
}
void UIManager::closeInfoUI()
{
	for (int i = 8; i < 11; i++)
	{
		interFaceUI[i]->SetDraw(false);
	}
}
void UIManager::showPassTurnSBUI()
{
	interFaceUI[7]->SetDraw(true);
}
void UIManager::closePassTurnSBUI()
{
	interFaceUI[7]->SetDraw(false);
}

void UIManager::setEnemyNumber(int amount)
{
	setNumber(amount,nullptr,interFaceUI[2], interFaceUI[3]);
}

void UIManager::setNumber(int amount, UIObject* left, UIObject* middle, UIObject* right)
{
	bool NEW = false;
	if (left == nullptr)
	{
		left = new UIObject();
		NEW = true;
	}
	if (amount <= 0)
	{
		amount = 0;
	}
	int x = amount / 100;
	left->setSprite("../Resource/Texture/UINumber.png",1,10,1,x);
	if ((amount / 100) <= 0)
	{
		left->SetDraw(false);
	}
	x = (amount / 10) % 10;
	middle->setSprite("../Resource/Texture/UINumber.png", 1, 10, 1, x);
	x = amount % 10;
	right->setSprite("../Resource/Texture/UINumber.png", 1, 10, 1, x);
	if (NEW == true)
	{
		delete left;
	}
}
void UIManager::setStatusbar(int curr,int max,int type, UIObject* bar, UIObject* maxbar)
{
	float radio = curr / (max * 1.0f);
	bar->SetSize(maxbar->GetSize().x * radio, maxbar->GetSize().y);
	float diff = (maxbar->GetSize().x - bar->GetSize().x) / 2.0f;
	bar->SetPosition(maxbar->GetPosition());
	if (type == CHARACTER || type == SUBMARINE)
	{
		bar->SetPosition(glm::vec3(bar->GetPosition().x - diff, bar->GetPosition().y, bar->GetPosition().z));
	}
	else if(type == ENEMY)
	{
		bar->SetPosition(glm::vec3(bar->GetPosition().x + diff, bar->GetPosition().y, bar->GetPosition().z));
	}
}

void UIManager::SetQuestgoal(int type)
{
	if (type == ELIMINATE)
	{
		interFaceUI[6]->SetTexture("../Resource/Texture/UIicon/Questtemp1.png");
		interFaceUI[1]->SetDraw(true);
		interFaceUI[2]->SetDraw(true);
		interFaceUI[3]->SetDraw(true);
	}
	else if(type == REACHGOAL)
	{
		interFaceUI[6]->SetTexture("../Resource/Texture/UIicon/Questtemp2.png");
	}
	else if (type == TARGET)
	{
		interFaceUI[6]->SetTexture("../Resource/Texture/UIicon/Questtemp3.png");
	}
}

void UIManager::setatkanimcount(int amount)
{
	countdownatkanim = amount;
}

bool UIManager::AtkAnimFinish()
{
	if (countdownatkanim == 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool UIManager::PassTurnAnimFinish()
{
	if (countdownphase == 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

UIObject* UIManager::getCutInUI(int index)
{
	return cutInUI[index];
}

UIObject* UIManager::getProfilePlayerUI(int index)
{
	return ProfilePlayer[index];
}

UIObject* UIManager::getProfileEnemyUI(int index)
{
	return ProfileEnemy[index];
}

UIObject* UIManager::getArrowHitRate(int index)
{
	return ArrowHitRate[index];
}

UIObject* UIManager::getMoveMenuUI(int index)
{
	return MoveMenuUI[index];
}

UIObject* UIManager::getPauseMenuUI(int index)
{
	return PauseMenu[index];
}

UIObject* UIManager::getInterFaceUI(int index)
{
	return interFaceUI[index];
}