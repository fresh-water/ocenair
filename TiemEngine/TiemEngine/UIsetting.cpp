#include "UISetting.h"
#include <sstream>

void WrapSliderBall(UIObject* button)
{
	float posx = (float)GameEngine::GetInstance()->ConvertX(GameEngine::GetInstance()->getCurMousePos().x);
	float posy = (float)GameEngine::GetInstance()->ConvertY(GameEngine::GetInstance()->getCurMousePos().y);
	posx /= (GameEngine::GetInstance()->GetWindowWidth()/2.0f);
	posy /= (GameEngine::GetInstance()->GetWindowHeight()/2.0f);
	button->SetPosition(glm::vec3(button->GetPosition().x, posy,button->GetPosition().z));
	
	int currsound = -((-0.06f - 0.45f - posy) / (0.9)) * 128;
	if (button->getButtontype() == UI_SOUND_MASTER)
	{
		GameEngine::GetInstance()->setMasterSound(currsound);
	}
	else if (button->getButtontype() == UI_SOUND_MUSIC)
	{
		GameEngine::GetInstance()->setMusicSound(currsound);
	}
	else if (button->getButtontype() == UI_SOUND_EFFECT)
	{
		GameEngine::GetInstance()->setEffectSound(currsound);
	}
}
void Resave(UIObject* button) {
	GameDataStorage::GetInstance()->ResaveFile();
	GameData::GetInstance()->gGameStateNext = GameState::GS_KMUTT;
}
void SetSpeedBox(UIObject* button) {

	if (button->getButtontype() == UI_SPEED_1) {
		GameEngine::GetInstance()->setSpeedLevel(1);
		
	}
	else if (button->getButtontype() == UI_SPEED_2) {
		GameEngine::GetInstance()->setSpeedLevel(2);
	}
	else if (button->getButtontype() == UI_SPEED_3) {
		GameEngine::GetInstance()->setSpeedLevel(3);
		
	}
}

UISetting::UISetting()
{

}

void UISetting::Init()
{
	//////////////////////////INTERFACEUI////////////////////////
	{
		for (int i = 0; i < 19; i++)
		{
			SettingUI[i] = new UIObject();
			SettingUI[i]->SetMode(1);
			SettingUI[i]->SetClickable(true);
			SettingUI[i]->SetDraw(false);
		}
		//black background
		SettingUI[0]->SetSize(2.0f, 2.0f);
		SettingUI[0]->SetPosition(glm::vec3(0, 0, 0));
		SettingUI[0]->SetMode(0);
		SettingUI[0]->SetColor(0.0f, 0.0f, 0.0f, 0.5f);
		SettingUI[0]->SetClickable(false);
		//background
		SettingUI[1]->SetSize(1.5, -2.7);
		SettingUI[1]->SetPosition(glm::vec3(0, -0.05, 0));
		SettingUI[1]->SetTexture("../Resource/Texture/menubase.png");
		SettingUI[1]->SetClickable(false);

		//MasterV slider bar
		SettingUI[2]->SetSize(0.05, 0.9);
		SettingUI[2]->SetPosition(glm::vec3(-0.50f, -0.06f, 0));
		SettingUI[2]->SetTexture("../Resource/Texture/menusoundbar.png");
		SettingUI[2]->setButtontype(UI_SOUND_SLIDER);
		SettingUI[2]->setAnotherButton(SettingUI[5]);
		SettingUI[2]->SetActiveFn(WrapSliderBall);
		SettingUI[2]->SetClickable(true);

		//MusicV slider bar
		SettingUI[3]->SetSize(0.05, 0.9);
		SettingUI[3]->SetPosition(glm::vec3(-0.30f, -0.06f, 0));
		SettingUI[3]->SetTexture("../Resource/Texture/menusoundbar.png");
		SettingUI[3]->setButtontype(UI_SOUND_SLIDER);
		SettingUI[3]->setAnotherButton(SettingUI[6]);
		SettingUI[3]->SetActiveFn(WrapSliderBall);
		SettingUI[3]->SetClickable(true);

		//SoundV slider bar
		SettingUI[4]->SetSize(0.05, 0.9);
		SettingUI[4]->SetPosition(glm::vec3(-0.10f, -0.06f, 0));
		SettingUI[4]->SetTexture("../Resource/Texture/menusoundbar.png");
		SettingUI[4]->setButtontype(UI_SOUND_SLIDER);
		SettingUI[4]->setAnotherButton(SettingUI[7]);
		SettingUI[4]->SetActiveFn(WrapSliderBall);
		SettingUI[4]->SetClickable(true);

		//MasterV slider ball
		SettingUI[5]->SetSize(0.1, -0.08);
		float ypos = ((GameEngine::GetInstance()->getMasterSound() / 128.0f)* (0.995)) - 0.06f - 0.45f;
		SettingUI[5]->SetPosition(glm::vec3(-0.5f, ypos, 0));
		SettingUI[5]->SetTexture("../Resource/Texture/menusoundcon.png");
		SettingUI[5]->setButtontype(UI_SOUND_MASTER);
		SettingUI[5]->SetClickable(false);

		//MusicV slider ball
		SettingUI[6]->SetSize(0.1, -0.08);
		ypos = ((GameEngine::GetInstance()->getRealMusicSound() / 128.0f) * (0.995)) - 0.06f - 0.45f;
		SettingUI[6]->SetPosition(glm::vec3(-0.3f, ypos, 0));
		SettingUI[6]->SetTexture("../Resource/Texture/menusoundcon.png");
		SettingUI[6]->setButtontype(UI_SOUND_MUSIC);
		SettingUI[6]->SetClickable(false);

		//SoundV slider ball
		SettingUI[7]->SetSize(0.1, -0.08);
		ypos = ((GameEngine::GetInstance()->getRealEffectSound() / 128.0f) * (0.995)) - 0.06f - 0.45f;
		SettingUI[7]->SetPosition(glm::vec3(-0.1f, ypos, 0));
		SettingUI[7]->SetTexture("../Resource/Texture/menusoundcon.png");
		SettingUI[7]->setButtontype(UI_SOUND_EFFECT);
		SettingUI[7]->SetClickable(false);

		//Back button
		SettingUI[8]->SetSize(0.25, -0.20);
		SettingUI[8]->SetPosition(glm::vec3(0.45f, -0.60f, 0));
		SettingUI[8]->setSprite("../Resource/Texture/CSback.png",2,1,0,0);
		SettingUI[8]->SetClickOnAnim(true);
		SettingUI[8]->SetClickable(true);
		SettingUI[8]->setButtontype(UI_BACK_OPTION);

		//normal speed off
		SettingUI[9]->SetSize(0.15, -0.18);
		SettingUI[9]->SetPosition(glm::vec3(0.1f, 0.32f, 0));
		SettingUI[9]->SetTexture("../Resource/Texture/UIBoxNS.png");
		SettingUI[9]->SetActiveFn(SetSpeedBox);
		SettingUI[9]->setAnotherButton(SettingUI[12]);
		SettingUI[9]->SetClickable(true);
		SettingUI[9]->setButtontype(UI_SPEED_1);
		
		//quite speed off
		SettingUI[10]->SetSize(0.15, -0.18);
		SettingUI[10]->SetPosition(glm::vec3(0.30f, 0.32f, 0));
		SettingUI[10]->SetTexture("../Resource/Texture/UIBoxNS.png");
		SettingUI[10]->SetActiveFn(SetSpeedBox);
		SettingUI[10]->SetClickable(true);
		SettingUI[10]->setAnotherButton(SettingUI[12]);
		SettingUI[10]->setButtontype(UI_SPEED_2);
		
		//fast speed off
		SettingUI[11]->SetSize(0.15, -0.18);
		SettingUI[11]->SetPosition(glm::vec3(0.5f, 0.32f, 0));
		SettingUI[11]->SetTexture("../Resource/Texture/UIBoxNS.png");
		SettingUI[11]->SetActiveFn(SetSpeedBox);
		SettingUI[11]->SetClickable(true);
		SettingUI[11]->setAnotherButton(SettingUI[12]);
		SettingUI[11]->setButtontype(UI_SPEED_3);
		////////////////////////////////////////////////////////////////////
		//Speed Mark
		SettingUI[12]->SetSize(0.15, -0.18);
		SettingUI[12]->SetPosition(glm::vec3(0.1f, 0.32f, 0));
		SettingUI[12]->SetTexture("../Resource/Texture/UIBoxS.png");
		SettingUI[12]->SetClickable(false);
		SettingUI[12]->setButtontype(UI_SPEED);
		////////////////////////////////////////////////////////////////////
		//Reset
		SettingUI[13]->SetSize(0.40, -0.2);
		SettingUI[13]->SetPosition(glm::vec3(0.3f, -0.05f, 0));
		SettingUI[13]->setButtontype(UI_RESET);
		SettingUI[13]->setSprite("../Resource/Texture/CSreset.png",2,1,0,0);
		SettingUI[13]->SetClickable(true);
		SettingUI[13]->SetClickOnAnim(true);
		//Tutorial
		SettingUI[14]->SetSize(0.40, -0.2);
		SettingUI[14]->SetPosition(glm::vec3(0.3f, -0.3f, 0));
		SettingUI[14]->setSprite("../Resource/Texture/CStutorial.png", 2, 1, 0, 0);
		SettingUI[14]->setButtontype(UI_TUTORIAL);
		SettingUI[14]->SetClickable(true);
		SettingUI[14]->SetClickOnAnim(true);
		//BlackBG
		SettingUI[15]->SetSize(2.0f, 2.0f);
		SettingUI[15]->SetPosition(glm::vec3(0, 0, 0));
		SettingUI[15]->SetMode(0);
		SettingUI[15]->SetColor(0.0f, 0.0f, 0.0f, 0.5f);
		SettingUI[15]->SetClickable(false);
		//ResaveBG
		SettingUI[16]->SetSize(0.75, -0.75);
		SettingUI[16]->SetPosition(glm::vec3(0.0f, 0.0f, 0));
		SettingUI[16]->SetTexture("../Resource/Texture/CSresavebg.png");
		SettingUI[16]->SetClickable(false);
		//Reset
		SettingUI[17]->SetSize(0.30, -0.20);
		SettingUI[17]->SetPosition(glm::vec3(-0.17f, -0.2f, 0));
		SettingUI[17]->setSprite("../Resource/Texture/CSreset.png", 2, 1, 0, 0);
		SettingUI[17]->SetActiveFn(Resave);
		SettingUI[17]->SetClickable(true);
		SettingUI[17]->SetClickOnAnim(true);
		//back
		SettingUI[18]->SetSize(0.30, -0.20);
		SettingUI[18]->SetPosition(glm::vec3(0.17f, -0.2f, 0));
		SettingUI[18]->setSprite("../Resource/Texture/CSback.png", 2, 1, 0, 0);
		SettingUI[18]->setButtontype(UI_BACK_RESAVE);
		SettingUI[18]->SetClickable(true);
		SettingUI[18]->SetClickOnAnim(true);
	}
	//////////////////////////TUTORIAL///////////////////////////
	{
		for (int i = 0; i < 5; i++)
		{
			TutorialUI[i] = new UIObject();
			TutorialUI[i]->SetMode(1);
			TutorialUI[i]->SetClickable(false);
			TutorialUI[i]->SetDraw(false);
		}
		//black background
		TutorialUI[0]->SetSize(2.0f, 2.0f);
		TutorialUI[0]->SetPosition(glm::vec3(0, 0, 0));
		TutorialUI[0]->SetMode(0);
		TutorialUI[0]->SetColor(0.0f, 0.0f, 0.0f, 0.5f);
		TutorialUI[0]->SetClickable(false);
		//Tutorial
		TutorialUI[1]->SetSize(1.5, -1.5);
		TutorialUI[1]->SetPosition(glm::vec3(0, 0, 0));
		TutorialUI[1]->SetTexture("../Resource/Texture/UIgameplay/tutorial1.png");
		TutorialUI[1]->SetClickable(false);
		//Back
		TutorialUI[2]->SetSize(0.3, -0.15);
		TutorialUI[2]->SetPosition(glm::vec3(-0.0f, -0.8f, 0));
		TutorialUI[2]->setSprite("../Resource/Texture/CSback.png",2,1,1,1);
		TutorialUI[2]->setButtontype(UI_BACK_TURORIAL);
		TutorialUI[2]->SetClickable(true);
		TutorialUI[2]->SetClickOnAnim(true);
		//ArrowNext
		TutorialUI[3]->SetSize(0.2, 0.3);
		TutorialUI[3]->SetPosition(glm::vec3(0.8f, 0, 0));
		TutorialUI[3]->SetTexture("../Resource/Texture/UIgameplay/Arrow_Green.png");
		TutorialUI[3]->setButtontype(UI_ARROW_RIGHT_TURORIAL);
		TutorialUI[3]->SetClickable(true);
		//ArrowBack
		TutorialUI[4]->SetSize(-0.2, 0.3);
		TutorialUI[4]->SetPosition(glm::vec3(-0.8f, 0, 0));
		TutorialUI[4]->SetTexture("../Resource/Texture/UIgameplay/Arrow_Green.png");
		TutorialUI[4]->setButtontype(UI_ARROW_LEFT_TURORIAL);
		TutorialUI[4]->SetClickable(true);
	}
}
void UISetting::showTutorialUI(int index)
{
	for (int i = 0; i < 5; i++)
	{
		TutorialUI[i]->SetDraw(true);
	}
	currentTutorial = index;
	if (index == 1){
		TutorialUI[1]->SetTexture("../Resource/Texture/UIgameplay/tutorial1.png");
		TutorialUI[4]->SetDraw(false);
	}
	else if(index == 2){
		TutorialUI[1]->SetTexture("../Resource/Texture/UIgameplay/tutorial2.png");
	}
	else if (index == 3) {
		TutorialUI[1]->SetTexture("../Resource/Texture/UIgameplay/tutorial3.png");
		TutorialUI[3]->SetDraw(false);
	}
}
void UISetting::closeTutorialUI()
{
	for (int i = 0; i < 5; i++)
	{
		TutorialUI[i]->SetDraw(false);
	}
}
int UISetting::getCurrentTutorial()
{
	return currentTutorial;
}
void UISetting::showAudioSUI() {
	for (int i = 0; i < 15; i++)
	{
		SettingUI[i]->SetDraw(true);
	}
	//Check speed value, set UI appearance.
	if (GameEngine::GetInstance()->getSpeedLevel() == 1) {
		SettingUI[12]->SetPosition(glm::vec3(0.1f, 0.32f, 0));
	}
	else if (GameEngine::GetInstance()->getSpeedLevel() == 2) {
		SettingUI[12]->SetPosition(glm::vec3(0.30f, 0.32f, 0));
	}
	else if (GameEngine::GetInstance()->getSpeedLevel() == 3) {
		SettingUI[12]->SetPosition(glm::vec3(0.5f, 0.32f, 0));
	}
}
void UISetting::showResaveMenu() {
	for (int i = 15; i < 19; i++)
	{
		SettingUI[i]->SetDraw(true);
	}
}
void UISetting::closeAudioSUI() {
	for (int i = 0; i < 19; i++)
	{
		SettingUI[i]->SetDraw(false);
	}
}
void UISetting::closeResaveMenu() {
	for (int i = 15; i < 19; i++)
	{
		SettingUI[i]->SetDraw(false);
	}
}
void UISetting::speedSelect(int speed) {
	SettingUI[12]->SetDraw(true);
	if (speed == 1) {
		//cout << "Game speed: normal" << endl;
		SettingUI[12]->SetPosition(glm::vec3(0.1f, 0.32f, 0));
	}
	else if (speed == 2) {
		//cout << "Game speed: fast" << endl;
		SettingUI[12]->SetPosition(glm::vec3(0.3f, 0.32f, 0));
	}
	else if (speed == 3) {
		//cout << "Game speed: very fast" << endl;
		SettingUI[12]->SetPosition(glm::vec3(0.5f, 0.32f, 0));
	}
}
void UISetting::SkipActivate(bool skip) {
	if (skip == true) {
		SettingUI[13]->SetDraw(true);
		SettingUI[14]->SetDraw(true);
		SettingUI[15]->SetDraw(false);
		SettingUI[16]->SetDraw(false);
	}
	else if (skip == false) {
		SettingUI[13]->SetDraw(false);
		SettingUI[14]->SetDraw(false);
		SettingUI[15]->SetDraw(true);
		SettingUI[16]->SetDraw(true);
	}
}
UIObject* UISetting::getTutorialUI(int index){
	return TutorialUI[index];
}
UIObject* UISetting::getAudioSUI(int index) {
	return SettingUI[index];
}