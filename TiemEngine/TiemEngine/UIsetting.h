#pragma once
#include <vector>
#include "GLRenderer.h"
#include "GameEngine.h"
#include "GameDataStorage.h"
#include "UIObject.h"
#include "Character.h"
#include "Submarine.h"
#include "Enemy.h"
#include "Camera.h"

using namespace std;
class UISetting
{
	UIObject* SettingUI[19];
	UIObject* TutorialUI[5];
	int currentTutorial = 1;
public:
	UISetting();
	void Init();
	void Update();
	void showAudioSUI();
	void closeAudioSUI();
	void showTutorialUI(int index);
	void closeTutorialUI();
	int getCurrentTutorial();
	void showResaveMenu();
	void closeResaveMenu();
	void speedSelect(int speed);
	void SkipActivate(bool skip);


	UIObject* getTutorialUI(int index);
	UIObject* getAudioSUI(int index);
};