#include "Character.h"
#include "Astar.h"

void Character::Init(string name, int hp, int maxhp, int atk, int atkdis, int move, int x, int y, int eva, int def, int cd, string tex,
	int oxygen, int losin, int losout)
{
	Unit::Init(name, hp, maxhp, atk, atkdis, move, x, y, eva, def, cd, tex);
	this->oxygen = oxygen;
	this->LOSin = losin;
	this->LOSout = losout;
	this->atkcost = 0;
	this->movecost = 0;
	for (int i = 0; i < 2; i++)
	{
		miniO2bar[i] = new GameObject;
		miniO2bar[i]->SetSize(80, 7);
		miniO2bar[i]->SetMode(0);
	}
	miniO2bar[0]->SetColor(0, 0.5f, 0.5f, 0.7f);
	miniO2bar[1]->SetColor(0, 1, 1, 0.7f);
}

vector<glm::vec2> Character::ActiveSkill(int** typearr, int sizex, int sizey)
{
	vector<glm::vec2> skillarea;
	glm::vec2 unitpos = this->getBoardPosition();
	if (this->getName() == "Ash")
	{
		skillarea.push_back(unitpos);
	}
	else if (this->getName() == "Todd")
	{
		for (int j = -1; j <= 1; j++)
		{
			for (int i = -1; i <= 1; i++)
			{
				skillarea.push_back(glm::vec2(unitpos.x + i, unitpos.y + j));
			}
		}
	}
	else if (this->getName() == "Might")
	{
		for (int j = -1; j <= 1; j++)
		{
			for (int i = -1; i <= 1; i++)
			{
				skillarea.push_back(glm::vec2(unitpos.x + i, unitpos.y + j));
			}
		}
	}
	else if (this->getName() == "Wong")
	{
		skillarea.push_back(unitpos);
	}
	else if (this->getName() == "Gray")
	{
		skillarea = UnitOpenSpaceAstar(typearr, sizex, sizey, AT_ATK, 2, unitpos);
	}
	else if (this->getName() == "Kim")
	{
		for (int j = -2; j <= 2 ;j++)
		{
			for (int i = -2; i <= 2; i++)
			{
				skillarea.push_back(glm::vec2(unitpos.x+i,unitpos.y+j));
			}
		}
	}
	return skillarea;
}
void Character::skillApply(int select) {
	setOxygen(10);
	setLOS(3,5);
	if (select == ASSAULT) { //increase movement and eva herself.s
		Unit::setHp(30);
		Unit::setAtk(19);
		Unit::setBoardPosition(0, 0);
	}
	else if (select == GUNNER) { //increase accuracy for nearest teammate by support attacking.
		Unit::setHp(30);
		Unit::setAtk(10);
		Unit::setBoardPosition(0, 1);
	}
	else if (select == TANK) {//def buff area for all allies and increase LOS for himself.
		Unit::setHp(30);
		Unit::setAtk(10);
		Unit::setBoardPosition(0, 2);
	}
	else if (select == MEDIC) { //most heal on the group, fater reviving character, can revive anyone in far away more than 1 character.
		Unit::setHp(30);
		Unit::setAtk(10);
		Unit::setBoardPosition(0, 3);
	}
	else if (select == SCOUT) {//using drone open LOS for 3 turns.
		Unit::setHp(30);
		Unit::setAtk(10);
		Unit::setBoardPosition(0, 4);
	}
	else {//Engineer restore oxygen nearest teammate, decrease oxygen for all allies, additional heal
			// the submarine more and more distance than other.
		Unit::setHp(30);
		Unit::setAtk(10);
		Unit::setBoardPosition(0, 5);
	}
}
int Character::getHp() {
	return Unit::getHp();
}
int Character::getAtk() {
	return Unit::getAtk();
}
int Character::getAtkcost(){
	return atkcost;
}
int Character::getMovecost(){
	return movecost;
}
int Character::getLifeLeft(){
	return lifeleft;
}
void Character::setLifeLeft(int life) {
	lifeleft = life;
}
bool Character::getisDown()
{
	return isDown;
}
void Character::setisDown(bool fact) {
	isDown = fact;
}
int Character::getDownTurn() {
	return downturn;
}
void Character::setDownTurn(int turn){
	downturn = turn;
}
void Character::setAtkcost(int amount)
{
	this->atkcost = amount;
}
void Character::setMovecost(int amount)
{
	this->movecost = amount;
}
int Character::getOxygen() {
	if (oxygen > 0)
	{
		return oxygen;
	}
	return 0;
}
void Character::setOxygen(int oxy) {
	if (oxy > 10)
	{
		oxy = 10;
	}
	this->oxygen = oxy;
	updateMiniHpbar();
}
int Character::getLOSin() {
	return LOSin + getBuffEffect(BUFF_LOS);
}
int Character::getLOSout() {
	return LOSout + getBuffEffect(BUFF_LOS);
}
void Character::setLOS(int losin, int losout) {
	this->LOSin = losin;
	this->LOSout = losout;
}
void Character::PassTurn()
{
	Unit::PassTurn();
	if (isDown)
	{
		if (downturn <= 0)
		{
			setDraw(false);
		}
		downturn--;
	}
}
int Character::getMaxHp(){
	return Unit::getMaxHp();
}

void Character::getBuff() {

	vector<Buff> cbuff = Unit::getBuffVector();
	for (int i = 0; i < cbuff.size(); i++) {
		if (cbuff.at(i).getType() == 3) {

		}
	}
}
void Character::readCharacterData(string filename, int index){
	//����	 MaxHp	Attack	MoveDistance  AtkDistance	�ź(%max100)	def	oxygen	�����ͧ���ǧ�	�����ͧ���ǧ�͡	Texture
	string line;
	int atk, movedis, eva, def,atkdis,maxhp,oxygen, row, column;
	string name;
	string texture, atktexture;
	int LOSin, LOSout;
	ifstream MyFile(filename);
	if (!MyFile.is_open()) {
		cout << "Error" << endl;
		return;
	}      
	for (int i=0;i<index;i++)
	{
		getline(MyFile, line);
	}
	MyFile >> name;
	MyFile >> maxhp;
	MyFile >> atk;
	MyFile >> movedis;
	MyFile >> atkdis;
	MyFile >> eva;
	MyFile >> def;
	MyFile >> oxygen;
	MyFile >> LOSin;
	MyFile >> LOSout;
	MyFile >> texture;

	Init(name,maxhp,maxhp,atk,atkdis,movedis,0,0,eva,def,0,texture,oxygen,LOSin,LOSout);

	MyFile >> row;
	MyFile >> column;
	MyFile >> atktexture;

	this->setRow(row);
	this->setCol(column);
	this->setatktexture(atktexture);

	this->setPath(this->getTexture(), row, column);
	this->SetAnimationLoop(0, 0, 4, 100);

	MyFile.close();
}

GameObject* Character::getminiO2bar(int index)
{
	return miniO2bar[index];
}

void Character::updateMiniHpbar()
{
	Unit::updateMiniHpbar();
	if (oxygen <= 0)
	{
		miniO2bar[0]->setDraw(false);
		miniO2bar[1]->setDraw(false);
		return;
	}
	else
	{
		miniO2bar[0]->setDraw(true);
		miniO2bar[1]->setDraw(true);
	}

	float radio = oxygen / (10 * 1.0f);
	miniO2bar[1]->SetSize(miniO2bar[0]->GetSize().x * radio, miniO2bar[0]->GetSize().y);
	float diff = (miniO2bar[0]->GetSize().x - miniO2bar[1]->GetSize().x) / 2.0f;
	miniO2bar[1]->SetPosition(miniO2bar[0]->GetPosition());
	miniO2bar[1]->SetPosition(glm::vec3(miniO2bar[1]->GetPosition().x - diff, miniO2bar[1]->GetPosition().y, miniO2bar[1]->GetPosition().z));
}
void Character::SetPosition(glm::vec3 newPosition)
{
	glm::vec3 currentoffset[2] = { miniO2bar[0]->GetPosition() - GetPosition(),miniO2bar[1]->GetPosition() - GetPosition() };
	Unit::SetPosition(newPosition);
	for (int i = 0; i < 2; i++)
	{
		miniO2bar[i]->SetPosition(newPosition + currentoffset[i]);
	}
}

Character::Character(string name, int hp, int maxhp, int atk, int atkdis, int move, int x, int y, int eva, int def, int cd, string tex,
	int oxygen, int losin, int losout) : Unit(name,hp,maxhp,atk, atkdis,move,x,y,eva,def,cd,tex) {
	this->oxygen = oxygen;
	this->LOSin = losin;
	this->LOSout = losout;
	this->atkcost = 0;
	this->movecost = 0;
}
Character::Character(): Unit()
{
	
}