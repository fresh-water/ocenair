#pragma once

#include "GameObject.h"
#include "SpriteObject.h"
#include "GameDataStorage.h"
#include "GameEngine.h"
#include "Audio.h"
#include <string>
#include <iostream>
#include <fstream>
using namespace std;

class SoundPlayer {
private:
	static SoundPlayer* instance;
	AudioEngine ae;
	SoundEffect se;
	Music mu;
public:
	static SoundPlayer* GetInstance();
	void Init();
	void playSound(string filename);
	void playMusic(string filename);
};