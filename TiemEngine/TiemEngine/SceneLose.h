#pragma once
#include "Level.h"
#include "Board.h"

class SceneLose : public Level
{
private:
	vector<DrawableObject*> objectsList;
	vector<DrawableObject*> UIList;
	Camera* maincamera;
	Board* mainboard;
	UIObject* InterFaceUI[2];
	int mainstate;
	int BOARDSIZEROW;
	int BOARDSIZECOLUMN;
	float CAMERASPEED;
	float CAMERAOFFSET;
public:
	virtual void LevelLoad();
	virtual void LevelInit();
	virtual void LevelUpdate();
	virtual void LevelDraw();
	virtual void LevelFree();
	virtual void LevelUnload();

	virtual void HandleKey(char key);
	virtual void HandleMouse(int type, int x, int y);

	virtual void HandleMouseLeft(int x, int y);
	virtual void HandleMouseRight(int x, int y);
	virtual void HandleMouseMove(int x, int y);
};