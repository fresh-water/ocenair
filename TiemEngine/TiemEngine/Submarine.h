#ifndef SUBMARINE_H
#define SUBMARINE_H
#include "Unit.h"

using namespace std;

class Submarine : public Unit {
private:
	int LOSin;	//LOS = line of sight
	int LOSout;
	bool skillActive = false;
	int movecost;
	int atkcost;
	int SubCD[2];
	//vector<Buff> buff;
public:
	void Init(string name, int hp, int maxhp, int atk, int atkdis, int move, int x, int y, int eva, int def, int cd, string tex
		, int losin, int losout);

	vector<glm::vec2> ActiveSkill1(int** typearr, int sizex, int sizey);
	vector<glm::vec2> ActiveSkill2(int** typearr, int sizex, int sizey);
	vector<glm::vec2> ActiveSkill3(int** typearr, int sizex, int sizey);
	int getSubCD(int index);
	void setSubCD(int index,int amount);
	void PassTurn();
	
	int getHp();
	int getAtk();
	int getAtkcost();
	int getMovecost();
	void setAtkcost(int amount);
	void setMovecost(int amount);
	int getLOSin();
	int getLOSout();
	void setLOS(int losin, int losout);

	vector<glm::vec2> HealOxygen(int distant);

	void readCharacterData(string filename,int index);

	virtual void SetAnimation(int anim);

	Submarine(string name, int hp, int maxhp, int atk, int atkdis, int move, int x, int y, int eva, int def, int cd, string tex
		, int losin, int losout);
	Submarine();
};
#endif
