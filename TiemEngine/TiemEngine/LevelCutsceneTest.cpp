#include "LevelCutsceneTest.h"

void Skip(UIObject* button)
{
	cout << "Skip cutscene" << endl;
	GameData::GetInstance()->gGameStateNext = GameState::GS_LEVEL3;
}
void LevelCutsceneTest::LevelLoad()
{
	SquareMeshVbo* square = new SquareMeshVbo();
	square->LoadData();
	GameEngine::GetInstance()->AddMesh(SquareMeshVbo::MESH_NAME, square);
}

void LevelCutsceneTest::LevelInit()
{
	maincamera = new Camera;
	maincamera->Init(GameEngine::GetInstance()->GetWindowWidth(), GameEngine::GetInstance()->GetWindowHeight());
	maincamera->SetEnable(false);

	currindex = 1;
	numofimage = 9;

	UIObject * bg = new UIObject();
	bg->SetMode(1);
	bg->SetDraw(true);
	bg->SetSize(2, -2);
	bg->SetPosition(glm::vec3(0.0f, 0.0f, 0.0f));
	bg->SetClickable(false);
	bg->SetTexture("../Resource/Texture/CharacterSelectBG.png");
	UIList.push_back(bg);

	for (int i = 0;i<9;i++)
	{
		cutsceneImage[i] = new UIObject();
		cutsceneImage[i]->SetMode(1);
		cutsceneImage[i]->SetDraw(true);
		cutsceneImage[i]->SetColor(0,0,0,0);
		cutsceneImage[i]->SetClickable(false);
		UIList.push_back(cutsceneImage[i]);
		timescale.push_back(200.0f);
	}

	cutsceneImage[0]->SetSize(2.0, -2.0);
	cutsceneImage[0]->SetPosition(glm::vec3(0.0f, 0.0f, 0.0f));
	cutsceneImage[0]->SetTexture("../Resource/Texture/CutsceneTest/b1.png");
	cutsceneImage[0]->SetDraw(true);

	cutsceneImage[1]->SetSize(2.0, -2.0);
	cutsceneImage[1]->SetPosition(glm::vec3(0.0f, 0.0f, 0.0f));
	cutsceneImage[1]->SetTexture("../Resource/Texture/CutsceneTest/b2.png");

	cutsceneImage[2]->SetSize(2.0, -2.0);
	cutsceneImage[2]->SetPosition(glm::vec3(0.0f, 0.0f, 0.0f));
	cutsceneImage[2]->SetTexture("../Resource/Texture/CutsceneTest/b3.png");

	cutsceneImage[3]->SetSize(2.0, -2.0);
	cutsceneImage[3]->SetPosition(glm::vec3(0.0f, 0.0f, 0.0f));
	cutsceneImage[3]->SetTexture("../Resource/Texture/CutsceneTest/b4.png");

	cutsceneImage[4]->SetSize(2.0, -2.0);
	cutsceneImage[4]->SetPosition(glm::vec3(0.0f, 0.0f, 0.0f));
	cutsceneImage[4]->SetTexture("../Resource/Texture/CutsceneTest/b5.png");

	cutsceneImage[5]->SetSize(2.0, -2.0);
	cutsceneImage[5]->SetPosition(glm::vec3(0.0f, 0.0f, 0.0f));
	cutsceneImage[5]->SetTexture("../Resource/Texture/CutsceneTest/b6.png");

	cutsceneImage[6]->SetSize(2.0, -2.0);
	cutsceneImage[6]->SetPosition(glm::vec3(0.0f, 0.0f, 0.0f));
	cutsceneImage[6]->SetTexture("../Resource/Texture/CutsceneTest/b7.png");

	cutsceneImage[7]->SetSize(2.0, -2.0);
	cutsceneImage[7]->SetPosition(glm::vec3(0.0f, 0.0f, 0.0f));
	cutsceneImage[7]->SetTexture("../Resource/Texture/CutsceneTest/b8.png");

	cutsceneImage[8]->SetSize(2.0, -2.0);
	cutsceneImage[8]->SetPosition(glm::vec3(0.0f, 0.0f, 0.0f));
	cutsceneImage[8]->SetTexture("../Resource/Texture/CutsceneTest/b9.png");

	timescale[8] = 500.0f;

	skipUI = new UIObject();
	skipUI->SetMode(1);
	skipUI->SetDraw(true);
	skipUI->SetSize(0.15, -0.08);
	skipUI->SetPosition(glm::vec3(0.9f, 0.9f, 0.0f));
	skipUI->SetClickable(true);
	skipUI->SetActiveFn(Skip);
	skipUI->SetClickOnAnim(true);
	skipUI->setSprite("../Resource/Texture/CSdeploy.png",2,1,0,0);
	UIList.push_back(skipUI);
}

void LevelCutsceneTest::LevelUpdate()
{
	maincamera->CameraDrawArea();

	if (currenttime <= 1.0f)
	{
		currenttime += 1 / timescale[currindex];
		cutsceneImage[currindex]->SetColor(0, 0, 0, currenttime);
	}

	if (currenttime > 1.0f)
	{
		if (currindex >= numofimage)
		{
			GameData::GetInstance()->gGameStateNext = GameState::GS_LEVEL3;
			currindex = numofimage - 1;
		}
		//cutsceneImage[currindex]->SetDraw(true);
		currindex++;
		currenttime = 0.0f;
	}

	int deltaTime = GameEngine::GetInstance()->GetDeltaTime();
	for (DrawableObject* obj : objectsList) {
		obj->Update(deltaTime);
	}
}

void LevelCutsceneTest::LevelDraw()
{
	GameEngine::GetInstance()->Render(objectsList);
	GameEngine::GetInstance()->GetRenderer()->RenderUI(UIList);
}

void LevelCutsceneTest::LevelFree()
{
	for (DrawableObject* obj : objectsList) {
		if (obj != nullptr)
		{
			delete obj;
		}
	}
	objectsList.clear();
	for (DrawableObject* obj : UIList) {
		delete obj;
	}
	UIList.clear();
}

void LevelCutsceneTest::LevelUnload()
{
	GameEngine::GetInstance()->ClearMesh();
}

void LevelCutsceneTest::HandleKey(char key)
{
	switch (key)
	{
	case 'a':
		break;
	case 'd':
		break;
	case 'w':
		break;
	case 'I':
		break;
	case 'O':
		break;

	case 'E': GameData::GetInstance()->gGameStateNext = GameState::GS_QUIT; ; break;
	case 'r': GameData::GetInstance()->gGameStateNext = GameState::GS_RESTART; ; break;
	case 'e': GameData::GetInstance()->gGameStateNext = GameState::GS_WIN; ; break;		//GS_WIN/GS_LOSE scene loading
	case 's': GameData::GetInstance()->gGameStateNext = GameState::GS_MAINMENU; ; break;
	case 'S':
		break;
	}
}

void LevelCutsceneTest::HandleMouse(int type, int x, int y)
{
	int scrolling = x;
	int realX, realY;
	realX = GameEngine::GetInstance()->ConvertX(x);
	realY = GameEngine::GetInstance()->ConvertY(y);

	if (type == MOUSEMOVE)//move
	{
		HandleMouseMove(realX, realY);
	}
	else if (type == MOUSELEFT)//left
	{
		HandleMouseLeft(realX, realY);
	}
	else if (type == MOUSERIGHT)//right
	{
		HandleMouseRight(realX, realY);
	}
	else if (type == MOUSESCROLL)//scroll
	{
		HandleMouseScroll(scrolling);
	}

}

void LevelCutsceneTest::HandleMouseLeft(int x, int y)
{
	UIObject* ui1 = dynamic_cast<UIObject*>(skipUI);
	if (ui1 != nullptr)
	{
		if (ui1->ClickOn(x, y))
		{
			return;
		}
	}
}
void LevelCutsceneTest::HandleMouseRight(int x, int y)
{
	
}
void LevelCutsceneTest::HandleMouseMove(int x, int y)
{
	bool hitbutton = false;
	for (int i = UIList.size() - 1; i >= 0; i--)
	{
		UIObject* ui1 = dynamic_cast<UIObject*>(UIList[i]);
		if (ui1 != nullptr)
		{
			if (ui1->OnMouseStay(x, y))
			{
				hitbutton = true;
				if (prevbutton != ui1)
				{
					prevbutton = ui1;
					GameEngine::GetInstance()->playSound("../Resource/Sound/Click.mp3");
				}
				return;
			}
		}
	}
	if (hitbutton == false)
	{
		prevbutton = nullptr;
	}
}
void LevelCutsceneTest::HandleMouseScroll(int amount)
{

}

