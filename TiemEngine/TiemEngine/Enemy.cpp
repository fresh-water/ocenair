#include "Enemy.h"

vector<glm::vec2> Enemy::ActiveSkill(int** typearr, int sizex, int sizey)
{
	vector<glm::vec2> skillarea;
	if (this->getName() == "Stingray1")
	{
		cout << this->getName() << "skill" << endl;
	}
	else if (this->getName() == "Shark1")
	{
		cout << this->getName() << "skill" << endl;
	}
	else if (this->getName() == "Normal1")
	{
		cout << this->getName() << "skill" << endl;
	}
	else if (this->getName() == "Worm1")
	{
		cout << this->getName() << "skill" << endl;
	}
	else if (this->getName() == "Stingray2")
	{
		cout << this->getName() << "skill" << endl;
	}
	else if (this->getName() == "Shark2")
	{
		cout << this->getName() << "skill" << endl;
	}
	else if (this->getName() == "Normal2")
	{
		cout << this->getName() << "skill" << endl;
	}
	else if (this->getName() == "Worm2")
	{
		cout << this->getName() << "skill" << endl;
	}
	else if (this->getName() == "FinalBoss")
	{
		cout << this->getName() << "skill" << endl;
	}
	return skillarea;
}
int Enemy::getskillcooldownturn()
{
	return skillcooldownturn;
}
void Enemy::setskillcooldownturn(int amount)
{
	skillcooldownturn = amount;
	if (skillcooldownturn < 0)
	{
		skillcooldownturn = 0;
	}
}

string Enemy::getEnemyType() {
	return "typeOfenemy";
}
void Enemy::setEnemyTargetpos(glm::vec2 targetpos)
{
	if (targetpos != glm::vec2(-1,-1))
	{
		this->targetpos = targetpos;
	}
}
glm::vec2 Enemy::getTargetpos()
{
	return targetpos;
}
void Enemy::clearpath()
{
	path.clear();
}
void Enemy::setEnemypath(vector<glm::vec2> path)
{
	this->path = path;
}
vector<glm::vec2> Enemy::getEnemypath()
{
	return path;
}
void Enemy::followTarget() {
	////not implement
}
void Enemy::readCharacterData(string filename, int index)
{
	//����	 MaxHp	Attack	MoveDistance	AtkDistance	�ź(%max100)	def	Texture
	string line;
	int atk, movedis, eva, def, atkdis, maxhp, row, column;
	string name;
	string texture, atktexture;
	int LOSin, LOSout;
	ifstream MyFile(filename);
	if (!MyFile.is_open()) {
		cout << "Error" << endl;
		return;
	}
	for (int i = 0; i < index; i++)
	{
		getline(MyFile, line);
	}
	MyFile >> name;
	MyFile >> maxhp;
	MyFile >> atk;
	MyFile >> movedis;
	MyFile >> atkdis;
	MyFile >> eva;
	MyFile >> def;
	MyFile >> texture;

	Init(name, maxhp, maxhp, atk, atkdis, movedis, 0, 0, eva, def, 0, texture);

	MyFile >> row;
	MyFile >> column;
	MyFile >> atktexture;

	this->setRow(row);
	this->setCol(column);
	this->setatktexture(atktexture);

	this->setPath(this->getTexture(), row, column);
	this->SetAnimationLoop(0, 0, 4, 100);

	MyFile.close();
}
void Enemy::setFoundPlayer(bool fact)
{
	FoundPlayer = fact;
}
bool Enemy::getFoundPlayer()
{
	return FoundPlayer;
}
void Enemy::setActive(bool fact)
{
	if (fact == true)
	{
		this->SetColorOffset(-0.0,-0.0,-0.0);
	}
	else
	{
		this->SetColorOffset(-0.15, -0.15, -0.15);
	}
	Active = fact;
}
bool Enemy::getActive()
{
	return Active;
}
void Enemy::setinVision(bool fact)
{
	this->inVision = fact;
}
bool Enemy::getinVision()
{
	return this->inVision;
}
void Enemy::Render(glm::mat4 globalModelTransform)
{
	if (inVision == false)
	{
		return;
	}
	Unit::Render(globalModelTransform);
}

Enemy::Enemy(string name, int hp, int maxhp, int atk, int atkdis, int move, int x, int y, int eva, int def, int cd, string tex)
	: Unit(name,hp,maxhp,atk,atkdis,move,x,y,eva,def,cd,tex) {

}
Enemy::Enemy():Unit()
{

}