#include "LevelMapSelect.h"

void BackMapSelection(UIObject* button)
{
	GameData::GetInstance()->gGameStateNext = GameState::GS_MAINMENU;
}

void LevelMapSelect::LevelLoad()
{
	SquareMeshVbo* square = new SquareMeshVbo();
	square->LoadData();
	GameEngine::GetInstance()->AddMesh(SquareMeshVbo::MESH_NAME, square);
}

void LevelMapSelect::LevelInit()
{
	GameDataStorage::GetInstance()->loadSaveFile();
	//GameEngine::GetInstance()->playMusic("../Resource/Sound/testbgm2.mp3");

	maincamera = new Camera;
	maincamera->Init(GameEngine::GetInstance()->GetWindowWidth(), GameEngine::GetInstance()->GetWindowHeight());
	maincamera->SetEnable(false);

	UIObject* bg = new UIObject();
	bg->SetMode(1);
	bg->SetDraw(true);
	bg->SetSize(2, -2);
	bg->SetPosition(glm::vec3(0.0f, 0.0f, 0.0f));
	bg->SetClickable(false);
	bg->SetTexture("../Resource/Texture/MainMenuBG.png");
	UIList.push_back(bg);

	for (int i = 0; i < 16; i++)
	{
		LevelBarUI[i] = new UIObject();
		LevelBarUI[i]->SetMode(1);
		LevelBarUI[i]->SetClickable(false);
		LevelBarUI[i]->SetDraw(true);
		LevelBarUI[i]->SetClickOnAnim(true);
	}
	for (int i = 0; i < 16; i++)
	{
		LevelBarUI[i]->setButtontype(UI_LEVEL_BAR);
	}
	float x = 0.2f;
	LevelBarUI[0]->SetSize(1.5, -0.25);
	LevelBarUI[0]->SetPosition(glm::vec3(x, 0.7f, 0.0f));
	LevelBarUI[0]->setSprite("../Resource/Texture/UIDay1.png",2,1,0,0);

	LevelBarUI[1]->SetSize(1.5, -0.25);
	LevelBarUI[1]->SetPosition(glm::vec3(x, 0.45f, 0.0f));
	LevelBarUI[1]->setSprite("../Resource/Texture/UIDay2.png", 2, 1, 0, 0);

	LevelBarUI[2]->SetSize(1.5, -0.25);
	LevelBarUI[2]->SetPosition(glm::vec3(x, 0.2f, 0.0f));
	LevelBarUI[2]->setSprite("../Resource/Texture/UIDay3.png", 2, 1, 0, 0);

	LevelBarUI[3]->SetSize(1.5, -0.25);
	LevelBarUI[3]->SetPosition(glm::vec3(x, -0.05f, 0.0f));
	LevelBarUI[3]->setSprite("../Resource/Texture/UIDay4.png", 2, 1, 0, 0);

	LevelBarUI[4]->SetSize(1.5, -0.25);
	LevelBarUI[4]->SetPosition(glm::vec3(x, -0.3f, 0.0f));
	LevelBarUI[4]->setSprite("../Resource/Texture/UIDay5.png", 2, 1, 0, 0);

	LevelBarUI[5]->SetSize(1.5, -0.25);
	LevelBarUI[5]->SetPosition(glm::vec3(x, -0.55f, 0.0f));
	LevelBarUI[5]->setSprite("../Resource/Texture/UIDay6.png", 2, 1, 0, 0);

	LevelBarUI[6]->SetSize(1.5, -0.25);
	LevelBarUI[6]->SetPosition(glm::vec3(x, -0.8f, 0.0f));
	LevelBarUI[6]->setSprite("../Resource/Texture/UIDay7.png", 2, 1, 0, 0);

	LevelBarUI[7]->SetSize(1.5, -0.25);
	LevelBarUI[7]->SetPosition(glm::vec3(x, -1.05f, 0.0f));
	LevelBarUI[7]->setSprite("../Resource/Texture/UIDay8.png", 2, 1, 0, 0);

	LevelBarUI[8]->SetSize(1.5, -0.25);
	LevelBarUI[8]->SetPosition(glm::vec3(x, -1.2f, 0.0f));
	LevelBarUI[8]->setSprite("../Resource/Texture/UIDay9.png", 2, 1, 0, 0);

	LevelBarUI[9]->SetSize(1.5, -0.25);
	LevelBarUI[9]->SetPosition(glm::vec3(x, -1.45f, 0.0f));
	LevelBarUI[9]->setSprite("../Resource/Texture/UIDay10.png", 2, 1, 0, 0);

	LevelBarUI[10]->SetSize(1.5, -0.25);
	LevelBarUI[10]->SetPosition(glm::vec3(x, -1.7f, 0.0f));
	LevelBarUI[10]->setSprite("../Resource/Texture/UIDay11.png", 2, 1, 0, 0);

	LevelBarUI[11]->SetSize(1.5, -0.25);
	LevelBarUI[11]->SetPosition(glm::vec3(x, -1.95f, 0.0f));
	LevelBarUI[11]->setSprite("../Resource/Texture/UIDay12.png", 2, 1, 0, 0);

	LevelBarUI[12]->SetSize(1.5, -0.25);
	LevelBarUI[12]->SetPosition(glm::vec3(x, -2.2f, 0.0f));
	LevelBarUI[12]->setSprite("../Resource/Texture/UIDay13.png", 2, 1, 0, 0);

	LevelBarUI[13]->SetSize(1.5, -0.25);
	LevelBarUI[13]->SetPosition(glm::vec3(x, -2.45f, 0.0f));
	LevelBarUI[13]->setSprite("../Resource/Texture/UIDay14.png", 2, 1, 0, 0);

	LevelBarUI[14]->SetSize(1.5, -0.25);
	LevelBarUI[14]->SetPosition(glm::vec3(x, -2.7f, 0.0f));
	LevelBarUI[14]->setSprite("../Resource/Texture/UIDay15.png", 2, 1, 0, 0);

	LevelBarUI[15]->SetSize(1.5, -0.25);
	LevelBarUI[15]->SetPosition(glm::vec3(x, -2.95f, 0.0f));
	LevelBarUI[15]->setSprite("../Resource/Texture/UIDay16.png", 2, 1, 0, 0);

	//LevelBarUI[16]->SetSize(1.5, -0.25);
	//LevelBarUI[16]->SetPosition(glm::vec3(x, -3.2f, 0.0f));
	//LevelBarUI[16]->setSprite("../Resource/Texture/UIDay17.png", 2, 1, 0, 0);

	for (int i = 0; i < 16; i++)
	{
		UIList.push_back(LevelBarUI[i]);
	}
	for (int i=0;i< GameDataStorage::GetInstance()->getUnlocklevelnumber();i++)
	{
		LevelBarUI[i]->SetClickable(true);
	}
	for (int i = 0; i < 16; i++) {
		if (LevelBarUI[i]->GetClickable() == false) {
			LevelBarUI[i]->SetColorOffset(-0.6, -0.6, -0.6);
		}
	}

	UIObject* backUI = new UIObject();
	backUI->SetMode(1);
	backUI->SetDraw(true);
	backUI->SetSize(0.3, -0.15);
	backUI->SetPosition(glm::vec3(-0.8f, -0.8f, 0.0f));
	backUI->SetClickable(true);
	backUI->setSprite("../Resource/Texture/CSback.png",2,1,0,0);
	backUI->SetClickOnAnim(true);
	backUI->SetActiveFn(BackMapSelection);

	UIList.push_back(backUI);

}

void LevelMapSelect::LevelUpdate()
{
	maincamera->CameraDrawArea();
	int deltaTime = GameEngine::GetInstance()->GetDeltaTime();
	for (DrawableObject* obj : objectsList) {
		obj->Update(deltaTime);
	}


}

void LevelMapSelect::LevelDraw()
{
	GameEngine::GetInstance()->Render(objectsList);
	GameEngine::GetInstance()->GetRenderer()->RenderUI(UIList);
}

void LevelMapSelect::LevelFree()
{
	for (DrawableObject* obj : objectsList) {
		if (obj != nullptr)
		{
			delete obj;
		}
	}
	objectsList.clear();
	for (DrawableObject* obj : UIList) {
		delete obj;
	}
	UIList.clear();
}

void LevelMapSelect::LevelUnload()
{
	GameEngine::GetInstance()->ClearMesh();
}

void LevelMapSelect::HandleKey(char key)
{
	switch (key)
	{
	case 'a':
		break;
	case 'd':
		break;
	case 'w':
		if (GameDataStorage::GetInstance()->getCurrlevelnumber() > 16)
		{
			GameDataStorage::GetInstance()->setNextlevelnumber(0);
		}
		else
		{
			GameDataStorage::GetInstance()->setNextlevelnumber(GameDataStorage::GetInstance()->getCurrlevelnumber() + 1);
		}
		cout << "currentlevel = " << GameDataStorage::GetInstance()->getCurrlevelnumber() << endl;
		GameDataStorage::GetInstance()->UpdateSaveFile();
		for (int i = 0; i < GameDataStorage::GetInstance()->getUnlocklevelnumber(); i++)
		{
			LevelBarUI[i]->SetClickable(true);
		}
		break;
	case 'I':
		break;
	case 'o':
		//GameData::GetInstance()->gGameStateNext = GameState::GS_CUTSCENE1; ; break;
		break;

	case 'E': GameData::GetInstance()->gGameStateNext = GameState::GS_QUIT; ; break;
	case 'r': GameData::GetInstance()->gGameStateNext = GameState::GS_RESTART; ; break;
	case 'e': GameData::GetInstance()->gGameStateNext = GameState::GS_WIN; ; break;		//GS_WIN/GS_LOSE scene loading
	case 's': GameData::GetInstance()->gGameStateNext = GameState::GS_MAINMENU; ; break;
	case 'S':
		break;
	}
}

void LevelMapSelect::HandleMouse(int type, int x, int y)
{
	int scrolling = x;
	int realX, realY;
	realX = GameEngine::GetInstance()->ConvertX(x);
	realY = GameEngine::GetInstance()->ConvertY(y);

	if (type == MOUSEMOVE)//move
	{
		HandleMouseMove(realX, realY);
	}
	else if (type == MOUSELEFT)//left
	{
		HandleMouseLeft(realX, realY);
	}
	else if (type == MOUSERIGHT)//right
	{
		HandleMouseRight(realX, realY);
	}
	else if (type == MOUSESCROLL)//scroll
	{
		HandleMouseScroll(scrolling);
	}

}

void LevelMapSelect::HandleMouseLeft(int x, int y)
{
	for (int i = UIList.size()-1; i >= 0; i--)
	{
		UIObject* ui1 = dynamic_cast<UIObject*>(UIList[i]);
		if (ui1 != nullptr && ui1->GetClickable() == true)
		{
			if (ui1->ClickOn(x, y))
			{
				if (ui1->GetClickable())
				{
					GameEngine::GetInstance()->playSound("../Resource/Sound/Click Laser (V-ktor).wav");
				}
				if (ui1->getButtontype() == UI_LEVEL_BAR) {
					GameDataStorage::GetInstance()->setNextlevelnumber(i);
					GameData::GetInstance()->gGameStateNext = GameState::GS_CHARACTERSELECT;	
				}
				return;
			}
		}
	}
}
void LevelMapSelect::HandleMouseRight(int x, int y)
{

}
void LevelMapSelect::HandleMouseMove(int x, int y)
{
	bool hitbutton = false;
	for (int i = UIList.size() - 1; i >= 0; i--)
	{
		UIObject* ui1 = dynamic_cast<UIObject*>(UIList[i]);
		if (ui1 != nullptr)
		{
			if (ui1->OnMouseStay(x, y))
			{
				hitbutton = true;
				if (prevbutton != ui1)
				{
					prevbutton = ui1;
					GameEngine::GetInstance()->playSound("../Resource/Sound/Click.mp3");
				}
			}
		}
	}
	if (hitbutton == false)
	{
		prevbutton = nullptr;
	}
}
void LevelMapSelect::HandleMouseScroll(int amount)
{
	float size = amount / 15.0f;
	scrollsize += size;
	if (scrollsize > 0) {
		scrollsize = 0;
	}
	else if (scrollsize < -2.5){
		scrollsize = -2.5;
	}
	for (int i = UIList.size() - 1; i >= 0; i--)
	{
		UIObject* ui1 = dynamic_cast<UIObject*>(UIList[i]);
		if (ui1 != nullptr)
		{
			if (ui1->getButtontype() == UI_LEVEL_BAR) {
				LevelBarUI[0]->SetPosition(glm::vec3(0.2f, 0.7f - scrollsize, 0.0f));
				LevelBarUI[1]->SetPosition(glm::vec3(0.2f, 0.45f - scrollsize, 0.0f));
				LevelBarUI[2]->SetPosition(glm::vec3(0.2f, 0.2f - scrollsize, 0.0f));
				LevelBarUI[3]->SetPosition(glm::vec3(0.2f, -0.05f - scrollsize, 0.0f));
				LevelBarUI[4]->SetPosition(glm::vec3(0.2f, -0.3f - scrollsize, 0.0f));
				LevelBarUI[5]->SetPosition(glm::vec3(0.2f, -0.55f - scrollsize, 0.0f));
				LevelBarUI[6]->SetPosition(glm::vec3(0.2f, -0.8f - scrollsize, 0.0f));
				LevelBarUI[7]->SetPosition(glm::vec3(0.2f, -1.05f - scrollsize, 0.0f));
				LevelBarUI[8]->SetPosition(glm::vec3(0.2f, -1.3f - scrollsize, 0.0f));
				LevelBarUI[9]->SetPosition(glm::vec3(0.2f, -1.55f - scrollsize, 0.0f));
				LevelBarUI[10]->SetPosition(glm::vec3(0.2f, -1.8f - scrollsize, 0.0f));
				LevelBarUI[11]->SetPosition(glm::vec3(0.2f, -2.05f - scrollsize, 0.0f));
				LevelBarUI[12]->SetPosition(glm::vec3(0.2f, -2.3f - scrollsize, 0.0f));
				LevelBarUI[13]->SetPosition(glm::vec3(0.2f, -2.55f - scrollsize, 0.0f));
				LevelBarUI[14]->SetPosition(glm::vec3(0.2f, -2.8f - scrollsize, 0.0f));
				LevelBarUI[15]->SetPosition(glm::vec3(0.2f, -3.05f - scrollsize, 0.0f));
				//LevelBarUI[16]->SetPosition(glm::vec3(0.2f, -3.3f + scrollsize, 0.0f));
			}
		}
	}
	//cout << "Scrolling Value:" << scrollsize << endl;
}

