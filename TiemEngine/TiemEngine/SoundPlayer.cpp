#include "SoundPlayer.h"

using namespace std;

SoundPlayer* SoundPlayer::instance = nullptr;

SoundPlayer* SoundPlayer::GetInstance()
{
	if (instance == nullptr) {
		instance = new SoundPlayer();
	}
	return instance;
}
void SoundPlayer::Init()
{
	ae.init();
}
void SoundPlayer::playSound(string filename)
{
	SoundEffect sound_1 = ae.loadSoundEffect(filename);
	sound_1.volume(GameEngine::GetInstance()->getEffectSound());
	sound_1.play();
}
void SoundPlayer::playMusic(string filename)
{
	Music music = ae.loadMusic(filename);
	music.volume(GameEngine::GetInstance()->getMusicSound());
	music.play(-1);
}