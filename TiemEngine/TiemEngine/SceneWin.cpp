#include "SceneWin.h"

void NextWin(UIObject* button)
{
	GameDataStorage::GetInstance()->setNextlevelnumber(GameDataStorage::GetInstance()->getCurrlevelnumber() + 1);
	GameDataStorage::GetInstance()->UpdateSaveFile();
	GameData::GetInstance()->gGameStateNext = GameState::GS_LEVELSELECT;
}
void BackWin(UIObject* button)
{
	GameDataStorage::GetInstance()->setNextlevelnumber(GameDataStorage::GetInstance()->getCurrlevelnumber() + 1);
	GameDataStorage::GetInstance()->UpdateSaveFile();
	GameData::GetInstance()->gGameStateNext = GameState::GS_MAINMENU;
}

void SceneWin::LevelLoad()
{
	SquareMeshVbo* square = new SquareMeshVbo();
	square->LoadData();
	GameEngine::GetInstance()->AddMesh(SquareMeshVbo::MESH_NAME, square);

	//cout << "Load Level" << endl;
}

void SceneWin::LevelInit()
{
	maincamera = new Camera;
	maincamera->Init(GameEngine::GetInstance()->GetWindowWidth(), GameEngine::GetInstance()->GetWindowHeight(),
		-GameEngine::GetInstance()->GetWindowWidth() / 2 - CAMERAOFFSET, CAMERAOFFSET + BOARDSIZEROW * 100 - GameEngine::GetInstance()->GetWindowWidth() / 2,
		-GameEngine::GetInstance()->GetWindowHeight() / 2 - CAMERAOFFSET, CAMERAOFFSET + BOARDSIZECOLUMN * 100 - GameEngine::GetInstance()->GetWindowHeight() / 2);

	for (int i=0;i<3;i++)
	{
		InterFaceUI[i] = new UIObject();
		InterFaceUI[i]->SetMode(1);
		InterFaceUI[i]->SetDraw(true);
		UIList.push_back(InterFaceUI[i]);
	}

	InterFaceUI[0]->SetSize(2.0, -1.7);
	InterFaceUI[0]->SetPosition(glm::vec3(0.0f, 0.0f, 0.0f));
	InterFaceUI[0]->SetTexture("../Resource/Texture/UIWintemp.png");
	InterFaceUI[0]->SetClickable(false);

	InterFaceUI[1]->SetSize(0.4, -0.3);
	InterFaceUI[1]->SetPosition(glm::vec3(-0.7f, -0.7f, 0.0f));
	InterFaceUI[1]->setSprite("../Resource/Texture/CSback.png", 2, 1, 0, 0);
	InterFaceUI[1]->SetClickOnAnim(true);
	InterFaceUI[1]->SetClickable(true);
	InterFaceUI[1]->SetActiveFn(BackWin);

	InterFaceUI[2]->SetSize(0.4, -0.3);
	InterFaceUI[2]->SetPosition(glm::vec3(0.7f, -0.7f, 0.0f));
	InterFaceUI[2]->setSprite("../Resource/Texture/UINext.png", 1, 1, 0, 0);
	InterFaceUI[2]->SetClickable(true);
	InterFaceUI[2]->SetActiveFn(NextWin);
}

void SceneWin::LevelUpdate()
{
	maincamera->CameraDrawArea();
	int deltaTime = GameEngine::GetInstance()->GetDeltaTime();
	for (DrawableObject* obj : objectsList) {
		obj->Update(deltaTime);
	}
}

void SceneWin::LevelDraw()
{
	GameEngine::GetInstance()->Render(objectsList);
	GameEngine::GetInstance()->GetRenderer()->RenderUI(UIList);
}

void SceneWin::LevelFree()
{
	for (DrawableObject* obj : objectsList) {
		if (obj != nullptr)
		{
			delete obj;
		}
	}
	objectsList.clear();
	for (DrawableObject* obj : UIList) {
		delete obj;
	}
	UIList.clear();
}

void SceneWin::LevelUnload()
{
	GameEngine::GetInstance()->ClearMesh();
}

int SceneWin::WorldtoBoardX(float x)
{
	int result;
	float fresult;
	fresult = (float)(x + (maincamera->GetCenter().x * maincamera->GetZoomSize()) + ((maincamera->GetWindowWidth() * maincamera->GetZoomSize()) / 2.0f)) / maincamera->GetZoomSize();
	fresult /= 100.0f;
	result = floorf(fresult);
	return result;
}
int SceneWin::WorldtoBoardY(float y)
{
	int result;
	float fresult;
	fresult = (float)(y + (maincamera->GetCenter().y * maincamera->GetZoomSize()) + ((maincamera->GetWindowHeight() * maincamera->GetZoomSize()) / 2.0f)) / maincamera->GetZoomSize();
	fresult /= 100.0f;
	result = floorf(fresult);
	return result;
}

void SceneWin::HandleKey(char key)
{
	switch (key)
	{
	case 'q': GameData::GetInstance()->gGameStateNext = GameState::GS_QUIT; ; break;
	case 'r': GameData::GetInstance()->gGameStateNext = GameState::GS_RESTART; ; break;
	//default: 
	//	GameDataStorage::GetInstance()->setNextlevelnumber(GameDataStorage::GetInstance()->getCurrlevelnumber() + 1);
	//	GameDataStorage::GetInstance()->UpdateSaveFile();
	//	GameData::GetInstance()->gGameStateNext = GameState::GS_LEVELSELECT; 
	//	break;
	}
}

void SceneWin::HandleMouse(int type, int x, int y)
{
	int scrolling = x;
	int realX, realY;
	realX = GameEngine::GetInstance()->ConvertX(x);
	realY = GameEngine::GetInstance()->ConvertY(y);

	if (type == MOUSEMOVE)//move
	{
		HandleMouseMove(realX, realY);
	}
	else if (type == MOUSELEFT)//left
	{
		HandleMouseLeft(realX, realY);
	}
	else if (type == MOUSERIGHT)//right
	{
		HandleMouseRight(realX, realY);
	}
	else if (type == MOUSESCROLL)//scroll
	{
		HandleMouseScroll(scrolling);
	}
}

void SceneWin::HandleMouseLeft(int x, int y)
{
	for (int i = UIList.size() - 1; i >= 0; i--)
	{
		UIObject* ui1 = dynamic_cast<UIObject*>(UIList[i]);
		if (ui1 != nullptr)
		{
			if (ui1->OnMouseStay(x, y))
			{

			}
		}
	}
	for (int i = UIList.size() - 1; i >= 0; i--)
	{
		UIObject* ui1 = dynamic_cast<UIObject*>(UIList[i]);
		if (ui1 != nullptr)
		{
			if (ui1->ClickOn(x, y))
			{
				return;
			}
		}
	}
}
void SceneWin::HandleMouseRight(int x, int y)
{

}
void SceneWin::HandleMouseMove(int x, int y)
{
	bool hitbutton = false;
	for (int i = UIList.size() - 1; i >= 0; i--)
	{
		UIObject* ui1 = dynamic_cast<UIObject*>(UIList[i]);
		if (ui1 != nullptr)
		{
			if (ui1->OnMouseStay(x, y))
			{
				hitbutton = true;
				if (prevbutton != ui1)
				{
					prevbutton = ui1;
					GameEngine::GetInstance()->playSound("../Resource/Sound/Click.mp3");
				}
				break;
			}
		}
	}
	if (hitbutton == false)
	{
		prevbutton = nullptr;
	}
}
void SceneWin::HandleMouseScroll(int amount)
{

}

