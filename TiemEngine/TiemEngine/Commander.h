#ifndef COMMANDER_H
#define COMMANDER_H
#include "Character.h"
#include <string>

using namespace std;


class Commander : public Character {
private:
	string target;
	int amount;
public:
	void act();

	int getEnemyAmount();
	string getEnemyType();
	void followTarget();

	int getHp();
	int getAtk();
	Enemy(int hp, int atk, int move, int x, int y, string target, int amount);
};
#endif