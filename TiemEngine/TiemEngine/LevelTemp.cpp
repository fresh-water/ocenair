#include "LevelTemp.h"

void LevelTemp::LevelLoad()
{
	SquareMeshVbo* square = new SquareMeshVbo();
	square->LoadData();
	GameEngine::GetInstance()->AddMesh(SquareMeshVbo::MESH_NAME, square);

	//cout << "Load Level" << endl;
}

void LevelTemp::LevelInit()
{
	cout << "LevelTemp" << endl;
	mainboard = new Board();

	mainboard->BoardReadfile("../Resource/GameData/testboard3.txt", &BOARDSIZEROW, &BOARDSIZECOLUMN);

	maincamera = new Camera;
	maincamera->Init(GameEngine::GetInstance()->GetWindowWidth(), GameEngine::GetInstance()->GetWindowHeight(),
		-GameEngine::GetInstance()->GetWindowWidth() / 2 - CAMERAOFFSET, CAMERAOFFSET + BOARDSIZEROW * 100 - GameEngine::GetInstance()->GetWindowWidth() / 2,
		-GameEngine::GetInstance()->GetWindowHeight() / 2 - CAMERAOFFSET, CAMERAOFFSET + BOARDSIZECOLUMN * 100 - GameEngine::GetInstance()->GetWindowHeight() / 2);

	mainboard->SetBoardDefault();

	mainboard->SetMapSprite();

	//mainboard->SummonallUnit();

	mainboard->PushBackallobj(&objectsList, &UIList);

	mainboard->StartPlayerTurn();
}

void LevelTemp::LevelUpdate()
{
	mainboard->BoardUpdate(maincamera);
	maincamera->CameraDrawArea();
	int deltaTime = GameEngine::GetInstance()->GetDeltaTime();
	for (DrawableObject* obj : objectsList) {
		obj->Update(deltaTime);
	}
}

void LevelTemp::LevelDraw()
{
	GameEngine::GetInstance()->Render(objectsList);
	GameEngine::GetInstance()->GetRenderer()->RenderUI(UIList);
}

void LevelTemp::LevelFree()
{
	for (DrawableObject* obj : objectsList) {
		if (obj != nullptr)
		{
			delete obj;
		}
	}
	objectsList.clear();
	for (DrawableObject* obj : UIList) {
		delete obj;
	}
	UIList.clear();
}

void LevelTemp::LevelUnload()
{
	GameEngine::GetInstance()->ClearMesh();
}

int LevelTemp::WorldtoBoardX(float x)
{
	int result;
	float fresult;
	fresult = (float)(x + (maincamera->GetCenter().x * maincamera->GetZoomSize()) + ((maincamera->GetWindowWidth() * maincamera->GetZoomSize()) / 2.0f)) / maincamera->GetZoomSize();
	fresult /= 100.0f;
	result = floorf(fresult);
	return result;
}
int LevelTemp::WorldtoBoardY(float y)
{
	int result;
	float fresult;
	fresult = (float)(y + (maincamera->GetCenter().y * maincamera->GetZoomSize()) + ((maincamera->GetWindowHeight() * maincamera->GetZoomSize()) / 2.0f)) / maincamera->GetZoomSize();
	fresult /= 100.0f;
	result = floorf(fresult);
	return result;
}

void LevelTemp::HandleKey(char key)
{
	switch (key)
	{
	case 'a':
		maincamera->CameraMove(0, -10);
		break;
	case 'd':
		maincamera->CameraMove(0, 10);
		break;
	case 'w':
		maincamera->CameraMove(10, 0);
		break;
	case 's':
		maincamera->CameraMove(-10, 0);
		break;
	case 'I':
		maincamera->SetCameraPosition(0, 0);
		break;
	case 'O':
		maincamera->CameraSetZoom(100);
		break;

	case 'E': GameData::GetInstance()->gGameStateNext = GameState::GS_QUIT; ; break;
	case 'r': GameData::GetInstance()->gGameStateNext = GameState::GS_RESTART; ; break;
	case 'e': GameData::GetInstance()->gGameStateNext = GameState::GS_LOSE; ; break;
	case 'S':
		mainboard->StartEnemyTurn();
		break;
	}
}

void LevelTemp::HandleMouse(int type, int x, int y)
{
	int scrolling = x;
	int realX, realY;
	realX = GameEngine::GetInstance()->ConvertX(x);
	realY = GameEngine::GetInstance()->ConvertY(y);

	if (type == MOUSEMOVE)//move
	{
		HandleMouseMove(realX, realY);
	}
	else if (type == MOUSELEFT)//left
	{
		HandleMouseLeft(realX, realY);
	}
	else if (type == MOUSERIGHT)//right
	{
		HandleMouseRight(realX, realY);
	}
	else if (type == MOUSESCROLL)//scroll
	{
		HandleMouseScroll(scrolling);
	}

}

void LevelTemp::HandleMouseLeft(int x, int y)
{
	float realX = (float)x;
	float realY = (float)y;

	for (int i = 0; i < UIList.size(); i++)
	{
		UIObject* ui1 = dynamic_cast<UIObject*>(UIList[i]);
		if (ui1 != nullptr)
		{
			if (ui1->ClickOn(x, y, mainstate, mainboard->GetBoardstate()))
			{
				if (mainstate == LS_SELECTED)
				{
					maincamera->SetEnable(false);
				}
				else if (mainstate == LS_NORMAL)
				{
					maincamera->SetEnable(true);
				}
				return;
			}
		}
	}

	mainboard->CloseMenuUI();
	maincamera->SetEnable(true);

	int BX, BY;//board X,Y

	BX = WorldtoBoardX(realX);
	BY = WorldtoBoardY(realY);

	if (BX >= 0 && BX < BOARDSIZEROW && BY >= 0 && BY < BOARDSIZECOLUMN)
	{
		mainboard->ClickHandle(MOUSELEFT, BX, BY, mainstate, maincamera);
	}
}
void LevelTemp::HandleMouseRight(int x, int y)
{
	mainboard->ClickHandle(MOUSERIGHT, x, y, mainstate, maincamera);
}
void LevelTemp::HandleMouseMove(int x, int y)
{
	if (y > (maincamera->GetWindowHeight() / 2.0f - 50.0f))
	{
		//cout << "up" << endl;
		maincamera->CameraMove(CAMERASPEED, 0);
	}
	else if (y < -(maincamera->GetWindowHeight() / 2.0f - 50.0f))
	{
		//cout << "down" << endl;
		maincamera->CameraMove(-CAMERASPEED, 0);
	}
	if (x > (maincamera->GetWindowWidth() / 2.0f - 50.0f))
	{
		//cout << "right" << endl;
		maincamera->CameraMove(0, CAMERASPEED);
	}
	else if (x < -(maincamera->GetWindowWidth() / 2.0f - 50.0f))
	{
		//cout << "left" << endl;
		maincamera->CameraMove(0, -CAMERASPEED);
	}

	int BX, BY;//board X,Y

	BX = WorldtoBoardX(x);
	BY = WorldtoBoardY(y);

	if (BX >= 0 && BX < BOARDSIZEROW && BY >= 0 && BY < BOARDSIZECOLUMN)
	{
		mainboard->ClickHandle(MOUSEMOVE, BX, BY, mainstate, maincamera);
	}
}
void LevelTemp::HandleMouseScroll(int amount)
{
	if (amount > 0) //scroll up
	{
		maincamera->CameraSetZoom(maincamera->GetZoomPercent() - 10.0f);
	}
	else if (amount < 0)//scroll down
	{
		maincamera->CameraSetZoom(maincamera->GetZoomPercent() + 10.0f);
	}
}

