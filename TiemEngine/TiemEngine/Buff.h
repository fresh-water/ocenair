#ifndef BUFF_H
#define BUFF_H
#include <string>
#include <vector>
#include "EnumList.h"
//#include "GameObject.h"
using namespace std;

class Buff{
private:
	int effect;
	int type;
	int duration;
public:
	Buff(int type, int effect, int duration);
	int getEffect();
	int getType();
	int getDuration();

	void decreaseDuration(int amount);
	void extendDuration(int amount);
	~Buff();
};
#endif
