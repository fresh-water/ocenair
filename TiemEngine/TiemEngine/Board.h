#pragma once

#include <vector>
#include <string>
#include <fstream>
#include <iostream>
#include <math.h>
#include "GLRenderer.h"
#include "GameEngine.h"
#include "Unit.h"
#include "EnumList.h"
#include "Character.h"
#include "Submarine.h"
#include "Enemy.h"
#include "UIManager.h"
#include "FinalBoss.h"

using namespace std;

struct SpecialBlock{
	int type;
	int duration;
	int effect;
};

class Board
{
	UIManager uimanager;
	int SizeX, SizeY;
	int** TypeArr;
	string** MapSpriteArr;
	int** TempArr;
	SpecialBlock** SpecialBlockArr;
	int** VisionArr;
	GameObject*** BoardArr;
	GameObject*** TempBoardArr;
	GameObject*** EffectBoardArr;
	GameObject*** GridObj;
	GameObject*** AtkBoardFXArr;
	Camera* cam;
	int** GoalArr;
	int Targetindex;
	GameObject* Cursor;
	vector <Unit*> UnitVector;
	glm::vec2 prevmouseonblock;
	int goaltype;
	int enemywait;
	bool triedtowalk;
	int mainstate;
	int previousstate;
	int SelectedUnitX, SelectedUnitY;
	int UnitMovingtoX, UnitMovingtoY;
	int charactercount, enemycount;
	int turncount = 0;
	int curranimwalkindex = 0;
	float curranimwalktime = 0;
public:
	Board();
	void Init(int x, int y);
	void setCamera(Camera* maincamera);
	void SetMapSprite();
	void SetBoardDefault();
	void BoardReadfile(string filename,int *sizeX,int *sizeY);
	void StartGame();
	void AddUnit(Unit* unit, int posx, int posy);
	void PushBackallobj(vector<DrawableObject*>* objecttodraw, vector<DrawableObject*>* UItodraw);
	void CutoffCamera();
	void BoardUpdate(Camera* maincamera);
	void SelectedUnitOpenSpace(int actiontype);
	void UseSelectedUnitSkill();
	void ClickHandle(int type, int x, int y, int& state, Camera* camera);
	void ClickMoveHandle(int x, int y, int& state, Camera* camera);
	void ClickLeftHandle(int x, int y, int& state, Camera* camera);
	void ClickRightHandle(int x, int y, int& state, Camera* camera);

	void StartPlayerTurn();
	void StartEnemyTurn();
	void EnemyAction();
	void PassTurn();

	void UnitMove(int fromx, int fromy, int tox, int toy);
	void UnitAtk(int fromx, int fromy, int tox, int toy);
	void OpenSpace(int type, int sign, int x, int y);
	bool ResetBoard();

	void StatusUpdate();
	void VisionUpdate();

	void SubO2Update();
	void ActionCheck();
	void OpenPauseUI();
	void CloseMenuUI();
	void ClosePauseUI();
	int& GetBoardstate();
	void SetBoardType(int type,int x,int y);
	int GetUnitindex(int posx, int posy);
	void SetMainState(int state);
	int GetTypeArr(int x, int y);
	int** GetWholeTypeArr();
	GameObject* GetBoardArr(int x, int y);
	int GetMainState();
	Unit* GetUnitVector(int index);
	int GetAmountUnit();
	int GetSizeX();
	int GetSizeY();
	UIManager& GetUIManager();
	Unit* GetSelectedUnit();
};