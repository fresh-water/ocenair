#include "Level.h"

const int BOARDSIZEROW = 40;
const int BOARDSIZECOLUMN = 40;
const float CAMERASPEED = 10;

void Level::LevelLoad()
{
	SquareMeshVbo * square = new SquareMeshVbo();
	square->LoadData();
	GameEngine::GetInstance()->AddMesh(SquareMeshVbo::MESH_NAME, square);

	cout << "Load Level" << endl;
}

void Level::LevelInit()
{
	GameObject * obj = new GameObject();
	obj->SetColor(1.0, 0.0, 0.0);
	objectsList.push_back(obj);

	ImageObject * img = new ImageObject();
	img->SetSize(0.8f, -1.2f);
	img->SetPosition(glm::vec3(-1.0f, 0.0f, 0.0f));
	img->SetTexture("../Resource/Texture/Wong.png");
	objectsList.push_back(img);

	SpriteObject * sprite = new SpriteObject("../Resource/Texture/TestSprite.png", 4, 7);
	sprite->SetSize(2.0f, -2.0f);
	sprite->SetPosition(glm::vec3(1.0f, 0.0f, 0.0f));
	sprite->SetAnimationLoop(0, 0, 27, 50);
	objectsList.push_back(sprite);

	cout << "Init Level" << endl;
}

void Level::LevelUpdate()
{
	//cout << "Update Level" << endl;
	int deltaTime = GameEngine::GetInstance()->GetDeltaTime();
	for (DrawableObject* obj : objectsList) {
		obj->Update(deltaTime);
	}
}

void Level::LevelDraw()
{
	GameEngine::GetInstance()->Render(objectsList);
	//cout << "Draw Level" << endl;
}

void Level::LevelFree()
{
	for (DrawableObject* obj : objectsList) {
		if (obj != nullptr)
		{
			delete obj;
		}
	}
	objectsList.clear();
	for (DrawableObject* obj : UIList) {
		delete obj;
	}
	UIList.clear();
	cout << "Free Level" << endl;
}

void Level::LevelUnload()
{
	GameEngine::GetInstance()->ClearMesh();
	cout << "Unload Level" << endl;
}

void Level::HandleKey(char key)
{

	switch (key)
	{
		case 'w': //player->Translate(glm::vec3(0, 0.3, 0)); break;
		case 's': //player->Translate(glm::vec3(0, -0.3, 0)); break;
		case 'a': //player->Translate(glm::vec3(-0.3, 0, 0)); break;
		case 'd': //player->Translate(glm::vec3(0.3, 0, 0)); break;
		case 'q': GameData::GetInstance()->gGameStateNext = GameState::GS_QUIT; ; break;
		case 'r': GameData::GetInstance()->gGameStateNext = GameState::GS_RESTART; ; break;
		case 'e': GameData::GetInstance()->gGameStateNext = GameState::GS_LEVEL2; ; break;
	}
}

void Level::HandleMouse(int type, int x, int y)
{
	float realX, realY;

	// Calculate Real X Y 
	realX = -3 + x * (6.0f / GameEngine::GetInstance()->GetWindowWidth());
	realY = -3 + (GameEngine::GetInstance()->GetWindowHeight() - y) * (6.0f / GameEngine::GetInstance()->GetWindowHeight());

	//player->SetPosition(glm::vec3(realX, realY, 0));
}

void Level::HandleMouseLeft(int x, int y)
{
	float realX = (float)x;
	float realY = (float)y;

	int BX, BY;//board X,Y

	BX = WorldtoBoardX(realX);
	BY = WorldtoBoardY(realY);

	if (BX >= 0 && BX < BOARDSIZEROW && BY >= 0 && BY < BOARDSIZECOLUMN)
	{
		mainboard->ClickHandle(MOUSELEFT, BX, BY,mainstate,maincamera);
	}
}
void Level::HandleMouseRight(int x, int y)
{
	mainboard->ResetBoard();
}
void Level::HandleMouseMove(int x, int y)
{
	if (y > (maincamera->GetWindowHeight() / 2.0f - 50.0f))
	{
		//cout << "up" << endl;
		maincamera->CameraMove(CAMERASPEED, 0);
	}
	else if (y < -(maincamera->GetWindowHeight() / 2.0f - 50.0f))
	{
		//cout << "down" << endl;
		maincamera->CameraMove(-CAMERASPEED, 0);
	}
	if (x > (maincamera->GetWindowWidth() / 2.0f - 50.0f))
	{
		//cout << "right" << endl;
		maincamera->CameraMove(0, CAMERASPEED);
	}
	else if (x < -(maincamera->GetWindowWidth() / 2.0f - 50.0f))
	{
		//cout << "left" << endl;
		maincamera->CameraMove(0, -CAMERASPEED);
	}
}
void Level::HandleMouseScroll(int amount)
{
	if (amount > 0) //scroll up
	{
		maincamera->CameraSetZoom(maincamera->GetZoomPercent() - 10.0f);
	}
	else if (amount < 0)//scroll down
	{
		maincamera->CameraSetZoom(maincamera->GetZoomPercent() + 10.0f);
	}
}

int Level::WorldtoBoardX(float x)
{
	int result;
	float fresult;
	fresult = (float)(x + (maincamera->GetCenter().x * maincamera->GetZoomSize()) + ((maincamera->GetWindowWidth() * maincamera->GetZoomSize()) / 2.0f)) / maincamera->GetZoomSize();
	fresult /= 100.0f;
	result = floorf(fresult);
	return result;
}
int Level::WorldtoBoardY(float y)
{
	int result;
	float fresult;
	fresult = (float)(y + (maincamera->GetCenter().y * maincamera->GetZoomSize()) + ((maincamera->GetWindowHeight() * maincamera->GetZoomSize()) / 2.0f)) / maincamera->GetZoomSize();
	fresult /= 100.0f;
	result = floorf(fresult);
	return result;
}

float Level::ScreentoWorldX(float x)
{
	return (x - (GameEngine::GetInstance()->GetWindowWidth()/2.0f));
}

float Level::ScreentoWorldY(float y)
{
	return (y - (GameEngine::GetInstance()->GetWindowHeight() / 2.0f));
}