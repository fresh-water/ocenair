#pragma once

#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <map>

using namespace std;

struct UVmapSprite {
	int row;
	int col;
};

struct SpriteInfo {
	string texture;
	int row;
	int col;
};

struct SoundInfo {
	string soundpath;
	int volume;
};

class GameDataStorage
{
	static GameDataStorage* instance;
	map<string, unsigned int> TextureStorage;

	map<int, SpriteInfo> characterInfo;
	map<int, SpriteInfo> enemyInfo;
	map<string, SpriteInfo> EffectInfo;
	map<string, string> Skillicon;
	map<string, UVmapSprite> mapsprite;
	map<string, SoundInfo> soundInfo;
	string mapSpriteName;
	map<int, string> mapfilename;
	vector<int> selectedCharacter = {0,1,2};
	vector<int> RemainingCharacter;
	map<int, int> numofchar;
	int unlockedlevel = 1;
	int currentlevel = 1;
	bool openvision = false;
public :
	static GameDataStorage* GetInstance();
	void Init();
	void UpdateSaveFile();
	void loadSaveFile();
	void readCharacterStatusFile(string filename);
	void readEnemyStatusFile(string filename);
	void readMapSprite(string filename);
	void readMapFileName(string filename);
	void readSoundFile(string filename);
	unsigned int gettexturestorage(string name);

	UVmapSprite getMapSpriteUV(string key);
	SpriteInfo getCharacterSpriteInfo(int key);
	SpriteInfo getEnemySpriteInfo(int key);
	SpriteInfo getEffectInfo(string name);
	string getSkillIcon(string name);
	SoundInfo getSoundInfo(string key);
	string getMapFileName(int levelnum);
	string getMapSpriteName();
	int getSelectedCharacter(int index);
	void setSelectedCharacter(vector<int> select);
	int getNumofchar(int key);
	int getNumofcharcurrentlevel();
	int getCurrlevelnumber();
	int getUnlocklevelnumber();
	void setNextlevelnumber(int num);
	void setOpenvision(bool fact);
	bool getOpenvision();

	void ResaveFile();
	vector<int> getRemainCharacter();
	void killCharacter(string name);
};

