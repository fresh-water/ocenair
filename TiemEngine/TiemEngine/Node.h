#pragma once

#include <vector>
#include <string>
#include "GLRenderer.h"
#include "GameEngine.h"
#include "Unit.h"
#include "EnumList.h"

using namespace std;
class Node
{
public:
	int g, h, f;
	glm::vec2 position;
	Node* parent;
	Node(Node *parent,glm::vec2 pos);
};