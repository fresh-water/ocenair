#pragma once
#include "Level.h"

class Level3 : public Level
{
private:
	vector<DrawableObject*> objectsList;
	GameObject* player;
	Camera* maincamera;
public:
	virtual void LevelLoad();
	virtual void LevelInit();
	virtual void LevelUpdate();
	virtual void LevelDraw();
	virtual void LevelFree();
	virtual void LevelUnload();

	virtual int WorldtoBoardX(float x);
	virtual int WorldtoBoardY(float y);

	virtual void HandleKey(char key);
	virtual void HandleMouse(int type, int x, int y);

	void HandleMouseLeft(int x, int y);
	void HandleMouseRight(int x, int y);
	void HandleMouseMove(int x, int y);
	void HandleMouseScroll(int amount);
};
