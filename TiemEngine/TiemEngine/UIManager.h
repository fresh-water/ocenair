#pragma once

#include <vector>
#include "GLRenderer.h"
#include "GameEngine.h"
#include "GameDataStorage.h"
#include "UIObject.h"
#include "Character.h"
#include "Submarine.h"
#include "Enemy.h"
#include "Camera.h"

using namespace std;
class UIManager
{		
		UIObject* MoveMenuUI[7];
		UIObject* ProfilePlayer[20];
		UIObject* ProfileEnemy[20];
		UIObject* ArrowHitRate[5];
		UIObject* PauseMenu[5];
		UIObject* interFaceUI[11];
		UIObject* cutInUI[8];
		UIObject* InfoProfile[3];
		int countdownphase = 0;
		int countdownatkanim = 0;
	public:
		UIManager();
		void Init();
		void Update();

		void showPhaseChange(int type);
		void showCutinUI(bool hit, Unit* attacker, Unit* defender);
		void showSkillCutinUI(Unit* skilluser);
		void showProfileUI(Unit* unit);
		void showProfileUI(Unit* unit, int hit, int damage);
		void closeProfileUI();
		void showPauseUI();
		void closePauseUI();
		void showMoveMenuUI(Unit* unit, Camera* camera);
		void closeMoveMenuUI();
		void showSkillMenuUI(Unit* unit, Camera* camera);
		void showSubSkillUI(Submarine* unit, Camera* camera);
		void showInfoUI(Unit* unit);
		void closeInfoUI();
		void showPassTurnSBUI();
		void closePassTurnSBUI();

		void setEnemyNumber(int amount);
		void setNumber(int amount, UIObject* left, UIObject* middle, UIObject* right);
		void setStatusbar(int curr, int max, int type, UIObject* bar, UIObject* maxbar);
		void SetQuestgoal(int type);
		void setatkanimcount(int amount);
		bool AtkAnimFinish();
		bool PassTurnAnimFinish();

		UIObject* getCutInUI(int index);
		UIObject* getMoveMenuUI(int index);
		UIObject* getProfilePlayerUI(int index);
		UIObject* getProfileEnemyUI(int index);
		UIObject* getArrowHitRate(int index);
		UIObject* getPauseMenuUI(int index);
		UIObject* getInterFaceUI(int index);
};