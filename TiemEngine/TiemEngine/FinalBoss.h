#ifndef FINAL_H
#define FINAL_H
#include "Enemy.h"
#include <string>

using namespace std;

class FinalBoss : public Enemy
{
private:
	vector<glm::vec2> hurtbox;
	vector<glm::vec2> hitbox;
	bool bossisActive = true;
public:
	vector<glm::vec2> getHitbox();
	void setHitbox();
	vector<glm::vec2> getHurtbox();
	void setHurtbox();

	virtual vector<glm::vec2> ActiveSkill(int** typearr, int sizex, int sizey);
	virtual void setActive(bool fact);
	virtual bool getActive();
	virtual glm::vec2 getBoardPosition();
	virtual void Render(glm::mat4 globalModelTransform);

	virtual void readCharacterData(string filename, int index);
	FinalBoss(string name, int hp, int maxhp, int atk, int atkdis, int move, int x, int y, int eva, int def, int cd, string tex);
	FinalBoss();
};
#endif