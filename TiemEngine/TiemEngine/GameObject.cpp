
#include "GameObject.h"
#include "GameEngine.h"
#include "SquareMeshVbo.h"
#include "GameDataStorage.h"


GameObject::GameObject()
{
	color = glm::vec3(0.0, 0.0, 0.0);
	coloroffset = glm::vec3(0.0, 0.0, 0.0);

	this->currentRow = 0;
	this->currentColumn = 0;
	this->GenUV();

	this->startRow = 0;
	this->startColumn = 0;
	this->loopMax = 1;
	this->loopCount = 0;
	this->animationTime = 0;
	this->timeCount = 0;
}


GameObject::~GameObject()
{
}

void GameObject::SetFxtype(int type)
{
	fxtype = type;
}
int GameObject::GetFxtype()
{
	return fxtype;
}

void GameObject::SetColor(float r, float g, float b)
{
	color = glm::vec3(r, g, b);
	alpha = 1.0f;
}

void GameObject::SetColor(float r, float g, float b, float a)
{
	color = glm::vec3(r, g, b);
	alpha = a;
}
glm::vec3 GameObject::GetColor()
{
	return color;
}

void GameObject::SetColorOffset(float r, float g, float b)
{
	coloroffset = glm::vec3(r,g,b);
}
glm::vec3 GameObject::GetColorOffset()
{
	return coloroffset;
}

void GameObject::SetTexture(string path)
{
	//texture = GameEngine::GetInstance()->GetRenderer()->LoadTexture(path);
	texture = GameDataStorage::GetInstance()->gettexturestorage(path);
}
void GameObject::SetTexture(unsigned int tex)
{
	texture = tex;
}

void GameObject::SetMode(int mode)
{
	Mode = mode;
}

void GameObject::setDraw(bool fact)
{
	Draw = fact;
}
bool GameObject::getDraw()
{
	return Draw;
}

void GameObject::setInCam(bool fact)
{
	inCam = fact;
}
bool GameObject::getInCam()
{
	return inCam;
}

void GameObject::Render(glm::mat4 globalModelTransform)
{
	if (Draw == false || inCam == false)
	{
		return;
	}
	SquareMeshVbo* squareMesh = dynamic_cast<SquareMeshVbo*> (GameEngine::GetInstance()->GetRenderer()->GetMesh(SquareMeshVbo::MESH_NAME));

	GLuint modelMatixId = GameEngine::GetInstance()->GetRenderer()->GetModelMatrixAttrId();
	GLuint colorId = GameEngine::GetInstance()->GetRenderer()->GetColorUniformId();
	GLuint renderModeId = GameEngine::GetInstance()->GetRenderer()->GetModeUniformId();
	GLuint alphaId = GameEngine::GetInstance()->GetRenderer()->GetAlphaUniformId();
	GLuint offsetId = GameEngine::GetInstance()->GetRenderer()->GetOffsetUniformId();

	if (modelMatixId == -1) {
		cout << "Error: Can't perform transformation " << endl;
		return;
	}
	if (renderModeId == -1) {
		cout << "Error: Can't set renderMode in ImageObject " << endl;
		return;
	}
	if (alphaId == -1) {
		cout << "Error: Can't set ALPHA in ImageObject " << endl;
		return;
	}
	if (offsetId == -1) {
		cout << "Error: Can't set OFFSET in ImageObject " << endl;
		return;
	}

	vector <glm::mat4> matrixStack;

	glm::mat4 currentMatrix = this->getTransform();

	if (squareMesh != nullptr) {

		currentMatrix = globalModelTransform * currentMatrix;
		glUniformMatrix4fv(modelMatixId, 1, GL_FALSE, glm::value_ptr(currentMatrix));
		glUniform3f(colorId, color.x, color.y, color.z);
		glUniform3f(offsetId, coloroffset.x, coloroffset.y, coloroffset.z);
		glUniform1f(alphaId, this->alpha);
		glUniform1i(renderModeId, Mode);
		glBindTexture(GL_TEXTURE_2D, texture);
		if(isAnim){ //check whether sprite is anim_sprite or not.
			squareMesh->AdjustTexcoord(uv);
		}
		else{
			squareMesh->ResetTexcoord();
		}
		squareMesh->Render();
	}
}

void GameObject::Update(float deltaTime)
{
	timeCount += deltaTime;
	if (timeCount > animationTime)
	{
		this->NextAnimation();
		this->GenUV();
		timeCount = 0;
	}


}

void GameObject::GenUV()
{
	uv[0] = currentColumn / (columnMax * 1.0f);
	uv[1] = currentRow / (rowMax * 1.0f);

	uv[2] = (currentColumn + 1) / (columnMax * 1.0f);
	uv[3] = currentRow / (rowMax * 1.0f);

	uv[4] = (currentColumn + 1) / (columnMax * 1.0f);
	uv[5] = (currentRow + 1) / (rowMax * 1.0f);

	uv[6] = currentColumn / (columnMax * 1.0f);
	uv[7] = (currentRow + 1) / (rowMax * 1.0f);
}

void GameObject::SetAnimationLoop(int startRow, int startColumn, int howManyFrame, int delayBetaweenFrame)
{
	this->startRow = startRow;
	this->startColumn = startColumn;
	this->loopMax = howManyFrame;
	this->loopCount = 0;
	this->animationTime = delayBetaweenFrame;

	currentColumn = startColumn;
	currentRow = startRow;

	GenUV();
}

void GameObject::NextAnimation()
{
	loopCount++;
	if (loopCount < loopMax)
	{
		currentColumn++;
		if (currentColumn == columnMax)
		{
			currentColumn = 0;
			currentRow++;
			if (currentRow == rowMax)
			{
				currentRow = 0;
			}
		}
	}
	else if (Loop == false)
	{
		Draw = false;
	}
	else
	{
		currentRow = startRow;
		currentColumn = startColumn;
		loopCount = 0;
	}

}
void GameObject::setPath(string fileName, int row, int column){
	this->columnMax = column;
	this->rowMax = row;
	//texture = GameEngine::GetInstance()->GetRenderer()->LoadTexture(fileName);
	texture = GameDataStorage::GetInstance()->gettexturestorage(fileName);
	isAnim = true;
}
void GameObject::setPath(unsigned int texture, int row, int column) {
	this->columnMax = column;
	this->rowMax = row;
	this->texture = texture;
	isAnim = true;
}

void GameObject::setUVmap(unsigned int texture, int row, int column) {
	this->columnMax = 10;
	this->rowMax = 50;

	this->startRow = row;
	this->startColumn = column;
	this->currentColumn = column;
	this->currentRow = row;
	this->texture = texture;
	isAnim = true;

	GenUV();
}

void GameObject::setUVmap(int row, int column) {
	this->columnMax = 10;
	this->rowMax = 50;

	this->startRow = row;
	this->startColumn = column;
	this->currentColumn = column;
	this->currentRow = row;
	isAnim = true;

	GenUV();
}

void GameObject::setAnimState(){
	//change ani state from input
}

void GameObject::setLoop(bool fact)
{
	Loop = fact;
}