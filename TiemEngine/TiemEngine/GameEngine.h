#pragma once

#include <vector>
#include <string>
#include "DrawableObject.h"
#include "GLRenderer.h"
#include "GameData.h"
#include "Audio.h"

using namespace std;
class GameEngine
{
	static GameEngine* instance;
	int winWidth, winHeight;
	int deltaTime;
	bool isMouseDown;
	int masterSound = 64, musicSound = 64, effectSound = 64;
	int curSpeedlevel = 1;
	bool skip = false;
	glm::vec2 curMousePos;
	AudioEngine ae;
	SoundEffect se;
	Music mu;
	GLRenderer *renderer;
	GameEngine();
public:
	static GameEngine* GetInstance();
	GLRenderer * GetRenderer();
	void Init(int width, int height);
	void SetDrawArea(float left, float right, float bottom, float top);
	void SetBackgroundColor(float r, float g, float b);
	void AddMesh(string name, MeshVbo* mesh);
	void ClearMesh();
	void Render(vector<DrawableObject*> renderObjects);
	int GetWindowWidth();
	int GetWindowHeight();
	int GetDeltaTime();
	void SetDeltaTime(int dt);
	bool getIsMouseDown();
	void setIsMouseDown(bool flag);
	glm::vec2 getCurMousePos();
	void setCurMousePos(int x, int y);
	float setCurrAnimspeed();

	void playMusic(string name);
	void playSound(string name);
	void setMasterSound(int vol);
	void setMusicSound(int vol);
	void setEffectSound(int vol);
	int getMasterSound();
	int getMusicSound();
	int getEffectSound();
	int getRealMusicSound();
	int getRealEffectSound();

	void setSpeedLevel(int speed);
	int getSpeedLevel();
	void setSkipLevel(bool skip);
	bool getSkipLevel();

	float ConvertX(int x);
	float ConvertY(int y);
};