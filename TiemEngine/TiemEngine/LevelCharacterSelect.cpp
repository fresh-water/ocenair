#include "LevelCharacterSelect.h"

void Next(UIObject* button)
{
	if (GameDataStorage::GetInstance()->getCurrlevelnumber() == 16) {
		GameData::GetInstance()->gGameStateNext = GameState::GS_CUTSCENE1;
	}
	else {
		GameData::GetInstance()->gGameStateNext = GameState::GS_LEVEL3;
	}
}
		

void Back(UIObject* button)
{
	cout << "Back" << endl;
	GameData::GetInstance()->gGameStateNext = GameState::GS_LEVELSELECT;
}

void Selected(UIObject* button)
{
	if (button->selected == false)
	{
		//button->SetColorOffset(0, 1, 0);
		button->selected = true;
		//button->setSprite("../Resource/Texture/CSbox2.png", 1, 1, 0, 0);
	}
	else
	{
		//button->SetColorOffset(0, 0, 0);
		button->selected = false;
		//button->setSprite("../Resource/Texture/CSbox1.png", 1, 1, 0, 0);
	}
}

void LevelCharacterSelect::LevelLoad()
{
	SquareMeshVbo* square = new SquareMeshVbo();
	square->LoadData();
	GameEngine::GetInstance()->AddMesh(SquareMeshVbo::MESH_NAME, square);
}

void LevelCharacterSelect::LevelInit()
{
	maincamera = new Camera;
	maincamera->Init(GameEngine::GetInstance()->GetWindowWidth(), GameEngine::GetInstance()->GetWindowHeight());
	maincamera->SetEnable(false);
	characterCount = GameDataStorage::GetInstance()->getNumofcharcurrentlevel();

	if (characterCount >= 6)
	{
		selectedcharacter = GameDataStorage::GetInstance()->getRemainCharacter();
		GameDataStorage::GetInstance()->setSelectedCharacter(selectedcharacter);
		if (GameDataStorage::GetInstance()->getCurrlevelnumber() == 16) {
			GameData::GetInstance()->gGameStateNext = GameState::GS_CUTSCENE1;
		}
		else
		{
			GameData::GetInstance()->gGameStateNext = GameState::GS_LEVEL3;
		}
	}

	UIObject * bg = new UIObject();
	bg->SetMode(1);
	bg->SetDraw(true);
	bg->SetSize(2, -2);
	bg->SetPosition(glm::vec3(0.0f, 0.0f, 0.0f));
	bg->SetClickable(false);
	bg->SetTexture("../Resource/Texture/CSbg.png");
	UIList.push_back(bg);

	SlotUI1 = new UIObject();
	SlotUI1->SetMode(1);
	SlotUI1->SetDraw(true);
	SlotUI1->SetSize(0.25, 0.45);
	SlotUI1->SetPosition(glm::vec3(0.0f, 0.6f, 0.0f));
	SlotUI1->SetClickable(false);
	SlotUI1->SetTexture("../Resource/Texture/CSslot.png");
	UIList.push_back(SlotUI1);

	SlotUI2 = new UIObject();
	SlotUI2->SetMode(1);
	SlotUI2->SetDraw(true);
	SlotUI2->SetSize(0.25, 0.45);
	SlotUI2->SetPosition(glm::vec3(0.3f, 0.6f, 0.0f));
	SlotUI2->SetClickable(false);
	SlotUI2->SetTexture("../Resource/Texture/CSslot.png");
	UIList.push_back(SlotUI2);

	SlotUI3 = new UIObject();
	SlotUI3->SetMode(1);
	SlotUI3->SetDraw(true);
	SlotUI3->SetSize(0.25, 0.45);
	SlotUI3->SetPosition(glm::vec3(0.6f, 0.6f, 0.0f));
	SlotUI3->SetClickable(false);
	SlotUI3->SetTexture("../Resource/Texture/CSslot.png");
	UIList.push_back(SlotUI3);

	SlotUI4 = new UIObject();
	SlotUI4->SetMode(1);
	SlotUI4->SetDraw(false);
	SlotUI4->SetSize(0.25, 0.45);
	SlotUI4->SetPosition(glm::vec3(0.87f, 0.6f, 0.0f));
	SlotUI4->SetClickable(false);
	SlotUI4->SetTexture("../Resource/Texture/CSslot.png");
	UIList.push_back(SlotUI4);

	PlateUI = new UIObject();
	PlateUI->SetMode(1);
	PlateUI->SetDraw(true);
	PlateUI->SetSize(1.1, -1.1);
	PlateUI->SetPosition(glm::vec3(0.3f, -0.25f, 0.0f));
	PlateUI->SetClickable(false);
	PlateUI->SetTexture("../Resource/Texture/CSplate.png");
	UIList.push_back(PlateUI);

	DataUI = new UIObject();
	DataUI->SetMode(1);
	DataUI->SetDraw(false);
	DataUI->SetSize(0.6, -1.7);
	DataUI->SetPosition(glm::vec3(-0.6f, 0.0f, 0.0f));
	DataUI->SetClickable(false);
	UIList.push_back(DataUI);
	DataUI->SetTexture("../Resource/Texture/Ash_Info.png");

	for (int i = 0 ; i<6 ;i++)
	{
		CharacterIconUI[i] = new UIObject();
		CharacterIconUI[i]->SetMode(1);
		CharacterIconUI[i]->SetDraw(true);
		CharacterIconUI[i]->SetSize(0.25, -0.45);
		CharacterIconUI[i]->SetClickable(false);
		CharacterIconUI[i]->SetClickOnAnim(true);
		CharacterIconUI[i]->SetActiveFn(Selected);
		CharacterIconUI[i]->setSprite("../Resource/Texture/CSbox.png",2,1,0,0);

		CharacterSpriteUI[i] = new UIObject();
		CharacterSpriteUI[i]->SetMode(1);
		CharacterSpriteUI[i]->SetDraw(true);
		CharacterSpriteUI[i]->SetSize(0.2, -0.3);
		CharacterSpriteUI[i]->SetClickable(false);

		CharacterSelectUI[i] = new UIObject();
		CharacterSelectUI[i]->SetMode(1);
		CharacterSelectUI[i]->SetDraw(false);
		CharacterSelectUI[i]->SetSize(0.2, -0.3);
		CharacterSelectUI[i]->SetClickable(false);

		CharacterDeathUI[i] = new UIObject();
		CharacterDeathUI[i]->SetMode(1);
		CharacterDeathUI[i]->SetDraw(true);
		CharacterDeathUI[i]->SetSize(0.25, -0.45);
		CharacterDeathUI[i]->SetClickable(false);
		CharacterDeathUI[i]->setSprite("../Resource/Texture/CSbox4.png", 1, 1, 0, 0);
	}

	CharacterIconUI[0]->SetPosition(glm::vec3(0.0f, -0.03f, 0.0f));
	CharacterSpriteUI[0]->SetPosition(glm::vec3(0.0f, -0.03f, 0.0f));
	CharacterDeathUI[0]->SetPosition(glm::vec3(0.0f, -0.03f, 0.0f));
	CharacterSelectUI[0]->SetPosition(glm::vec3(0.3f, 0.55f, 0.0f));
	SpriteInfo tempspriteinfo = GameDataStorage::GetInstance()->getCharacterSpriteInfo(0);
	CharacterSpriteUI[0]->setSprite(tempspriteinfo.texture, tempspriteinfo.row, tempspriteinfo.col, IDLE, 0);
		CharacterSelectUI[0]->setSprite(tempspriteinfo.texture, tempspriteinfo.row, tempspriteinfo.col, IDLE, 0);


	CharacterIconUI[1]->SetPosition(glm::vec3(0.0f, -0.5f, 0.0f));
	CharacterSpriteUI[1]->SetPosition(glm::vec3(0.0f, -0.5f, 0.0f));
	CharacterDeathUI[1]->SetPosition(glm::vec3(0.0f, -0.5f, 0.0f));
	CharacterSelectUI[1]->SetPosition(glm::vec3(0.42f, 0.55f, 0.0f));
	tempspriteinfo = GameDataStorage::GetInstance()->getCharacterSpriteInfo(1);
	CharacterSpriteUI[1]->setSprite(tempspriteinfo.texture, tempspriteinfo.row, tempspriteinfo.col, IDLE, 0);
		CharacterSelectUI[1]->setSprite(tempspriteinfo.texture, tempspriteinfo.row, tempspriteinfo.col, IDLE, 0);

	CharacterIconUI[2]->SetPosition(glm::vec3(0.3f, -0.03f, 0.0f));
	CharacterSpriteUI[2]->SetPosition(glm::vec3(0.3f, -0.03f, 0.0f));
	CharacterDeathUI[2]->SetPosition(glm::vec3(0.3f, -0.03f, 0.0f));
	CharacterSelectUI[2]->SetPosition(glm::vec3(0.55f, 0.55f, 0.0f));
	tempspriteinfo = GameDataStorage::GetInstance()->getCharacterSpriteInfo(2);
	CharacterSpriteUI[2]->setSprite(tempspriteinfo.texture, tempspriteinfo.row, tempspriteinfo.col, IDLE, 0);
		CharacterSelectUI[2]->setSprite(tempspriteinfo.texture, tempspriteinfo.row, tempspriteinfo.col, IDLE, 0);

	CharacterIconUI[3]->SetPosition(glm::vec3(0.3f, -0.5f, 0.0f));
	CharacterSpriteUI[3]->SetPosition(glm::vec3(0.3f, -0.5f, 0.0f));
	CharacterDeathUI[3]->SetPosition(glm::vec3(0.3f, -0.5f, 0.0f));
	CharacterSelectUI[3]->SetPosition(glm::vec3(0.68f, 0.55f, 0.0f));
	tempspriteinfo = GameDataStorage::GetInstance()->getCharacterSpriteInfo(3);
	CharacterSpriteUI[3]->setSprite(tempspriteinfo.texture, tempspriteinfo.row, tempspriteinfo.col, IDLE, 0);
		CharacterSelectUI[3]->setSprite(tempspriteinfo.texture, tempspriteinfo.row, tempspriteinfo.col, IDLE, 0);

	CharacterIconUI[4]->SetPosition(glm::vec3(0.6f, -0.03f, 0.0f));
	CharacterSpriteUI[4]->SetPosition(glm::vec3(0.6f, -0.03f, 0.0f));
	CharacterDeathUI[4]->SetPosition(glm::vec3(0.6f, -0.03f, 0.0f));
	CharacterSelectUI[4]->SetPosition(glm::vec3(0.1f, 0.55f, 0.0f));
	tempspriteinfo = GameDataStorage::GetInstance()->getCharacterSpriteInfo(4);
	CharacterSpriteUI[4]->setSprite(tempspriteinfo.texture, tempspriteinfo.row, tempspriteinfo.col, IDLE, 0);
		CharacterSelectUI[4]->setSprite(tempspriteinfo.texture, tempspriteinfo.row, tempspriteinfo.col, IDLE, 0);

	CharacterIconUI[5]->SetPosition(glm::vec3(0.6f, -0.5f, 0.0f));
	CharacterSpriteUI[5]->SetPosition(glm::vec3(0.6f, -0.5f, 0.0f));
	CharacterDeathUI[5]->SetPosition(glm::vec3(0.6f, -0.5f, 0.0f));
	CharacterSelectUI[5]->SetPosition(glm::vec3(0.1f, 0.55f, 0.0f));
	tempspriteinfo = GameDataStorage::GetInstance()->getCharacterSpriteInfo(5);
	CharacterSpriteUI[5]->setSprite(tempspriteinfo.texture, tempspriteinfo.row, tempspriteinfo.col, IDLE, 0);
		CharacterSelectUI[5]->setSprite(tempspriteinfo.texture, tempspriteinfo.row, tempspriteinfo.col, IDLE, 0);

	for (int i = 0; i < 6; i++)
	{
		UIList.push_back(CharacterIconUI[i]);
		UIList.push_back(CharacterSpriteUI[i]);
		UIList.push_back(CharacterSelectUI[i]);
		UIList.push_back(CharacterDeathUI[i]);
	}

	vector<int> currRemain = GameDataStorage::GetInstance()->getRemainCharacter();
	for (int i=0;i<currRemain.size();i++)
	{
		CharacterIconUI[currRemain[i]]->SetClickable(true);
		CharacterDeathUI[currRemain[i]]->SetDraw(false);
	}

	NextUI = new UIObject();
	NextUI->SetMode(1);
	NextUI->SetDraw(false);
	NextUI->SetSize(0.35, -0.2);
	NextUI->SetPosition(glm::vec3(0.8f, -0.9f, 0.0f));
	NextUI->SetClickable(true);
	NextUI->SetActiveFn(Next);
	NextUI->SetClickOnAnim(true);
	NextUI->setSprite("../Resource/Texture/CSdeploy.png",2,1,0,0);
	UIList.push_back(NextUI);

	BackUI = new UIObject();
	BackUI->SetMode(1);
	BackUI->SetDraw(true);
	BackUI->SetSize(0.35, -0.2);
	BackUI->SetPosition(glm::vec3(-0.8f, -0.9f, 0.0f));
	BackUI->SetClickable(true);
	BackUI->SetActiveFn(Back);
	BackUI->SetClickOnAnim(true);
	BackUI->setSprite("../Resource/Texture/CSback.png", 2, 1, 0, 0);
	UIList.push_back(BackUI);
}

void LevelCharacterSelect::LevelUpdate()
{
	maincamera->CameraDrawArea();
	int deltaTime = GameEngine::GetInstance()->GetDeltaTime();
	for (DrawableObject* obj : objectsList) {
		obj->Update(deltaTime);
	}
	if (characterCount >= 4) {
		SlotUI1->SetPosition(glm::vec3(-0.1f, 0.6f, 0.0f));
		SlotUI2->SetPosition(glm::vec3(0.17f, 0.6f, 0.0f));
		SlotUI3->SetPosition(glm::vec3(0.44f, 0.6f, 0.0f));
		SlotUI4->SetPosition(glm::vec3(0.71f, 0.6f, 0.0f));
		SlotUI4->SetDraw(true);
		if (selectedcharacter.size() >= 1) {
			CharacterSelectUI[selectedcharacter.at(0)]->SetPosition(glm::vec3(-0.1f, 0.6f, 0.0f));
		}
		if (selectedcharacter.size() >= 2) {
			CharacterSelectUI[selectedcharacter.at(1)]->SetPosition(glm::vec3(0.17f, 0.6f, 0.0f));
		}
		if (selectedcharacter.size() >= 3) {
			CharacterSelectUI[selectedcharacter.at(2)]->SetPosition(glm::vec3(0.44f, 0.6f, 0.0f));
		}
		if (selectedcharacter.size() >= 4) {
			CharacterSelectUI[selectedcharacter.at(3)]->SetPosition(glm::vec3(0.71f, 0.6f, 0.0f));
		}
	}
	else {
		SlotUI1->SetPosition(glm::vec3(0.0f, 0.6f, 0.0f));
		SlotUI2->SetPosition(glm::vec3(0.3f, 0.6f, 0.0f));
		SlotUI3->SetPosition(glm::vec3(0.6f, 0.6f, 0.0f));
		SlotUI4->SetDraw(false);
		for (int i = 0; i < selectedcharacter.size(); i++) {
			CharacterSelectUI[selectedcharacter.at(i)]->SetDraw(false);
		}
		if (selectedcharacter.size() >= 1) {
			CharacterSelectUI[selectedcharacter.at(0)]->SetPosition(glm::vec3(0.0f, 0.6f, 0.0f));
			CharacterSelectUI[selectedcharacter.at(0)]->SetDraw(true);
		}
		if (selectedcharacter.size() >= 2) {
			CharacterSelectUI[selectedcharacter.at(1)]->SetPosition(glm::vec3(0.3f, 0.6f, 0.0f));
			CharacterSelectUI[selectedcharacter.at(1)]->SetDraw(true);
		}
		if (selectedcharacter.size() >= 3) {
			CharacterSelectUI[selectedcharacter.at(2)]->SetPosition(glm::vec3(0.6f, 0.6f, 0.0f));
			CharacterSelectUI[selectedcharacter.at(2)]->SetDraw(true);
		}
		if (selectedcharacter.size() >= 4) {
			CharacterSelectUI[selectedcharacter.at(3)]->SetDraw(true);
		}
	}
}

void LevelCharacterSelect::LevelDraw()
{
	GameEngine::GetInstance()->Render(objectsList);
	GameEngine::GetInstance()->GetRenderer()->RenderUI(UIList);
}

void LevelCharacterSelect::LevelFree()
{
	for (DrawableObject* obj : objectsList) {
		if (obj != nullptr)
		{
			delete obj;
		}
	}
	objectsList.clear();
	for (DrawableObject* obj : UIList) {
		delete obj;
	}
	UIList.clear();
}

void LevelCharacterSelect::LevelUnload()
{
	GameEngine::GetInstance()->ClearMesh();
}

void LevelCharacterSelect::HandleKey(char key)
{
	switch (key)
	{
	case 'a':
		break;
	case 'd':
		break;
	case 'w':
		if (GameDataStorage::GetInstance()->getCurrlevelnumber() > 16)
		{
			GameDataStorage::GetInstance()->setNextlevelnumber(0);
		}
		else
		{
			GameDataStorage::GetInstance()->setNextlevelnumber(GameDataStorage::GetInstance()->getCurrlevelnumber() + 1);
		}
		cout << "currentlevel = " << GameDataStorage::GetInstance()->getCurrlevelnumber() << endl;
		characterCount = GameDataStorage::GetInstance()->getNumofcharcurrentlevel();
		GameDataStorage::GetInstance()->UpdateSaveFile();
		//cout << "characterCount = " << characterCount << endl;
		break;
	case 's':
		if (GameDataStorage::GetInstance()->getCurrlevelnumber() > 16)
		{
			GameDataStorage::GetInstance()->setNextlevelnumber(0);
		}
		else
		{
			GameDataStorage::GetInstance()->setNextlevelnumber(GameDataStorage::GetInstance()->getCurrlevelnumber() - 1);
		}
		cout << "currentlevel = " << GameDataStorage::GetInstance()->getCurrlevelnumber() << endl;
		characterCount = GameDataStorage::GetInstance()->getNumofcharcurrentlevel();
		//cout << "characterCount = " << characterCount << endl;
		break;
	case 'I':
		break;
	case 'O':
		break;

	case 'E': GameData::GetInstance()->gGameStateNext = GameState::GS_QUIT; ; break;
	case 'r': GameData::GetInstance()->gGameStateNext = GameState::GS_RESTART; ; break;
	case 'e': GameData::GetInstance()->gGameStateNext = GameState::GS_WIN; ; break;		//GS_WIN/GS_LOSE scene loading
	case 'S':
		break;
	}
}

void LevelCharacterSelect::HandleMouse(int type, int x, int y)
{
	int scrolling = x;
	int realX, realY;
	realX = GameEngine::GetInstance()->ConvertX(x);
	realY = GameEngine::GetInstance()->ConvertY(y);

	if (type == MOUSEMOVE)//move
	{
		HandleMouseMove(realX, realY);
	}
	else if (type == MOUSELEFT)//left
	{
		HandleMouseLeft(realX, realY);
	}
	else if (type == MOUSERIGHT)//right
	{
		HandleMouseRight(realX, realY);
	}
	else if (type == MOUSESCROLL)//scroll
	{
		HandleMouseScroll(scrolling);
	}

}

void LevelCharacterSelect::HandleMouseLeft(int x, int y)
{
	UIObject* ui1 = dynamic_cast<UIObject*>(NextUI);
	if (ui1 != nullptr)
	{
		if (ui1->ClickOn(x, y))
		{
			GameDataStorage::GetInstance()->setSelectedCharacter(selectedcharacter);
			return;
		}
	}
	ui1 = dynamic_cast<UIObject*>(BackUI);
	if (ui1 != nullptr)
	{
		if (ui1->ClickOn(x, y))
		{
			if (ui1->GetClickable())
			{
				GameEngine::GetInstance()->playSound("../Resource/Sound/Click Laser (V-ktor).wav");
			}
			return;
		}
	}

	for (int i = 0; i < 6; i++)
	{
		UIObject* ui1 = dynamic_cast<UIObject*>(CharacterIconUI[i]);
		if (ui1 != nullptr)
		{
			if (ui1->ClickOn(x, y) && ui1->GetClickable())
			{
				bool same = false;
				for (int j=0;j< selectedcharacter.size();j++)
				{
					if (selectedcharacter[j] == i)
					{
						selectedcharacter.erase(selectedcharacter.begin() + j);
						same = true;
					}
				}
				if (same == false)
				{
					if (selectedcharacter.size() < characterCount)
					{
						selectedcharacter.push_back(i);
					}
					else
					{
						CharacterIconUI[selectedcharacter[0]]->ActiveFunction();
						selectedcharacter.erase(selectedcharacter.begin() + 0);
						selectedcharacter.push_back(i);
					}
				}
				if (selectedcharacter.size() == characterCount || selectedcharacter.size() == GameDataStorage::GetInstance()->getRemainCharacter().size())
				{
					NextUI->SetDraw(true);
				}
				else
				{
					NextUI->SetDraw(false);
				}
				//render sprite selected
				for (int l = 0; l < 6; l++) {
					CharacterSelectUI[l]->SetDraw(false);
				}
				for (int k = 0; k < selectedcharacter.size(); k++) {
					if (selectedcharacter.size() >= 1) {
						int first = selectedcharacter.at(0);
						CharacterSelectUI[selectedcharacter.at(0)]->SetPosition(glm::vec3(0.0f, 0.6f, 0.0f));
						CharacterSelectUI[selectedcharacter.at(0)]->SetDraw(true);
					}
					if (selectedcharacter.size() >= 2) {
						int second = selectedcharacter.at(1);
						CharacterSelectUI[selectedcharacter.at(1)]->SetPosition(glm::vec3(0.3f, 0.6f, 0.0f));
						CharacterSelectUI[selectedcharacter.at(1)]->SetDraw(true);
					}
					if (selectedcharacter.size() >= 3) {
						int third = selectedcharacter.at(2);
						CharacterSelectUI[selectedcharacter.at(2)]->SetPosition(glm::vec3(0.6f, 0.6f, 0.0f));
						CharacterSelectUI[selectedcharacter.at(2)]->SetDraw(true);
					}
					if (selectedcharacter.size() >= 4 && characterCount == 4) {
						int fourth = selectedcharacter.at(3);
						CharacterSelectUI[selectedcharacter.at(3)]->SetPosition(glm::vec3(0.71f, 0.55f, 0.0f));
						CharacterSelectUI[selectedcharacter.at(3)]->SetDraw(true);
					}	
				}
				if (ui1 == dynamic_cast<UIObject*>(CharacterIconUI[0])) {
					DataUI->SetDraw(true);
					DataUI->setSprite("../Resource/Texture/Ash_Info.png", 1, 1, 0, 0);
				}
				if (ui1 == dynamic_cast<UIObject*>(CharacterIconUI[1])) {
					DataUI->SetDraw(true);
					DataUI->setSprite("../Resource/Texture/Might_Info.png", 1, 1, 0, 0);
				}
				if (ui1 == dynamic_cast<UIObject*>(CharacterIconUI[2])) {
					DataUI->SetDraw(true);
					DataUI->setSprite("../Resource/Texture/Gray_Info.png", 1, 1, 0, 0);
				}
				if (ui1 == dynamic_cast<UIObject*>(CharacterIconUI[3])) {
					DataUI->SetDraw(true);
					DataUI->setSprite("../Resource/Texture/Todd_Info.png", 1, 1, 0, 0);
				}
				if (ui1 == dynamic_cast<UIObject*>(CharacterIconUI[4])) {
					DataUI->SetDraw(true);
					DataUI->setSprite("../Resource/Texture/Wong_Info.png", 1, 1, 0, 0);
				}
				if (ui1 == dynamic_cast<UIObject*>(CharacterIconUI[5])) {
					DataUI->SetDraw(true);
					DataUI->setSprite("../Resource/Texture/Kim_Info.png", 1, 1, 0, 0);
				}
				return;
			}
		}
	}
}
void LevelCharacterSelect::HandleMouseRight(int x, int y)
{
	
}
void LevelCharacterSelect::HandleMouseMove(int x, int y)
{
	bool hitbutton = false;
	for (int i = UIList.size() - 1; i >= 0; i--)
	{
		UIObject* ui1 = dynamic_cast<UIObject*>(UIList[i]);
		if (ui1 != nullptr)
		{
			if (ui1->OnMouseStay(x, y))
			{
				hitbutton = true;
				if (prevbutton != ui1)
				{
					prevbutton = ui1;
					GameEngine::GetInstance()->playSound("../Resource/Sound/Click.mp3");
				}
			}
		}
	}
	if (hitbutton == false)
	{
		prevbutton = nullptr;
	}
	for (int i = UIList.size() - 1; i >= 0; i--)
	{
		UIObject* ui1 = dynamic_cast<UIObject*>(UIList[i]);
		if (ui1 != nullptr)
		{
			if (ui1->OnMouseStay(x, y))
			{
				if (ui1 == dynamic_cast<UIObject*>(CharacterIconUI[0])) {
					DataUI->SetDraw(true);
					DataUI->setSprite("../Resource/Texture/Ash_Info.png", 1, 1, 0, 0);
				}
				if (ui1 == dynamic_cast<UIObject*>(CharacterIconUI[1])) {
					DataUI->SetDraw(true);
					DataUI->setSprite("../Resource/Texture/Todd_Info.png", 1, 1, 0, 0);
				}
				if (ui1 == dynamic_cast<UIObject*>(CharacterIconUI[2])) {
					DataUI->SetDraw(true);
					DataUI->setSprite("../Resource/Texture/Might_Info.png", 1, 1, 0, 0);
				}
				if (ui1 == dynamic_cast<UIObject*>(CharacterIconUI[3])) {
					DataUI->SetDraw(true);
					DataUI->setSprite("../Resource/Texture/Wong_Info.png", 1, 1, 0, 0);
				}
				if (ui1 == dynamic_cast<UIObject*>(CharacterIconUI[4])) {
					DataUI->SetDraw(true);
					DataUI->setSprite("../Resource/Texture/Gray_Info.png", 1, 1, 0, 0);
				}
				if (ui1 == dynamic_cast<UIObject*>(CharacterIconUI[5])) {
					DataUI->SetDraw(true);
					DataUI->setSprite("../Resource/Texture/Kim_Info.png", 1, 1, 0, 0);
				}
			}
		}
		//delete ui1;
	}
}
void LevelCharacterSelect::HandleMouseScroll(int amount)
{

}

