#pragma once

#include <vector>
#include "GLRenderer.h"
#include "GameEngine.h"
#include "Unit.h"

using namespace std;
class Camera
{
	float winWidth, winHeight;
	float Left,Right,Down,Up;
	float maxzoompercent,minzoompercent, zoompercent;
	glm::vec3 center;
	float minWidth,maxWidth, minHeight , maxHeight;
	bool enable = true; //player cant longer control cam
	Unit* unitFollow;
	bool isfollow = false;
public:
	Camera();
	void Init(int width, int height);
	void Init(int width, int height, float minwidth, float maxwidth, float minheight, float maxheight);
	void Update();

	void CameraDrawArea();
	void CameraMove(float up, float right);
	void SetCameraPosition(float x,float y);
	void CameraSetZoom(float percent);
	void CameraAdjust();
	
	void SetFollowUnit(Unit* unit);
	void SetIsFollow(bool fact);
	bool GetIsFollow();
	void SetEnable(bool fact);
	bool GetEnable();

	glm::vec3 GetCenter();
	float GetZoomPercent();
	float GetZoomSize();
	float GetLeft();
	float GetRight();
	float GetDown();
	float GetUp();
	float GetWindowWidth();
	float GetWindowHeight();
};