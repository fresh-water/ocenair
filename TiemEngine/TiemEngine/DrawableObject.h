#pragma once
#include "glm.hpp"
#include <vector>
#include <string>

using namespace std;

class DrawableObject
{
protected:
	glm::vec3 pos;
	glm::vec3 size;

public:
	glm::mat4 getTransform();

	DrawableObject();
	~DrawableObject();
	virtual void Render(glm::mat4 globalModelTransform) = 0;
	virtual void Update(float deltaTime);
	void SetSize(float sizeX, float sizeY);
	virtual void SetPosition(glm::vec3 newPosition);
	glm::vec3 GetPosition();
	void Translate(glm::vec3 moveDistance);

	glm::vec3 GetSize();

	//Rotate, Scale ???

};

