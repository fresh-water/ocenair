#include "LevelCharacterSelect1.h"

void Next(UIObject* button)
{
	cout << "Next" << endl;
	GameData::GetInstance()->gGameStateNext = GameState::GS_LEVEL3;
}

void Back(UIObject* button)
{
	cout << "Back" << endl;
	GameData::GetInstance()->gGameStateNext = GameState::GS_MAINMENU;
}

void Selected(UIObject* button)
{
	if (button->selected == false)
	{
		button->SetColorOffset(1, 0, 0);
		button->selected = true;
	}
	else
	{
		button->SetColorOffset(0, 0, 0);
		button->selected = false;
	}
}

void LevelCharacterSelect1::LevelLoad()
{
	SquareMeshVbo* square = new SquareMeshVbo();
	square->LoadData();
	GameEngine::GetInstance()->AddMesh(SquareMeshVbo::MESH_NAME, square);
}

void LevelCharacterSelect1::LevelInit()
{
	maincamera = new Camera;
	maincamera->Init(GameEngine::GetInstance()->GetWindowWidth(), GameEngine::GetInstance()->GetWindowHeight());
	maincamera->SetEnable(false);
	characterCount = GameDataStorage::GetInstance()->getNumofcharcurrentlevel();


	UIObject* bg = new UIObject();
	bg->SetMode(1);
	bg->SetDraw(true);
	bg->SetSize(2, -2);
	bg->SetPosition(glm::vec3(0.0f, 0.0f, 0.0f));
	bg->SetClickable(false);
	bg->SetTexture("../Resource/Texture/CharacterSelectBG.png");
	UIList.push_back(bg);

	TitleUI = new UIObject();
	TitleUI->SetMode(1);
	TitleUI->SetDraw(true);
	TitleUI->SetSize(1.4, -0.8);
	TitleUI->SetPosition(glm::vec3(0.0f, 0.75f, 0.0f));
	TitleUI->SetClickable(false);
	TitleUI->SetTexture("../Resource/Texture/UICharacterSelect.png");
	UIList.push_back(TitleUI);

	//UIObject* CharacterIconUI[6];
	//UIObject* CharacterSpriteUI[6];
	//UIObject* NextUI;
	//UIObject* BackUI;

	for (int i = 0; i < 6; i++)
	{
		CharacterIconUI[i] = new UIObject();
		CharacterIconUI[i]->SetMode(1);
		CharacterIconUI[i]->SetDraw(true);
		CharacterIconUI[i]->SetSize(0.27, -0.37);
		CharacterIconUI[i]->SetClickable(true);
		CharacterIconUI[i]->SetActiveFn(Selected);
		CharacterIconUI[i]->SetTexture("../Resource/Texture/UIProfile.png");

		CharacterSpriteUI[i] = new UIObject();
		CharacterSpriteUI[i]->SetMode(1);
		CharacterSpriteUI[i]->SetDraw(true);
		CharacterSpriteUI[i]->SetSize(0.2, -0.3);
		CharacterSpriteUI[i]->SetClickable(false);
	}

	CharacterIconUI[0]->SetPosition(glm::vec3(-0.5f, 0.3f, 0.0f));
	CharacterSpriteUI[0]->SetPosition(glm::vec3(-0.5f, 0.3f, 0.0f));
	SpriteInfo tempspriteinfo = GameDataStorage::GetInstance()->getCharacterSpriteInfo(0);
	CharacterSpriteUI[0]->setSprite(tempspriteinfo.texture, tempspriteinfo.row, tempspriteinfo.col, IDLE, 0);

	CharacterIconUI[1]->SetPosition(glm::vec3(0.0f, 0.3f, 0.0f));
	CharacterSpriteUI[1]->SetPosition(glm::vec3(0.0f, 0.3f, 0.0f));
	tempspriteinfo = GameDataStorage::GetInstance()->getCharacterSpriteInfo(1);
	CharacterSpriteUI[1]->setSprite(tempspriteinfo.texture, tempspriteinfo.row, tempspriteinfo.col, IDLE, 0);

	CharacterIconUI[2]->SetPosition(glm::vec3(0.5f, 0.3f, 0.0f));
	CharacterSpriteUI[2]->SetPosition(glm::vec3(0.5f, 0.3f, 0.0f));
	tempspriteinfo = GameDataStorage::GetInstance()->getCharacterSpriteInfo(2);
	CharacterSpriteUI[2]->setSprite(tempspriteinfo.texture, tempspriteinfo.row, tempspriteinfo.col, IDLE, 0);

	CharacterIconUI[3]->SetPosition(glm::vec3(-0.5f, -0.2f, 0.0f));
	CharacterSpriteUI[3]->SetPosition(glm::vec3(-0.5f, -0.2f, 0.0f));
	tempspriteinfo = GameDataStorage::GetInstance()->getCharacterSpriteInfo(3);
	CharacterSpriteUI[3]->setSprite(tempspriteinfo.texture, tempspriteinfo.row, tempspriteinfo.col, IDLE, 0);

	CharacterIconUI[4]->SetPosition(glm::vec3(0.0f, -0.2f, 0.0f));
	CharacterSpriteUI[4]->SetPosition(glm::vec3(0.0f, -0.2f, 0.0f));
	tempspriteinfo = GameDataStorage::GetInstance()->getCharacterSpriteInfo(4);
	CharacterSpriteUI[4]->setSprite(tempspriteinfo.texture, tempspriteinfo.row, tempspriteinfo.col, IDLE, 0);

	CharacterIconUI[5]->SetPosition(glm::vec3(0.5f, -0.2f, 0.0f));
	CharacterSpriteUI[5]->SetPosition(glm::vec3(0.5f, -0.2f, 0.0f));
	tempspriteinfo = GameDataStorage::GetInstance()->getCharacterSpriteInfo(5);
	CharacterSpriteUI[5]->setSprite(tempspriteinfo.texture, tempspriteinfo.row, tempspriteinfo.col, IDLE, 0);

	for (int i = 0; i < 6; i++)
	{
		UIList.push_back(CharacterIconUI[i]);
		UIList.push_back(CharacterSpriteUI[i]);
	}

	NextUI = new UIObject();
	NextUI->SetMode(1);
	NextUI->SetDraw(false);
	NextUI->SetSize(0.5, -0.3);
	NextUI->SetPosition(glm::vec3(0.7f, -0.80f, 0.0f));
	NextUI->SetClickable(true);
	NextUI->SetActiveFn(Next);
	NextUI->SetTexture("../Resource/Texture/UINext.png");
	UIList.push_back(NextUI);

	BackUI = new UIObject();
	BackUI->SetMode(1);
	BackUI->SetDraw(true);
	BackUI->SetSize(0.5, -0.3);
	BackUI->SetPosition(glm::vec3(-0.7f, -0.80f, 0.0f));
	BackUI->SetClickable(true);
	BackUI->SetActiveFn(Back);
	BackUI->SetTexture("../Resource/Texture/UIBack.png");
	UIList.push_back(BackUI);
}

void LevelCharacterSelect1::LevelUpdate()
{
	maincamera->CameraDrawArea();
	int deltaTime = GameEngine::GetInstance()->GetDeltaTime();
	for (DrawableObject* obj : objectsList) {
		obj->Update(deltaTime);
	}

}

void LevelCharacterSelect1::LevelDraw()
{
	GameEngine::GetInstance()->Render(objectsList);
	GameEngine::GetInstance()->GetRenderer()->RenderUI(UIList);
}

void LevelCharacterSelect1::LevelFree()
{
	for (DrawableObject* obj : objectsList) {
		if (obj != nullptr)
		{
			delete obj;
		}
	}
	objectsList.clear();
	for (DrawableObject* obj : UIList) {
		delete obj;
	}
	UIList.clear();
}

void LevelCharacterSelect1::LevelUnload()
{
	GameEngine::GetInstance()->ClearMesh();
}

void LevelCharacterSelect1::HandleKey(char key)
{
	switch (key)
	{
	case 'a':
		break;
	case 'd':
		break;
	case 'w':
		if (GameDataStorage::GetInstance()->getCurrlevelnumber() >= 19)
		{
			GameDataStorage::GetInstance()->setNextlevelnumber(0);
		}
		else
		{
			GameDataStorage::GetInstance()->setNextlevelnumber(GameDataStorage::GetInstance()->getCurrlevelnumber() + 1);
		}
		cout << "currentlevel = " << GameDataStorage::GetInstance()->getCurrlevelnumber() << endl;
		characterCount = GameDataStorage::GetInstance()->getNumofcharcurrentlevel();
		cout << "characterCount = " << characterCount << endl;
		break;
	case 's':
		if (GameDataStorage::GetInstance()->getCurrlevelnumber() >= 19)
		{
			GameDataStorage::GetInstance()->setNextlevelnumber(0);
		}
		else
		{
			GameDataStorage::GetInstance()->setNextlevelnumber(GameDataStorage::GetInstance()->getCurrlevelnumber() - 1);
		}
		cout << "currentlevel = " << GameDataStorage::GetInstance()->getCurrlevelnumber() << endl;
		characterCount = GameDataStorage::GetInstance()->getNumofcharcurrentlevel();
		cout << "characterCount = " << characterCount << endl;
		break;
	case 'I':
		break;
	case 'O':
		break;

	case 'E': GameData::GetInstance()->gGameStateNext = GameState::GS_QUIT; ; break;
	case 'r': GameData::GetInstance()->gGameStateNext = GameState::GS_RESTART; ; break;
	case 'e': GameData::GetInstance()->gGameStateNext = GameState::GS_WIN; ; break;		//GS_WIN/GS_LOSE scene loading
	case 'S':
		break;
	}
}

void LevelCharacterSelect1::HandleMouse(int type, int x, int y)
{
	int scrolling = x;
	int realX, realY;
	realX = GameEngine::GetInstance()->ConvertX(x);
	realY = GameEngine::GetInstance()->ConvertY(y);

	if (type == MOUSEMOVE)//move
	{
		HandleMouseMove(realX, realY);
	}
	else if (type == MOUSELEFT)//left
	{
		HandleMouseLeft(realX, realY);
	}
	else if (type == MOUSERIGHT)//right
	{
		HandleMouseRight(realX, realY);
	}
	else if (type == MOUSESCROLL)//scroll
	{
		HandleMouseScroll(scrolling);
	}

}

void LevelCharacterSelect1::HandleMouseLeft(int x, int y)
{
	UIObject* ui1 = dynamic_cast<UIObject*>(NextUI);
	if (ui1 != nullptr)
	{
		if (ui1->ClickOn(x, y))
		{
			//for (int i = 0; i < selectedcharacter.size(); i++)
			//{
			//	cout << selectedcharacter[i] << " ";
			//}
			//cout << endl;
			GameDataStorage::GetInstance()->setSelectedCharacter(selectedcharacter);
			return;
		}
	}
	ui1 = dynamic_cast<UIObject*>(BackUI);
	if (ui1 != nullptr)
	{
		if (ui1->ClickOn(x, y))
		{
			return;
		}
	}

	for (int i = 0; i < 6; i++)
	{
		UIObject* ui1 = dynamic_cast<UIObject*>(CharacterIconUI[i]);
		if (ui1 != nullptr)
		{
			if (ui1->ClickOn(x, y))
			{
				bool same = false;
				for (int j = 0; j < selectedcharacter.size(); j++)
				{
					if (selectedcharacter[j] == i)
					{
						selectedcharacter.erase(selectedcharacter.begin() + j);
						same = true;
					}
				}
				if (same == false)
				{
					if (selectedcharacter.size() < characterCount)
					{
						selectedcharacter.push_back(i);
					}
					else
					{
						CharacterIconUI[selectedcharacter[0]]->ActiveFunction();
						selectedcharacter.erase(selectedcharacter.begin() + 0);
						selectedcharacter.push_back(i);
					}
				}
				if (selectedcharacter.size() == characterCount)
				{
					NextUI->SetDraw(true);
				}
				else
				{
					NextUI->SetDraw(false);
				}
				return;
			}
		}
	}
}
void LevelCharacterSelect1::HandleMouseRight(int x, int y)
{

}
void LevelCharacterSelect1::HandleMouseMove(int x, int y)
{

}
void LevelCharacterSelect1::HandleMouseScroll(int amount)
{

}

