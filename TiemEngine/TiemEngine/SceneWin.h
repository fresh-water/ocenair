#pragma once
#include "Level.h"
#include "Board.h"

class SceneWin : public Level
{
private:
	vector<DrawableObject*> objectsList;
	vector<DrawableObject*> UIList;
	Camera* maincamera;
	Board* mainboard;
	UIObject* InterFaceUI[3];
	int mainstate;
	int BOARDSIZEROW = 40;
	int BOARDSIZECOLUMN = 40;
	float CAMERASPEED = 10;
	float CAMERAOFFSET = 200;
public:
	virtual void LevelLoad();
	virtual void LevelInit();
	virtual void LevelUpdate();
	virtual void LevelDraw();
	virtual void LevelFree();
	virtual void LevelUnload();

	virtual void HandleKey(char key);
	virtual void HandleMouse(int type, int x, int y);

	virtual void HandleMouseLeft(int x, int y);
	virtual void HandleMouseRight(int x, int y);
	virtual void HandleMouseMove(int x, int y);
	virtual void HandleMouseScroll(int amount);
	//virtual void SetMainState(int state);
	//virtual int GetMainState();

	virtual int WorldtoBoardX(float x);
	virtual int WorldtoBoardY(float y);
};