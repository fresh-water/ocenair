#include "SceneLose.h"

void BackLose(UIObject* button)
{
	GameData::GetInstance()->gGameStateNext = GameState::GS_MAINMENU;
}

void SceneLose::LevelLoad()
{
	SquareMeshVbo* square = new SquareMeshVbo();
	square->LoadData();
	GameEngine::GetInstance()->AddMesh(SquareMeshVbo::MESH_NAME, square);

	//cout << "Load Level" << endl;
}

void SceneLose::LevelInit()
{
	cout << "Level LOSE SCENE" << endl;
	maincamera = new Camera;
	maincamera->Init(GameEngine::GetInstance()->GetWindowWidth(), GameEngine::GetInstance()->GetWindowHeight(),
		-GameEngine::GetInstance()->GetWindowWidth() / 2 - CAMERAOFFSET, CAMERAOFFSET + BOARDSIZEROW * 100 - GameEngine::GetInstance()->GetWindowWidth() / 2,
		-GameEngine::GetInstance()->GetWindowHeight() / 2 - CAMERAOFFSET, CAMERAOFFSET + BOARDSIZECOLUMN * 100 - GameEngine::GetInstance()->GetWindowHeight() / 2);

	for (int i = 0; i < 2; i++)
	{
		InterFaceUI[i] = new UIObject();
		InterFaceUI[i]->SetMode(1);
		InterFaceUI[i]->SetDraw(true);
		UIList.push_back(InterFaceUI[i]);
	}

	InterFaceUI[0]->SetSize(2.0, -1.7);
	InterFaceUI[0]->SetPosition(glm::vec3(0.0f, 0.0f, 0.0f));
	InterFaceUI[0]->SetTexture("../Resource/Texture/UILosetemp.png");
	InterFaceUI[0]->SetClickable(false);

	InterFaceUI[1]->SetSize(0.4, -0.3);
	InterFaceUI[1]->SetPosition(glm::vec3(-0.7f, -0.7f, 0.0f));
	InterFaceUI[1]->setSprite("../Resource/Texture/CSback.png", 2, 1, 0, 0);
	InterFaceUI[1]->SetClickOnAnim(true);
	InterFaceUI[1]->SetClickable(true);
	InterFaceUI[1]->SetActiveFn(BackLose);

	//InterFaceUI[2]->SetSize(0.4, -0.3);
	//InterFaceUI[2]->SetPosition(glm::vec3(0.7f, -0.7f, 0.0f));
	//InterFaceUI[2]->setSprite("../Resource/Texture/UINext.png", 1, 1, 0, 0);
	//InterFaceUI[2]->SetClickable(true);
	//InterFaceUI[2]->SetActiveFn(NextWin);
}

void SceneLose::LevelUpdate()
{
	maincamera->CameraDrawArea();
	int deltaTime = GameEngine::GetInstance()->GetDeltaTime();
	for (DrawableObject* obj : objectsList) {
		obj->Update(deltaTime);
	}
}

void SceneLose::LevelDraw()
{
	GameEngine::GetInstance()->Render(objectsList);
	GameEngine::GetInstance()->GetRenderer()->RenderUI(UIList);
}

void SceneLose::LevelFree()
{
	for (DrawableObject* obj : objectsList) {
		if (obj != nullptr)
		{
			delete obj;
		}
	}
	objectsList.clear();
	for (DrawableObject* obj : UIList) {
		delete obj;
	}
	UIList.clear();
}

void SceneLose::LevelUnload()
{
	GameEngine::GetInstance()->ClearMesh();
}

void SceneLose::HandleKey(char key)
{
	switch (key)
	{
	case 'q': GameData::GetInstance()->gGameStateNext = GameState::GS_QUIT; ; break;
	case 'r': GameData::GetInstance()->gGameStateNext = GameState::GS_RESTART; ; break;
		default: GameData::GetInstance()->gGameStateNext = GameState::GS_LEVELSELECT; ; break;
	}
}

void SceneLose::HandleMouse(int type, int x, int y)
{
	int scrolling = x;
	int realX, realY;
	realX = GameEngine::GetInstance()->ConvertX(x);
	realY = GameEngine::GetInstance()->ConvertY(y);

	if (type == MOUSEMOVE)//move
	{
		HandleMouseMove(realX, realY);
	}
	else if (type == MOUSELEFT)//left
	{
		HandleMouseLeft(realX, realY);
	}
	else if (type == MOUSERIGHT)//right
	{
		HandleMouseRight(realX, realY);
	}
	else if (type == MOUSESCROLL)//scroll
	{
		HandleMouseScroll(scrolling);
	}
}

void SceneLose::HandleMouseLeft(int x, int y)
{
	for (int i = UIList.size() - 1; i >= 0; i--)
	{
		UIObject* ui1 = dynamic_cast<UIObject*>(UIList[i]);
		if (ui1 != nullptr)
		{
			if (ui1->OnMouseStay(x, y))
			{

			}
		}
	}
	for (int i = UIList.size() - 1; i >= 0; i--)
	{
		UIObject* ui1 = dynamic_cast<UIObject*>(UIList[i]);
		if (ui1 != nullptr)
		{
			if (ui1->ClickOn(x, y))
			{
				return;
			}
		}
	}
}
void SceneLose::HandleMouseRight(int x, int y)
{

}
void SceneLose::HandleMouseMove(int x, int y)
{
	bool hitbutton = false;
	for (int i = UIList.size() - 1; i >= 0; i--)
	{
		UIObject* ui1 = dynamic_cast<UIObject*>(UIList[i]);
		if (ui1 != nullptr)
		{
			if (ui1->OnMouseStay(x, y))
			{
				hitbutton = true;
				if (prevbutton != ui1)
				{
					prevbutton = ui1;
					GameEngine::GetInstance()->playSound("../Resource/Sound/Click.mp3");
				}
				break;
			}
		}
	}
	if (hitbutton == false)
	{
		prevbutton = nullptr;
	}
}

