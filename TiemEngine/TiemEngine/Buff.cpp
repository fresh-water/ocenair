#include "Buff.h"

Buff::Buff(int type, int effect, int duration)
{
	this->type = type; 
	this->effect = effect;
	this->duration = duration;
}
int Buff::getEffect()
{
	return effect;
}
int Buff::getType()
{
	return type;
}
int Buff::getDuration(){
	return duration;
}
void Buff::decreaseDuration(int amount)
{
	duration = duration - amount;
}
void Buff::extendDuration(int amount)
{
	duration = duration + amount;
}
Buff::~Buff()
{

}