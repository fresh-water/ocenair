#pragma once
#include "GameEngine.h"
#include "SquareMeshVbo.h"
#include "GameObject.h"
#include "GameData.h"
#include "ImageObject.h"
#include "SpriteObject.h"
#include "Camera.h"
#include "Board.h"
#include "EnumList.h"
#include "Character.h"
#include "Submarine.h"
#include "Enemy.h"
#include "UIObject.h"
#include "GameDataStorage.h"
#include "UIsetting.h"

class Level
{
private:
	vector<DrawableObject*> objectsList;
	vector<DrawableObject*> UIList;
	Camera* maincamera;
	Board* mainboard;
	int mainstate;
protected:
	UIObject* prevbutton;
public:
	virtual void LevelLoad();
	virtual void LevelInit();
	virtual void LevelUpdate();
	virtual void LevelDraw();
	virtual void LevelFree();
	virtual void LevelUnload();

	virtual void HandleKey(char key);
	virtual void HandleMouse(int type, int x, int y);

	virtual void HandleMouseLeft(int x, int y);
	virtual void HandleMouseRight(int x, int y);
	virtual void HandleMouseMove(int x, int y);
	virtual void HandleMouseScroll(int amount);

	virtual int WorldtoBoardX(float x);
	virtual int WorldtoBoardY(float y);

	virtual float ScreentoWorldX(float x);
	virtual float ScreentoWorldY(float y);
};
