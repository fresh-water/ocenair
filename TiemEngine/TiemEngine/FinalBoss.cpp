#include "FinalBoss.h"

vector<glm::vec2> FinalBoss::getHitbox()
{
	return hitbox;
}
void FinalBoss::setHitbox()
{
	for (int j = -3; j <= 1; j++)
	{
		for (int i = -2; i <= 2; i++)
		{
			hitbox.push_back(glm::vec2(this->getBoardPosition().x + i, this->getBoardPosition().y + j));
		}
	}
}
void FinalBoss::readCharacterData(string filename, int index)
{
	Enemy::readCharacterData(filename,index);
}
vector<glm::vec2> FinalBoss::getHurtbox()
{
	return hurtbox;
}
void FinalBoss::setHurtbox()
{
	for (int j = -2; j <= 0; j++)
	{
		for (int i = -1; i <= 1; i++)
		{
			hurtbox.push_back(glm::vec2(this->getBoardPosition().x + i, this->getBoardPosition().y + j));
		}
	}
}
void FinalBoss::setActive(bool fact)
{
	if (fact)
	{
		bossisActive = true;
		Enemy::setActive(true);
	}
	else
	{
		if (Enemy::getActive())
		{
			Enemy::setActive(false);
		}
		else
		{
			bossisActive = false;
		}
	}
}
bool FinalBoss::getActive()
{
	if (bossisActive || Enemy::getActive())
	{
		return true;
	}
	else
	{
		return false;
	}
}
glm::vec2 FinalBoss::getBoardPosition()
{
	return Unit::getBoardPosition();
}

vector<glm::vec2> FinalBoss::ActiveSkill(int** typearr, int sizex, int sizey)
{
	return hitbox;
}

void FinalBoss::Render(glm::mat4 globalModelTransform)
{
	Unit::Render(globalModelTransform);
}

FinalBoss::FinalBoss(string name, int hp, int maxhp, int atk, int atkdis, int move, int x, int y, int eva, int def, int cd, string tex)
	: Enemy(name, hp, maxhp, atk, atkdis, move, x, y, eva, def, cd, tex)
{

}
FinalBoss::FinalBoss():Enemy()
{

}