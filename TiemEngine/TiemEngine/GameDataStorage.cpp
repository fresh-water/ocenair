#include "GameDataStorage.h"
#include "GameEngine.h"

GameDataStorage* GameDataStorage::instance = nullptr;

GameDataStorage* GameDataStorage::GetInstance()
{
	if (instance == nullptr) {
		instance = new GameDataStorage();
	}
	return instance;
}
void GameDataStorage::Init()
{
	readMapSprite("../Resource/GameData/Mapsprite.txt");
	readMapFileName("../Resource/GameData/MapList.txt");
	readCharacterStatusFile("../Resource/GameData/Character Status.txt");
	readEnemyStatusFile("../Resource/GameData/Enemy Status.txt");
}
void GameDataStorage::UpdateSaveFile()
{
	string line;
	vector<string> strstorage;
	int alive = 0;
	ofstream MyFile("../Resource/GameData/SavedData.txt");
	if (!MyFile.is_open()) {
		cout << "Error" << endl;
		return;
	}
	MyFile << "����	Alive" << endl;
	MyFile << "CurrentLevel	" << unlockedlevel << endl;
	for (int i = 0; i < 6; i++)
	{
		strstorage.push_back("N 0");
	}
	for (int i=0;i<RemainingCharacter.size();i++)
	{
		strstorage[RemainingCharacter[i]] = "N 1";
	}
	for (int i = 0; i < 6; i++)
	{
		MyFile << strstorage[i] << endl;
	}
	MyFile.close();
}
void GameDataStorage::loadSaveFile()
{
	vector<int> empty;
	RemainingCharacter = empty;
	string line;
	int alive = 0;
	ifstream MyFile("../Resource/GameData/SavedData.txt");
	if (!MyFile.is_open()) {
		cout << "Error" << endl;
		return;
	}
	getline(MyFile, line);
	MyFile >> line;
	MyFile >> unlockedlevel;
	currentlevel = unlockedlevel;
	for (int i = 0; i < 6; i++)
	{
		MyFile >> line;
		MyFile >> alive;
		if (alive == 1)
		{
			RemainingCharacter.push_back(i);
		}
	}
	MyFile.close();
}
void GameDataStorage::readCharacterStatusFile(string filename)
{
	string line;
	int atk, movedis, eva, def, atkdis, maxhp, oxygen, row, column;
	string name;
	string texture, atktexture, atkeffecttexture, SItexture;
	int LOSin, LOSout;
	ifstream MyFile(filename);
	if (!MyFile.is_open()) {
		cout << "Error" << endl;
		return;
	}
	getline(MyFile, line);
	getline(MyFile, line);
	for(int i = 0; i < 6 ; i++)
	{
		MyFile >> name;
		MyFile >> maxhp;
		MyFile >> atk;
		MyFile >> movedis;
		MyFile >> atkdis;
		MyFile >> eva;
		MyFile >> def;
		MyFile >> oxygen;
		MyFile >> LOSin;
		MyFile >> LOSout;
		MyFile >> texture;
		MyFile >> row;
		MyFile >> column;

		SpriteInfo si1;
		si1.texture = texture;
		si1.row = row;
		si1.col = column;
		characterInfo[i] = si1;

		MyFile >> atktexture;
		MyFile >> atkeffecttexture;
		MyFile >> row;
		MyFile >> column;

		si1.texture = atkeffecttexture;
		si1.row = row;
		si1.col = column;
		EffectInfo[name] = si1;

		MyFile >> SItexture;
		Skillicon[name] = SItexture;
	}
	MyFile.close();
}
void GameDataStorage::readEnemyStatusFile(string filename)
{
	string line;
	int atk, movedis, eva, def, atkdis, maxhp, oxygen, row, column;
	string name;
	string texture, atktexture, atkeffecttexture, SItexture;
	ifstream MyFile(filename);
	if (!MyFile.is_open()) {
		cout << "Error" << endl;
		return;
	}
	getline(MyFile, line);
	for (int i = 0; i < 10; i++)
	{
		MyFile >> name;
		MyFile >> maxhp;
		MyFile >> atk;
		MyFile >> movedis;
		MyFile >> atkdis;
		MyFile >> eva;
		MyFile >> def;
		MyFile >> texture;
		MyFile >> row;
		MyFile >> column;

		SpriteInfo si1;
		si1.texture = texture;
		si1.row = row;
		si1.col = column;
		enemyInfo[i] = si1;

		MyFile >> atktexture;
		MyFile >> atkeffecttexture;
		MyFile >> row;
		MyFile >> column;

		si1.texture = atkeffecttexture;
		si1.row = row;
		si1.col = column;
		EffectInfo[name] = si1;
	}

	MyFile.close();
}
void GameDataStorage::readMapSprite(string filename)
{
	string note;
	string key;
	int row, col;
	ifstream MyFile(filename);
	if (!MyFile.is_open()) {
		cout << "Error" << endl;
		return;
	}
	else
	{
		getline(MyFile, note);
		MyFile >> mapSpriteName;
		while (!MyFile.eof())
		{
			MyFile >> note;
			MyFile >> key;
			MyFile >> row;
			MyFile >> col; 
			UVmapSprite saveuv;
			saveuv.row = row;
			saveuv.col = col;
			mapsprite[key] = saveuv;
		}

		MyFile.close();
	}
}
void GameDataStorage::readSoundFile(string filename)
{
	string note;
	string key;
	string path;
	int vol;
	ifstream MyFile(filename);
	if (!MyFile.is_open()) {
		cout << "Error" << endl;
		return;
	}
	else
	{
		getline(MyFile, note);
		while (!MyFile.eof())
		{
			MyFile >> note;
			MyFile >> key;
			MyFile >> vol;
			MyFile >> path;
			SoundInfo soundtemp;
			soundtemp.volume = vol;
			soundtemp.soundpath = path;
			soundInfo[key] = soundtemp;
		}

		MyFile.close();
	}
}
void GameDataStorage::readMapFileName(string filename)
{
	string note;
	string key;
	int charnum;
	int count = 0;
	ifstream MyFile(filename);
	if (!MyFile.is_open()) {
		cout << "Error" << endl;
		return;
	}
	else
	{
		getline(MyFile, note);
		while (!MyFile.eof())
		{
			MyFile >> key;
			MyFile >> note;
			mapfilename[count] = note;
			MyFile >> charnum;
			numofchar[count] = charnum;
			count++;
		}

		MyFile.close();
	}
}

unsigned int GameDataStorage::gettexturestorage(string name)
{
	if (TextureStorage.count(name) == 0) {
		TextureStorage[name] = GameEngine::GetInstance()->GetRenderer()->LoadTexture(name);
	}
	return TextureStorage[name];
}

UVmapSprite GameDataStorage::getMapSpriteUV(string key)
{
	if (mapsprite.count(key) == 0) {
		cout << "Error: Key not valid.";
		cout << "mapsprite = ";
		cout << key << endl;
		UVmapSprite saveuv;
		saveuv.row = -1;
		saveuv.col = -1;
		return saveuv;
	}
	return mapsprite[key];
}
SpriteInfo GameDataStorage::getCharacterSpriteInfo(int key)
{
	if (characterInfo.count(key) == 0) {
		cout << "Error: Key not valid.";
		cout << "CharacterSprite" << endl;
		SpriteInfo empty;
		empty.texture = "NULL";
		empty.row = -1;
		empty.col = -1;
		return empty;
	}
	return characterInfo[key];
}
SpriteInfo GameDataStorage::getEnemySpriteInfo(int key)
{
	if (enemyInfo.count(key) == 0) {
		cout << "Error: Key not valid.";
		cout << "EnemySprite" << endl;
		SpriteInfo empty;
		empty.texture = "NULL";
		empty.row = -1;
		empty.col = -1;
		return empty;
	}
	return enemyInfo[key];
}
SpriteInfo GameDataStorage::getEffectInfo(string name)
{
	if (EffectInfo.count(name) == 0) {
		cout << "Error: Key not valid.";
		cout << "Effect" << endl;
		SpriteInfo empty;
		empty.texture = "NULL";
		empty.row = -1;
		empty.col = -1;
		return empty;
	}
	return EffectInfo[name];
}
string GameDataStorage::getSkillIcon(string name)
{
	if (Skillicon.count(name) == 0) {
		cout << "Error: Key not valid.";
		return "NULL";
	}
	return Skillicon[name];
}
string GameDataStorage::getMapFileName(int levelnum)
{
	return mapfilename[levelnum];
}
string GameDataStorage::getMapSpriteName()
{
	return mapSpriteName;
}
SoundInfo GameDataStorage::getSoundInfo(string key) {
	if (soundInfo.count(key) == 0) {
		cout << "Error: Key not valid.";
		SoundInfo empty;
		empty.volume = -1;
		empty.soundpath = "NULL";
		return empty;
	}
	return soundInfo[key];
}
int GameDataStorage::getSelectedCharacter(int index)
{
	return selectedCharacter[index];
}
void GameDataStorage::setSelectedCharacter(vector<int> select)
{
	selectedCharacter = select;
}
int GameDataStorage::getNumofchar(int key)
{
	return numofchar[key];
}
int GameDataStorage::getNumofcharcurrentlevel()
{
	return numofchar[currentlevel];
}
int GameDataStorage::getCurrlevelnumber()
{
	return currentlevel;
}
int GameDataStorage::getUnlocklevelnumber()
{
	return unlockedlevel;
}
void GameDataStorage::setNextlevelnumber(int num)
{
	currentlevel = num;
	if (currentlevel > unlockedlevel) {
		if(num >= 16)
		{ 
			num = 16;
		}
		unlockedlevel = num;
	}
}
void GameDataStorage::setOpenvision(bool fact)
{
	openvision = fact;
}
bool GameDataStorage::getOpenvision()
{
	return openvision;
}
void GameDataStorage::ResaveFile()
{
	ofstream MyFile("../Resource/GameData/SavedData.txt");
	if (!MyFile.is_open()) {
		cout << "Error" << endl;
		return;
	}
	MyFile << "����	Alive" << endl;
	MyFile << "CurrentLevel	" << 1 << endl;
	for (int i = 0; i < 6; i++)
	{
		MyFile << "N 1" << endl;
	}
	MyFile.close();
}
vector<int> GameDataStorage::getRemainCharacter()
{
	return RemainingCharacter;
}
void GameDataStorage::killCharacter(string name)
{
	int num = -1;
	if (name == "Ash"){
		num = 0;
	}
	else if (name == "Todd")
	{
		num = 1;
	}
	else if (name == "Might")
	{
		num = 2;
	}
	else if (name == "Wong")
	{
		num = 3;
	}
	else if (name == "Gray")
	{
		num = 4;
	}
	else if (name == "Kim")
	{
		num = 5;
	}
	vector<int> afterkill;
	for (int i=0; i < RemainingCharacter.size();i++)
	{
		if (RemainingCharacter[i] != num)
		{
			afterkill.push_back(RemainingCharacter[i]);
		}
	}
	RemainingCharacter = afterkill;
}
