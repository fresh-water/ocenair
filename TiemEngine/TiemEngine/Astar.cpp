#include "Astar.h"

vector<glm::vec2> Astar(Board board,int type ,glm::vec2 start, glm::vec2 end)
{
	return Astar(board.GetWholeTypeArr(), board.GetSizeX(), board.GetSizeY(),type,start,end);
}
vector<glm::vec2> Astar(int** typearr, int sizex, int sizey, int type, glm::vec2 start, glm::vec2 end)
{
	int count = 0;
	vector<Node*> openlist;
	vector<Node*> closelist;
	bool same = false;

	Node* start_node = new Node(nullptr, start);
	start_node->g = start_node->f = start_node->h = 0;
	Node* end_node = new Node(nullptr, end);
	end_node->g = end_node->f = end_node->h = 0;

	openlist.push_back(start_node);

	while (openlist.size() > 0)
	{
		int allF = 1000;
		int leastfindex = 0;

		for (int i = 0; i < openlist.size(); i++)
		{
			if (allF > openlist[i]->f)
			{
				same = false;
				for (int j = 0; j < closelist.size(); j++)
				{
					if (closelist[j]->position == openlist[i]->position)
					{
						same = true;
						break;
					}
				}
				if (same == false)
				{
					leastfindex = i;
					allF = openlist[i]->f;
				}
			}
		}

		Node* currentNode = openlist[leastfindex];

		openlist.erase(openlist.begin() + leastfindex);
		closelist.push_back(currentNode);
		if (currentNode->position == end_node->position)
		{
			vector<glm::vec2> result;
			Node* current = currentNode;
			if (type == AT_MOVE)
			{
				result.push_back(end);
			}
			while (current->parent != nullptr)
			{
				result.push_back(current->position);
				current = current->parent;
			}
			result.push_back(start);
			vector<glm::vec2> reverse;
			for (int i = result.size() - 1; i > 0; i--)
			{
				reverse.push_back(result[i]);
			}
			return reverse;
		}
		else
		{
			int dx, dy;
			glm::vec2 newposition[4] = { glm::vec2(1,0),glm::vec2(0,1) ,glm::vec2(-1,0) ,glm::vec2(0,-1) };
			for (int i = 0; i < 4; i++)
			{
				int posx = currentNode->position.x + newposition[i].x;
				int posy = currentNode->position.y + newposition[i].y;
				if (posx <= sizex - 1 && posx >= 0
					&& posy <= sizey - 1 && posy >= 0)
				{
					if (type == AT_MOVE)
					{
						if (typearr[posx][posy] != FLOOR && typearr[posx][posy] != UNIT) {
							continue;
						}
					}
					else if (type == AT_ATK)
					{
						if (typearr[posx][posy] == MOUNT || typearr[posx][posy] == ENEMY) {
							continue;
						}
					}

					Node* child = new Node(currentNode, glm::vec2(posx, posy));
					same = false;
					for (int j = 0; j < closelist.size(); j++)
					{
						if (closelist[j]->position == child->position)
						{
							same = true;
							break;
						}
					}
					if (same == false)
					{
						dx = currentNode->position.x - end_node->position.x;
						dy = currentNode->position.y - end_node->position.y;
						child->g = currentNode->g + 1;
						child->h = dx * dx + dy * dy;
						child->f = child->g + child->h;
						openlist.push_back(child);
					}
				}
			}
		}
	}
	//cout << "EMPTY" << endl;
	vector<glm::vec2> empty;
	empty.push_back(glm::vec2(-1,-1));
	return empty;
}

vector<glm::vec2> UnitOpenSpaceAstar(Board board, int type, int distant, glm::vec2 pos)
{
	return UnitOpenSpaceAstar(board.GetWholeTypeArr(), board.GetSizeX(), board.GetSizeY(), type, distant, pos);
}
vector<glm::vec2> UnitOpenSpaceAstar(int** typearr, int sizex, int sizey, int type, int distant, glm::vec2 pos)
{
	vector<Node*> openlist;
	vector<Node*> closelist;
	vector<glm::vec2> result;

	Node* start_node = new Node(nullptr, pos);
	start_node->g = start_node->f = start_node->h = 0;

	openlist.push_back(start_node);

	while (openlist.size() > 0)
	{
		int allF = 1000;
		int leastfindex = 0;


		for (int i = 0; i < openlist.size(); i++)
		{
			bool isSame = false;
			if (allF > openlist[i]->f)
			{
				for (int j = 0; j < closelist.size(); j++)
				{
					if (closelist[j]->position == openlist[i]->position)
					{
						isSame = true;
						break;
					}
				}
				if (isSame == false)
				{
					leastfindex = i;
					allF = openlist[i]->f;
				}
			}
		}

		Node* currentNode = openlist[leastfindex];

		openlist.erase(openlist.begin() + leastfindex);
		closelist.push_back(currentNode);
		glm::vec2 newposition[4] = { glm::vec2(1,0),glm::vec2(0,1) ,glm::vec2(-1,0) ,glm::vec2(0,-1) };
		for (int i = 0; i < 4; i++)
		{
			int posx = currentNode->position.x + newposition[i].x;
			int posy = currentNode->position.y + newposition[i].y;
			if (posx <= sizex - 1 && posx >= 0 && posy <= sizey - 1 && posy >= 0)
			{
				if (type == AT_MOVE)
				{
					if (typearr[posx][posy] != FLOOR && typearr[posx][posy] != UNIT) {
						continue;
					}
				}
				else if (type == AT_ATK)
				{
					if (typearr[posx][posy] == MOUNT)
					{
						continue;
					}
				}

				Node* child = new Node(currentNode, glm::vec2(posx, posy));
				bool isSame = false;
				for (int j = 0; j < closelist.size(); j++)
				{
					if (closelist[j]->position == child->position)
					{
						isSame = true;
						break;
					}
				}
				if (isSame == true)
				{
					continue;
				}
				child->g = currentNode->g + 1;
				child->h = 0;
				child->f = child->g + child->h;
				if (child->f > distant)
				{
					continue;
				}
				openlist.push_back(child);
				vector<glm::vec2>::iterator it = std::find(result.begin(), result.end(), child->position);
				if (it == result.end())
				{
					result.push_back(child->position);
				}
			}
		}
	}
	if (type == AT_MOVE)
	{
		result.push_back(pos);
	}
	else if (type == AT_ATK)
	{
		result.push_back(pos);
	}
	return result;
}
int EnemyFindclosestPlayer(vector<Unit*> unitarr, Enemy* enemy)
{
	int leastindex = -1;
	int leastdis = 10000;
	vector<glm::vec2> playerpos;
	for (int i = 0; i < unitarr.size(); i++)
	{
		Character* c1 = dynamic_cast<Character*>(unitarr[i]);
		if (c1 != nullptr && c1->getDraw() == true && c1->getisDown() == false)
		{
			playerpos.push_back(c1->getBoardPosition());
		}
		Submarine* s1 = dynamic_cast<Submarine*>(unitarr[i]);
		if (s1 != nullptr && s1->getDraw() == true)
		{
			playerpos.push_back(s1->getBoardPosition());
		}
	}
	for (int i = 0; i < playerpos.size(); i++)
	{
		int distance = abs(enemy->getBoardPosition().x - playerpos[i].x) + abs(enemy->getBoardPosition().y - playerpos[i].y);
		if (distance < leastdis)
		{
			leastindex = i;
			leastdis = distance;
		}
	}
	return leastdis;
}
void EnemyAstar2(Board board, Enemy* enemy)
{
	vector<glm::vec2> playerpos;
	for (int i = 0; i < board.GetAmountUnit(); i++)
	{
		Character* c1 = dynamic_cast<Character*>(board.GetUnitVector(i));
		if (c1 != nullptr && c1->getDraw() == true && c1->getisDown() == false)
		{
			playerpos.push_back(c1->getBoardPosition());
		}
		Submarine* s1 = dynamic_cast<Submarine*>(board.GetUnitVector(i));
		if (s1 != nullptr && s1->getDraw() == true)
		{
			playerpos.push_back(s1->getBoardPosition());
		}
	}
	int leastindex = -1;
	int leastdis = 10000;
	for (int i = 0; i < playerpos.size(); i++)
	{
		int distance = abs(enemy->getBoardPosition().x - playerpos[i].x) + abs(enemy->getBoardPosition().y - playerpos[i].y);
		if (distance < leastdis)
		{
			leastindex = i;
			leastdis = distance;
		}
	}
	enemy->setEnemyTargetpos(playerpos[leastindex]);
	if (leastdis > enemy->getAtkdis())
	{
		int currentcount = 0;
		int count = playerpos.size();
		while(currentcount < count)
		{
			int leastindex2 = -1;
			int leastdis2 = 10000;
			for (int i = 0; i < playerpos.size(); i++)
			{
				int distance = abs(enemy->getBoardPosition().x - playerpos[i].x) + abs(enemy->getBoardPosition().y - playerpos[i].y);
				if (distance < leastdis2)
				{
					leastindex2 = i;
					leastdis2 = distance;
				}
			}
			enemy->setEnemyTargetpos(playerpos[leastindex2]);

			enemy->setEnemypath(EnemyAstar(board, enemy->getAtkdis(), enemy->getBoardPosition(), enemy->getTargetpos()));
			currentcount++;
			if (enemy->getEnemypath()[0] != glm::vec2(-1,-1)){ break; }
			else { playerpos.erase(playerpos.begin() + leastindex2); }
		}
	}
	else
	{
		vector<glm::vec2> empty;
		enemy->setEnemypath(empty);
	}
}
vector<glm::vec2> EnemyAstar(Board board, int hitdistance, glm::vec2 start, glm::vec2 end)
{
	//vector<glm::vec2> temppath;
	//int distance = abs(start.x - end.x) + abs(start.y - end.y);
	//if (distance <= hitdistance)
	//{
	//	return temppath;
	//}
	vector<glm::vec2> path = Astar(board,AT_ATK,start,end);
	return path;
}