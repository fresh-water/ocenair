#include "Board.h"
#include "Astar.h"
#include <algorithm>
#include <sstream>

Board::Board()
{
	
}

void Board::Init(int x, int y)
{
	uimanager.Init();
	SelectedUnitX = -1;
	SelectedUnitY = -1;
	mainstate = BS_NORMAL;
	SizeX = x;
	SizeY = y;
	TypeArr = new int * [SizeX];
	SpecialBlockArr = new SpecialBlock * [SizeX];
	MapSpriteArr = new string * [SizeX];
	VisionArr = new int* [SizeX];
	TempArr = new int* [SizeX];
	BoardArr = new GameObject ** [SizeX];
	TempBoardArr = new GameObject **[SizeX];
	EffectBoardArr = new GameObject **[SizeX];
	GridObj = new GameObject * *[SizeX];
	AtkBoardFXArr = new GameObject * *[SizeX];
	GoalArr = new int* [SizeX];
	for(int i=0;i< SizeX;i++)
	{
		TypeArr[i] = new int[SizeY];
		SpecialBlockArr[i] = new SpecialBlock[SizeY];
		MapSpriteArr[i] = new string[SizeY];
		VisionArr[i] = new int[SizeY];
		TempArr[i] = new int[SizeY];
		GoalArr[i] = new int[SizeY];
		BoardArr[i] = new GameObject *[SizeY];
		TempBoardArr[i] = new GameObject * [SizeY];
		EffectBoardArr[i] = new GameObject * [SizeY];
		GridObj[i] = new GameObject * [SizeY];
		AtkBoardFXArr[i] = new GameObject * [SizeY];
	}
	for (int j = 0; j < SizeY; j++)
	{
		for (int i = 0; i < SizeX; i++)
		{
			BoardArr[i][j] = new GameObject();
			TempBoardArr[i][j] = new GameObject();
			EffectBoardArr[i][j] = new GameObject();
			GridObj[i][j] = new GameObject();
			AtkBoardFXArr[i][j] = new GameObject();
			VisionArr[i][j] = VS_FAR;
			TypeArr[i][j] = 0;
			SpecialBlock sb1; sb1.effect = 0; sb1.type = SB_NORMAL; sb1.duration = 1;
			SpecialBlockArr[i][j] = sb1;
			TempArr[i][j] = 0;
			GoalArr[i][j] = 0;
			MapSpriteArr[i][j] = "";
		}
	}
	Cursor = new GameObject();
	Cursor->SetSize(-100, -100);
	Cursor->SetPosition(glm::vec3((-GameEngine::GetInstance()->GetWindowWidth() / 2.0f) + 0 * 100 + 100 / 2.0f,
		(-GameEngine::GetInstance()->GetWindowHeight() / 2.0f) + 0 * 100 + 100 / 2.0f, 0.0f));
	Cursor->SetMode(1);
	Cursor->setPath("../Resource/Texture/Cursor.png",1,15);
	Cursor->SetAnimationLoop(0,0,15,10);
}
void Board::setCamera(Camera* maincamera)
{
	cam = maincamera;
}
void Board::SetMapSprite()
{
	string name = GameDataStorage::GetInstance()->getMapSpriteName();
	unsigned int texture = GameDataStorage::GetInstance()->gettexturestorage(name);
	for (int j = 0; j < SizeY; j++)
	{
		for (int i = 0; i < SizeX; i++)
		{
			UVmapSprite uv = GameDataStorage::GetInstance()->getMapSpriteUV(MapSpriteArr[i][j]);
			BoardArr[i][j]->SetMode(1);
			BoardArr[i][j]->setUVmap(texture, uv.row, uv.col);
		}
	}
}

void Board::SetBoardDefault()
{
	for (int j = 0; j < SizeY; j++)
	{
		for (int i = 0; i < SizeX; i++)
		{
			TempBoardArr[i][j]->SetColor(0.0, 0.0, 0.0, 0.0);
			TempBoardArr[i][j]->SetSize(100, -100);
			TempBoardArr[i][j]->SetPosition(glm::vec3((-GameEngine::GetInstance()->GetWindowWidth() / 2.0f) + i * 100 + 100 / 2.0f,
				(-GameEngine::GetInstance()->GetWindowHeight() / 2.0f) + j * 100 + 100 / 2.0f, 0.0f));
			EffectBoardArr[i][j]->SetColor(0.0, 0.0, 0.0, 0.0);
			EffectBoardArr[i][j]->SetSize(100, -100);
			EffectBoardArr[i][j]->SetPosition(glm::vec3((-GameEngine::GetInstance()->GetWindowWidth() / 2.0f) + i * 100 + 100 / 2.0f,
				(-GameEngine::GetInstance()->GetWindowHeight() / 2.0f) + j * 100 + 100 / 2.0f, 0.0f));
			BoardArr[i][j]->SetSize(100, -100);
			BoardArr[i][j]->SetPosition(glm::vec3((-GameEngine::GetInstance()->GetWindowWidth() / 2.0f) + i * 100 + 100 / 2.0f,
				(-GameEngine::GetInstance()->GetWindowHeight() / 2.0f) + j * 100 + 100 / 2.0f, 0.0f));
			GridObj[i][j]->SetColor(0.0, 0.0, 0.0, 0.1);
			GridObj[i][j]->SetSize(100, -100);
			GridObj[i][j]->SetMode(1);
			//unsigned int tex = GameEngine::GetInstance()->GetRenderer()->LoadTexture("../Resource/Texture/Gridtex.png");
			GridObj[i][j]->SetTexture("../Resource/Texture/Gridtex.png");
			GridObj[i][j]->SetPosition(glm::vec3((-GameEngine::GetInstance()->GetWindowWidth() / 2.0f) + i * 100 + 100 / 2.0f,
				(-GameEngine::GetInstance()->GetWindowHeight() / 2.0f) + j * 100 + 100 / 2.0f, 0.0f));
			AtkBoardFXArr[i][j]->SetSize(100, -100);
			AtkBoardFXArr[i][j]->SetMode(1);
			AtkBoardFXArr[i][j]->setDraw(false);
			AtkBoardFXArr[i][j]->SetPosition(glm::vec3((-GameEngine::GetInstance()->GetWindowWidth() / 2.0f) + i * 100 + 100 / 2.0f,
				(-GameEngine::GetInstance()->GetWindowHeight() / 2.0f) + j * 100 + 100 / 2.0f, 0.0f));
		}
	}
}
void Board::BoardReadfile(string filename, int* sizeX, int* sizeY)
{
	ifstream file1(filename);
	if (!file1.is_open())
	{
		cout << "Error Reading File" << endl;
	}
	else
	{
		this->goaltype = ELIMINATE;
		string temp;
		char chartemp1,chartemp2;
		getline(file1, temp);
		int sizex = -1, sizey = -1, type = 0, index = 0;
		bool fact;
		file1 >> sizex;
		file1 >> sizey;
		*sizeX = sizex;
		*sizeY = sizey;
		this->Init(sizex, sizey);
		this->SetBoardDefault();
		//char
		file1 >> temp;
		file1 >> charactercount;
		//enemy
		file1 >> temp;
		file1 >> enemycount;
		if (enemycount == -1)
		{
			this->goaltype = REACHGOAL;
		}
		else if (enemycount == -2)
		{
			this->goaltype = TARGET;
		}
		//goal
		file1 >> temp;
		file1 >> temp;
		for (int j = 0; j < sizey; j++)
		{
			for (int i = 0; i < sizex; i++)
			{
				file1 >> temp;
				if (temp == "F") { type = FLOOR; }
				else if (temp[0] == 'C') 
				{
					index = (int)temp[1] - 48;
					//cout << "index = " << index << endl;
					if (index > GameDataStorage::GetInstance()->getRemainCharacter().size())
					{
						type = FLOOR;
					}
					else
					{
						index = (GameDataStorage::GetInstance()->getSelectedCharacter(index - 1) + 2);
						Character* unit1 = new Character();
						unit1->readCharacterData("../Resource/GameData/Character Status.txt", index);
						this->AddUnit(unit1, i, sizey - 1 - j);
						type = CHARACTER;
					}
				}
				else if (temp == "S") 
				{ 
					Submarine* unit1 = new Submarine();
					unit1->readCharacterData("../Resource/GameData/Character Status.txt", 1);
					this->AddUnit(unit1, i, sizey - 1 - j);
					type = CHARACTER;
				}
				else if (temp[0] == 'E') 
				{ 
					stringstream ss(temp);
					ss >> chartemp1;
					ss >> index;
					ss >> chartemp2;
					Enemy* unit1 = new Enemy();
					unit1->readCharacterData("../Resource/GameData/Enemy Status.txt", index);
					fact = (chartemp2 == 'T')? true:false;
					unit1->setFoundPlayer(fact);
					this->AddUnit(unit1, i, sizey - 1 - j);
					type = ENEMY;
				}
				else if (temp[0] == 'T')
				{
					stringstream ss(temp);
					ss >> chartemp1;
					ss >> index;
					ss >> chartemp2;
					if (index != 10)
					{
						Enemy* unit1 = new Enemy();
						unit1->readCharacterData("../Resource/GameData/Enemy Status.txt", index);
						this->Targetindex = UnitVector.size();
						fact = (chartemp2 == 'T') ? true : false;
						unit1->setFoundPlayer(fact);
						this->AddUnit(unit1, i, sizey - 1 - j);
					}
					else
					{
						FinalBoss* unit1 = new FinalBoss();
						unit1->readCharacterData("../Resource/GameData/Enemy Status.txt", index);
						this->Targetindex = UnitVector.size();
						fact = (chartemp2 == 'T') ? true : false;
						unit1->setFoundPlayer(fact);
						this->AddUnit(unit1, i, sizey - 1 - j);
					}
					type = ENEMY;
				}
				else if (temp == "M") { type = MOUNT; }
				else if (temp == "R") { type = RIVER; }
				TypeArr[i][sizey - 1 - j] = type;
			}
		}
		//Mapsprite
		file1 >> temp;
		for (int j = 0; j < sizey; j++)
		{
			for (int i = 0; i < sizex; i++)
			{
				file1 >> MapSpriteArr[i][sizey - 1 - j];
			}
		}
		//SpecialBlock
		file1 >> temp;
		for (int j = 0; j < sizey; j++)
		{
			for (int i = 0; i < sizex; i++)
			{
				file1 >> temp;
				if (temp[0] == 'N')
				{
					continue;
				}

				SpecialBlock sb1;
				if (temp[0] == 'O'){ sb1.type = SB_O2; }
				else if (temp[0] == 'A') { sb1.type = SB_ATK; }
				else if (temp[0] == 'U') 
				{ 
					if (TypeArr[i][sizey - 1 - j] == FLOOR)
					{
						TypeArr[i][sizey - 1 - j] = UNIT;
					}
					continue;
				}
				else if (temp[0] == 'S') { sb1.type = SB_SPEED; }
				else if (temp[0] == 'H') { sb1.type = SB_HEAL; }
				else if (temp[0] == 'h') { sb1.type = SB_HIT; }
				else if (temp[0] == 'D') { sb1.type = SB_DOOR; TypeArr[i][sizey - 1 - j] = MOUNT; }
				else if (temp[0] == 'K') { sb1.type = SB_KEY; }
				else if (temp[0] == 'G') { sb1.type = SB_GATE; TypeArr[i][sizey - 1 - j] = MOUNT; }
				else if (temp[0] == 'B') { sb1.type = SB_BUTTON; }

				stringstream ss(temp);
				char tempchar;
				ss >> tempchar;
				ss >> sb1.effect;
				sb1.duration = 1;
				SpecialBlockArr[i][sizey - 1 - j] = sb1;
			}
		}

		if (this->goaltype == REACHGOAL)
		{
			//Goal
			file1 >> temp;
			for (int j = 0; j < sizey; j++)
			{
				for (int i = 0; i < sizex; i++)
				{
					file1 >> GoalArr[i][sizey - 1 - j];
				}
			}
		}
		uimanager.SetQuestgoal(this->goaltype);

		file1.close();
	}
}

void Board::StartGame()
{
	for (int i = 0; i < UnitVector.size(); i++)
	{
		FinalBoss* boss = dynamic_cast<FinalBoss*>(UnitVector[i]);
		if (boss != nullptr && boss->getDraw() == true)
		{
			boss->setHurtbox();
			boss->setHitbox();
			for (int j = 0; j < boss->getHurtbox().size(); j++)
			{
				TypeArr[(int)boss->getHurtbox()[j].x][(int)boss->getHurtbox()[j].y] = ENEMY;
			}
			for (int j = 0; j < boss->getHitbox().size(); j++)
			{
				int x = boss->getHitbox()[j].x, y = boss->getHitbox()[j].y;
				//unsigned int tex = GameEngine::GetInstance()->GetRenderer()->LoadTexture("../Resource/Texture/EnemySprite/FinalBoss_Effect2.png");
				AtkBoardFXArr[x][y]->setDraw(true);
				AtkBoardFXArr[x][y]->setPath("../Resource/Texture/EnemySprite/FinalBoss_Effect2.png", 1, 7);
				AtkBoardFXArr[x][y]->SetAnimationLoop(0, 0, 7, 150);
				AtkBoardFXArr[x][y]->setLoop(false);
			}
			int posx = boss->getBoardPosition().x, posy = boss->getBoardPosition().y -1;
			EffectBoardArr[posx][posy]->SetMode(1);
			EffectBoardArr[posx][posy]->SetColor(0, 0, 0, 1);
			EffectBoardArr[posx][posy]->SetSize(500, -500);
			EffectBoardArr[posx][posy]->setPath("../Resource/Texture/EnemySprite/FinalBossAura.png", 1, 5);
			EffectBoardArr[posx][posy]->SetColorOffset(0,-1,-1);
			EffectBoardArr[posx][posy]->SetAnimationLoop(0, 0, 5, 100);
			EffectBoardArr[posx][posy]->SetFxtype(FXB_BOSS);
			EffectBoardArr[posx][posy]->setDraw(true);
		}
	}
	for (int i = 0; i < UnitVector.size(); i++)
	{
		Enemy* e1 = dynamic_cast<Enemy*>(UnitVector[i]);
		if (e1 != nullptr && e1->getDraw() == true)
		{
			e1->setActive(true);
		}
		Submarine* s1 = dynamic_cast<Submarine*>(UnitVector[i]);
		if (s1 != nullptr && s1->getDraw() == true)
		{
			cam->SetCameraPosition(s1->GetPosition().x, s1->GetPosition().y);
		}
	}

	vector<glm::vec3> colordoorkey = { glm::vec3(0,-1,-1), glm::vec3(-1,0,-1), glm::vec3(-1,-1,0), glm::vec3(0,0,-1), glm::vec3(-1,0,0) };
	string Doorname[2] = { "../Resource/Texture/mapsprite/Door1.png", "../Resource/Texture/mapsprite/Door2.png" };
	vector<glm::vec3> colorgatebutton = { glm::vec3(-1,0,0), glm::vec3(0,0,-1), glm::vec3(-1,-1,0), glm::vec3(-1,0,-1), glm::vec3(0,-1,-1) };
	for (int j = 0; j < SizeY; j++)
	{
		for (int i = 0; i < SizeX; i++)
		{
			if (SpecialBlockArr[i][j].type == SB_KEY)
			{
				EffectBoardArr[i][j]->setPath("../Resource/Texture/mapsprite/Key1.png", 1, 4);
				EffectBoardArr[i][j]->SetAnimationLoop(0, 0, 4, 100);
				EffectBoardArr[i][j]->setDraw(true);
				EffectBoardArr[i][j]->SetColor(0,0,0,1);
				EffectBoardArr[i][j]->SetMode(1);
				EffectBoardArr[i][j]->SetFxtype(SB_KEY);
				glm::vec3 color = colordoorkey[SpecialBlockArr[i][j].effect-1];
				EffectBoardArr[i][j]->SetColorOffset(color.x, color.y, color.z);
			}
			else if(SpecialBlockArr[i][j].type == SB_DOOR)
			{
				int x = rand() % 2;
				EffectBoardArr[i][j]->setPath(Doorname[x], 1, 1);
				EffectBoardArr[i][j]->setDraw(true);
				EffectBoardArr[i][j]->SetColor(0, 0, 0, 1);
				EffectBoardArr[i][j]->SetMode(1);
				EffectBoardArr[i][j]->SetFxtype(SB_DOOR);
				glm::vec3 color = colordoorkey[SpecialBlockArr[i][j].effect-1];
				EffectBoardArr[i][j]->SetColorOffset(color.x, color.y, color.z);
			}
			else if (SpecialBlockArr[i][j].type == SB_BUTTON)
			{
				EffectBoardArr[i][j]->setPath("../Resource/Texture/mapsprite/Button1.png", 1, 1);
				EffectBoardArr[i][j]->setDraw(true);
				EffectBoardArr[i][j]->SetColor(0, 0, 0, 1);
				EffectBoardArr[i][j]->SetMode(1);
				EffectBoardArr[i][j]->SetFxtype(SB_GATE);
				glm::vec3 color = colorgatebutton[SpecialBlockArr[i][j].effect-1];
				EffectBoardArr[i][j]->SetColorOffset(color.x, color.y, color.z);
			}
			else if (SpecialBlockArr[i][j].type == SB_GATE)
			{
				EffectBoardArr[i][j]->setPath("../Resource/Texture/mapsprite/Gate1.png", 1, 1);
				EffectBoardArr[i][j]->setDraw(true);
				EffectBoardArr[i][j]->SetColor(0, 0, 0, 1);
				EffectBoardArr[i][j]->SetMode(1);
				EffectBoardArr[i][j]->SetFxtype(SB_GATE);
				glm::vec3 color = colorgatebutton[SpecialBlockArr[i][j].effect-1];
				EffectBoardArr[i][j]->SetColorOffset(color.x, color.y, color.z);
			}
		}
	}

	SubO2Update();
}

void Board::AddUnit(Unit* unit, int posx, int posy)
{
	glm::vec3 unitoffsetpos = glm::vec3(0,15,0);
	glm::vec3 minihpbaroffsetpos = glm::vec3(0, -35, 0);
	glm::vec3 minio2baroffsetpos = glm::vec3(0, -45, 0);
	unit->SetMode(1);
	unit->SetSize(-118, -118);
	unit->SetPosition(BoardArr[posx][posy]->GetPosition() + unitoffsetpos);
	for (int i = 0; i < 2; i++)
	{
		unit->getminiHpbar(i)->SetPosition(BoardArr[posx][posy]->GetPosition() + minihpbaroffsetpos);
	}
	unit->setBoardPosition(posx,posy);
	unit->SetTexture(unit->getTexture());
	UnitVector.push_back(unit);
	
	Character* c1 = dynamic_cast<Character*>(unit);
	Enemy* e1 = dynamic_cast<Enemy*>(unit);
	Submarine* s1 = dynamic_cast<Submarine*>(unit);
	FinalBoss* boss = dynamic_cast<FinalBoss*>(unit);
	if (c1 != nullptr && c1->getDraw() == true)
	{
		for (int i = 0; i < 2; i++)
		{
			c1->getminiO2bar(i)->SetPosition(BoardArr[posx][posy]->GetPosition() + minio2baroffsetpos);
		}
		TypeArr[posx][posy] = CHARACTER;
	}
	if (e1 != nullptr && e1->getDraw() == true)
	{
		TypeArr[posx][posy] = ENEMY;
	}
	if (s1 != nullptr && s1->getDraw() == true)
	{
		TypeArr[posx][posy] = CHARACTER;
	}
	if (boss != nullptr && boss->getDraw() == true)
	{
		unit->SetSize(-100*5, -100*5);
		TypeArr[posx][posy] = ENEMY;
	}
}

void Board::PushBackallobj(vector<DrawableObject*>* objecttodraw, vector<DrawableObject*>* UItodraw)
{
	for (int i = 0; i < SizeX; i++)
	{
		for (int j = 0; j < SizeY; j++)
		{
			objecttodraw->push_back(BoardArr[i][j]);
		}
	}
	for (int i = 0; i < SizeX; i++)
	{
		for (int j = 0; j < SizeY; j++)
		{
			objecttodraw->push_back(EffectBoardArr[i][j]);
		}
	}
	for (int i = 0; i < SizeX; i++)
	{
		for (int j = 0; j < SizeY; j++)
		{
			objecttodraw->push_back(TempBoardArr[i][j]);
		}
	}
	for (int i = 0; i < SizeX; i++)
	{
		for (int j = 0; j < SizeY; j++)
		{
			objecttodraw->push_back(GridObj[i][j]);
		}
	}
	for (int i = 0; i < UnitVector.size(); i++)
	{
		FinalBoss* boss = dynamic_cast<FinalBoss*>(UnitVector[i]);
		if (boss != nullptr)
		{
			objecttodraw->push_back(UnitVector[i]);
		}
	}
	objecttodraw->push_back(Cursor);
	for (int i = 0; i < UnitVector.size(); i++)
	{
		FinalBoss* boss = dynamic_cast<FinalBoss*>(UnitVector[i]);
		if (boss == nullptr)
		{
			objecttodraw->push_back(UnitVector[i]);
		}
	}
	for (int i = 0; i < SizeX; i++)
	{
		for (int j = 0; j < SizeY; j++)
		{
			objecttodraw->push_back(AtkBoardFXArr[i][j]);
		}
	}
	for (int i = 0; i < UnitVector.size(); i++)
	{
		for (int j = 0; j < 2; j++)
		{
			objecttodraw->push_back(UnitVector[i]->getminiHpbar(j));
		}
		Character* c1 = dynamic_cast<Character*>(UnitVector[i]);
		if (c1 != nullptr)
		{
			for (int j = 0; j < 2; j++)
			{
				objecttodraw->push_back(c1->getminiO2bar(j));
			}
		}
	}
	for (int i = 0; i < 7; i++)
	{
		UItodraw->push_back(uimanager.getMoveMenuUI(i));
	}
	for (int i = 0; i < 5; i++)
	{
		UItodraw->push_back(uimanager.getArrowHitRate(i));
	}
	for (int i = 0; i < 20; i++)
	{
		UItodraw->push_back(uimanager.getProfilePlayerUI(i));
		UItodraw->push_back(uimanager.getProfileEnemyUI(i));
	}
	for (int i = 0; i < 10; i++)
	{
		UItodraw->push_back(uimanager.getInterFaceUI(i));
	}
	for (int i = 0; i < 8; i++)
	{
		UItodraw->push_back(uimanager.getCutInUI(i));
	}
	for (int i = 0; i < 5; i++)
	{
		UItodraw->push_back(uimanager.getPauseMenuUI(i));
	}
}

void Board::CutoffCamera()
{
	for (int j = 0; j < SizeY; j++)
	{
		for (int i = 0; i < SizeX; i++)
		{
			if (BoardArr[i][j]->GetPosition().x > cam->GetLeft() - 50.0f && BoardArr[i][j]->GetPosition().x < cam->GetRight() + 50.0f && BoardArr[i][j]->GetPosition().y > cam->GetDown() - 50.0f && BoardArr[i][j]->GetPosition().y < cam->GetUp() + 50.0f)
			{
				TempBoardArr[i][j]->setInCam(true);
				EffectBoardArr[i][j]->setInCam(true);
				BoardArr[i][j]->setInCam(true);
				GridObj[i][j]->setInCam(true);
				AtkBoardFXArr[i][j]->setInCam(true);
			}
			else
			{
				TempBoardArr[i][j]->setInCam(false);
				EffectBoardArr[i][j]->setInCam(false);
				BoardArr[i][j]->setInCam(false);
				GridObj[i][j]->setInCam(false);
				AtkBoardFXArr[i][j]->setInCam(false);
			}
		}
	}
}

void Board::BoardUpdate(Camera* maincamera)
{
	uimanager.Update();
	CutoffCamera();
	if (mainstate == BS_ANIM)
	{
		int index = GetUnitindex(SelectedUnitX, SelectedUnitY);
		vector<glm::vec2> currwalkingpath = UnitVector[GetUnitindex(SelectedUnitX, SelectedUnitY)]->getWalkingAnimPath();
		if (curranimwalkindex > currwalkingpath.size() - 2)
		{
			UnitVector[index]->SetAnimation(IDLE);
			int tox = currwalkingpath[currwalkingpath.size() - 1].x;
			int toy = currwalkingpath[currwalkingpath.size() - 1].y;
			UnitVector[index]->setBoardPosition(tox, toy);
			Character* c1 = dynamic_cast<Character*>(UnitVector[index]);
			Enemy* e1 = dynamic_cast<Enemy*>(UnitVector[index]);
			Submarine* s1 = dynamic_cast<Submarine*>(UnitVector[index]);
			if (c1 != nullptr && c1->getDraw() == true)
			{
				c1->setMovecost(c1->getMovecost() - 1);
				TypeArr[tox][toy] = CHARACTER;
				VisionUpdate();
			}
			else if (e1 != nullptr && e1->getDraw() == true)
			{
				TypeArr[tox][toy] = ENEMY;
			}
			else if (s1 != nullptr && s1->getDraw() == true)
			{
				s1->setMovecost(s1->getMovecost() - 1);
				TypeArr[tox][toy] = CHARACTER;
				SubO2Update();
				VisionUpdate();
			}
			mainstate = BS_NORMAL;
			ResetBoard();
			curranimwalkindex = 0;
			maincamera->SetIsFollow(false);
		}
		else
		{
			curranimwalktime += GameEngine::GetInstance()->GetDeltaTime()/GameEngine::GetInstance()->setCurrAnimspeed();
			if (curranimwalktime >= 1.0f)
			{
				curranimwalktime = 1.0f;
			}
			//Flip
			if (currwalkingpath[curranimwalkindex].x < currwalkingpath[curranimwalkindex + 1].x && UnitVector[index]->GetSize().x >= 0)
			{
				UnitVector[index]->SetSize(-UnitVector[index]->GetSize().x, UnitVector[index]->GetSize().y);
			}
			else if (currwalkingpath[curranimwalkindex].x > currwalkingpath[curranimwalkindex + 1].x && UnitVector[index]->GetSize().x <= 0)
			{
				UnitVector[index]->SetSize(-UnitVector[index]->GetSize().x, UnitVector[index]->GetSize().y);
			}
			glm::vec2 currpos = currwalkingpath[curranimwalkindex] * (1 - curranimwalktime) + currwalkingpath[curranimwalkindex + 1] * (curranimwalktime);
			UnitVector[index]->SetPosition(glm::vec3((-GameEngine::GetInstance()->GetWindowWidth() / 2.0f) + currpos.x * 100 + 100 / 2.0f,
					(-GameEngine::GetInstance()->GetWindowHeight() / 2.0f) + currpos.y * 100 + 100 / 2.0f + 15.0f, 0.0f));
			if (curranimwalktime >= 1.0f)
			{
				curranimwalktime = 0.0f;
				curranimwalkindex++;
			}
			maincamera->SetFollowUnit(UnitVector[index]);
			maincamera->SetIsFollow(true);
		}
	}
	else if (mainstate == BS_ENEMY)
	{
		if (uimanager.PassTurnAnimFinish())
		{
			EnemyAction();
		}
	}
	else if (mainstate == BS_ENEMYANIM)
	{
		int index = GetUnitindex(SelectedUnitX, SelectedUnitY);
		vector<glm::vec2> currwalkingpath = UnitVector[GetUnitindex(SelectedUnitX, SelectedUnitY)]->getWalkingAnimPath();
		if (curranimwalkindex > currwalkingpath.size() - 2)
		{
			UnitVector[index]->SetAnimation(WALK);
			int tox = currwalkingpath[currwalkingpath.size() - 1].x;
			int toy = currwalkingpath[currwalkingpath.size() - 1].y;
			UnitVector[index]->setBoardPosition(tox, toy);
			Enemy* e1 = dynamic_cast<Enemy*>(UnitVector[index]);
			if (e1 != nullptr && e1->getDraw() == true)
			{
				TypeArr[tox][toy] = ENEMY;
				glm::vec2 enemypos = e1->getBoardPosition();
				glm::vec2 playerpos = e1->getTargetpos();
				e1->setEnemypath(EnemyAstar(*this, e1->getAtkdis(), enemypos, playerpos));
				if (e1->getEnemypath().size() <= e1->getAtkdis() || e1->getEnemypath().size() == 0)
				{
					bool skip = false;
					maincamera->SetCameraPosition(e1->GetPosition().x, e1->GetPosition().y);
					if (e1->getskillcooldownturn() <= 0)
					{
						SelectedUnitX = e1->getBoardPosition().x;
						SelectedUnitY = e1->getBoardPosition().y;
						UseSelectedUnitSkill();
						FinalBoss* boss = dynamic_cast<FinalBoss*>(e1);
						if (boss != nullptr && boss->getDraw() == true)
						{
							uimanager.setatkanimcount(100);
							skip = true;
						}
					}
					if (skip == false)
					{
						UnitAtk(e1->getBoardPosition().x, e1->getBoardPosition().y, e1->getTargetpos().x, e1->getTargetpos().y);
					}
					else
					{
						mainstate = BS_ENEMYATKANIM;
					}
				}
				else
				{
					mainstate = BS_ENEMY;
				}
				e1->setActive(false);
			}
			curranimwalkindex = 0;
			maincamera->SetIsFollow(false);
		}
		else
		{
			curranimwalktime += GameEngine::GetInstance()->GetDeltaTime() / GameEngine::GetInstance()->setCurrAnimspeed();
			if (curranimwalktime >= 1.0f)
			{
				curranimwalktime = 1.0f;
			}
			//Flip
			if (currwalkingpath[curranimwalkindex].x < currwalkingpath[curranimwalkindex + 1].x && UnitVector[index]->GetSize().x >= 0)
			{
				UnitVector[index]->SetSize(-UnitVector[index]->GetSize().x, UnitVector[index]->GetSize().y);
			}
			else if (currwalkingpath[curranimwalkindex].x > currwalkingpath[curranimwalkindex + 1].x && UnitVector[index]->GetSize().x <= 0)
			{
				UnitVector[index]->SetSize(-UnitVector[index]->GetSize().x, UnitVector[index]->GetSize().y);
			}
			glm::vec2 currpos = currwalkingpath[curranimwalkindex] * (1 - curranimwalktime) + currwalkingpath[curranimwalkindex + 1] * (curranimwalktime);
			UnitVector[GetUnitindex(SelectedUnitX, SelectedUnitY)]->SetPosition(glm::vec3((-GameEngine::GetInstance()->GetWindowWidth() / 2.0f) + currpos.x * 100 + 100 / 2.0f,
				(-GameEngine::GetInstance()->GetWindowHeight() / 2.0f) + currpos.y * 100 + 100 / 2.0f + 15.0f, 0.0f));
			if (curranimwalktime >= 1.0f)
			{
				curranimwalktime = 0.0f;
				curranimwalkindex++;
			}
			maincamera->SetFollowUnit(UnitVector[index]);
			maincamera->SetIsFollow(true);
		}
	}
	else if (mainstate == BS_ATKANIM)
	{
		if (uimanager.AtkAnimFinish())
		{
			StatusUpdate();
			mainstate = BS_NORMAL;
		}
	}
	else if (mainstate == BS_ENEMYATKANIM)
	{
		if (uimanager.AtkAnimFinish())
		{
			StatusUpdate();
			mainstate = BS_ENEMY;
		}
	}
	else if (mainstate == BS_NORMAL)
	{
		if (uimanager.AtkAnimFinish())
		{
			if (this->goaltype == ELIMINATE && enemycount <= 0)
			{
				for (int i = 0; i < UnitVector.size(); i++)
				{
					Character* c = dynamic_cast<Character*>(UnitVector[i]);
					if (c != nullptr)
					{
						if (c->getDraw() == false || c->getisDown())
						{

							GameDataStorage::GetInstance()->killCharacter(c->getName());
						}
					}
				}
				GameData::GetInstance()->gGameStateNext = GameState::GS_WIN;
			}
			if (this->goaltype == TARGET && UnitVector[Targetindex]->getDraw() == false)
			{
				GameData::GetInstance()->gGameStateNext = GameState::GS_WIN;
			}
		}
	}
}

void Board::SelectedUnitOpenSpace(int actiontype)
{
	int index = GetUnitindex(SelectedUnitX, SelectedUnitY);
	int ActionType = actiontype;
	if (ActionType == AT_SKILL)
	{
		Submarine* s1 = dynamic_cast<Submarine*>(UnitVector[index]);
		if (s1 != nullptr && s1->getDraw() == true)
		{
			vector<glm::vec2> skillarea;
			if (s1->getName() == "Jin(Ship)1")
			{
				skillarea = s1->ActiveSkill1(TypeArr, SizeX, SizeY);
			}
			else if (s1->getName() == "Jin(Ship)2")
			{
				skillarea = s1->ActiveSkill2(TypeArr, SizeX, SizeY);
			}
			else if (s1->getName() == "Jin(Ship)3")
			{
				skillarea = s1->ActiveSkill3(TypeArr, SizeX, SizeY);
			}
			for (int i = 0; i < skillarea.size(); i++)
			{
				int pathX = skillarea[i].x;
				int pathY = skillarea[i].y;
				if (pathX >= 0 && pathX < SizeX && pathY >= 0 && pathY < SizeY)
				{
					TempBoardArr[pathX][pathY]->SetColor(0, 1, 0, 0.5f);
					TempArr[pathX][pathY] = 1;
				}
			}
			return;
		}

		vector<glm::vec2> skillarea = UnitVector[GetUnitindex(SelectedUnitX, SelectedUnitY)]->ActiveSkill(TypeArr,SizeX,SizeY);
		for (int i = 0; i < skillarea.size(); i++)
		{
			int pathX = skillarea[i].x;
			int pathY = skillarea[i].y;
			if (pathX >= 0 && pathX < SizeX && pathY >= 0 && pathY < SizeY)
			{
				TempBoardArr[pathX][pathY]->SetColor(0, 1, 0, 0.5f);
				TempArr[pathX][pathY] = 1;
			}
		}
		return;
	}
	Character* c1 = dynamic_cast<Character*>(UnitVector[GetUnitindex(SelectedUnitX, SelectedUnitY)]);
	if (c1 != nullptr && c1->getDraw() == true)
	{
		if (ActionType == AT_MOVE)
		{
			if (c1->getMovecost() > 0)
			{
				OpenSpace(ActionType, c1->getMove(), SelectedUnitX, SelectedUnitY);
			}
			else
			{
				mainstate = BS_NORMAL;
			}
		}
		else if (ActionType == AT_ATK)
		{
			if (c1->getAtkcost() > 0)
			{
				OpenSpace(ActionType, c1->getAtkdis(), SelectedUnitX, SelectedUnitY);
			}
			else
			{
				mainstate = BS_NORMAL;
			}
		}
	}
	Submarine* s1 = dynamic_cast<Submarine*>(UnitVector[GetUnitindex(SelectedUnitX, SelectedUnitY)]);
	if (s1 != nullptr && s1->getDraw() == true)
	{
		if (ActionType == AT_MOVE)
		{
			if (s1->getMovecost() > 0)
			{
				OpenSpace(ActionType, s1->getMove(), SelectedUnitX, SelectedUnitY);
			}
			else
			{
				mainstate = BS_NORMAL;
			}
		}
		else if (ActionType == AT_ATK)
		{
			if (s1->getAtkcost() > 0)
			{
				OpenSpace(ActionType, s1->getAtkdis(), SelectedUnitX, SelectedUnitY);
			}
			else
			{
				mainstate = BS_NORMAL;
			}
		}
	}
}

void Board::UseSelectedUnitSkill()
{
	int index = GetUnitindex(SelectedUnitX, SelectedUnitY);
	if (UnitVector[index]->getName() == "Ash")
	{
		UnitVector[index]->addBuff(BUFF_MOVEDIS, 2, 2);
		UnitVector[index]->addBuff(BUFF_EVA, 20, 2);
		UnitVector[index]->setCD(3);
		uimanager.showSkillCutinUI(UnitVector[index]);
		ResetBoard();
	}
	else if (UnitVector[index]->getName() == "Todd")
	{
		vector<glm::vec2> skillarea = UnitVector[index]->ActiveSkill(TypeArr, SizeX, SizeY);
		for (int i = 0; i < skillarea.size(); i++)
		{
			int posX = skillarea[i].x;
			int posY = skillarea[i].y;
			int index = GetUnitindex(posX, posY);
			if (index == -1)
			{
				continue;
			}
			Character* c1 = dynamic_cast<Character*>(UnitVector[index]);
			Submarine* s1 = dynamic_cast<Submarine*>(UnitVector[index]);
			if ((c1 != nullptr && c1->getDraw() == true) || (s1 != nullptr && s1->getDraw() == true))
			{
				UnitVector[index]->addBuff(BUFF_LOS, 2, 4);
				UnitVector[index]->addBuff(BUFF_DEF, 20, 4);
			}
		}
		UnitVector[index]->setCD(3);
		uimanager.showSkillCutinUI(UnitVector[index]);
		VisionUpdate();
		ResetBoard();
	}
	else if (UnitVector[index]->getName() == "Might")
	{
		vector<glm::vec2> skillarea = UnitVector[index]->ActiveSkill(TypeArr, SizeX, SizeY);
		for (int i = 0; i < skillarea.size(); i++)
		{
			int posX = skillarea[i].x;
			int posY = skillarea[i].y;
			int index = GetUnitindex(posX, posY);
			if (index == -1)
			{
				continue;
			}
			Character* c1 = dynamic_cast<Character*>(UnitVector[index]);
			Submarine* s1 = dynamic_cast<Submarine*>(UnitVector[index]);
			if ((c1 != nullptr && c1->getDraw() == true) || (s1 != nullptr && s1->getDraw() == true))
			{
				UnitVector[index]->addBuff(BUFF_HIT, 5, 3);
			}
		}
		UnitVector[index]->setCD(3);
		uimanager.showSkillCutinUI(UnitVector[index]);
		ResetBoard();
	}
	else if (UnitVector[index]->getName() == "Wong")
	{
		UnitVector[index]->addBuff(BUFF_LOS, 4, 3);
		UnitVector[index]->setCD(3);
		uimanager.showSkillCutinUI(UnitVector[index]);
		VisionUpdate();
		ResetBoard();
	}
	else if (UnitVector[index]->getName() == "Gray")
	{
		vector<glm::vec2> skillarea = UnitVector[index]->ActiveSkill(TypeArr, SizeX, SizeY);
		for (int i = 0; i < skillarea.size(); i++)
		{
			int posX = skillarea[i].x;
			int posY = skillarea[i].y;
			int index = GetUnitindex(posX, posY);
			if (index == -1)
			{
				continue;
			}
			Character* c1 = dynamic_cast<Character*>(UnitVector[index]);
			if (c1 != nullptr && c1->getDraw() == true)
			{
				c1->setOxygen(c1->getOxygen() + 3);
			}
		}
		UnitVector[index]->setCD(3);
		uimanager.showSkillCutinUI(UnitVector[index]);
		ResetBoard();
	}
	else if (UnitVector[index]->getName() == "Kim")
	{
		vector<glm::vec2> skillarea = UnitVector[index]->ActiveSkill(TypeArr, SizeX, SizeY);
		for (int i=0;i<skillarea.size();i++)
		{
			int posX = skillarea[i].x;
			int posY = skillarea[i].y;
			int index = GetUnitindex(posX, posY);
			if (index == -1)
			{
				continue;
			}
			Character* c1 = dynamic_cast<Character*>(UnitVector[index]);
			Submarine* s1 = dynamic_cast<Submarine*>(UnitVector[index]);
			if ((c1 != nullptr && c1->getDraw() == true) || (s1 != nullptr && s1->getDraw() == true))
			{
				UnitVector[index]->setHp(UnitVector[index]->getHp() + 50);
			}
		}
		UnitVector[index]->setCD(3);
		uimanager.showSkillCutinUI(UnitVector[index]);
		ResetBoard();
	}
	else if (UnitVector[index]->getName() == "Jin(Ship)1")
	{
		UnitVector[index]->addBuff(BUFF_MOVEDIS, 3, 1);
		UnitVector[index]->setCD(3);
		uimanager.showSkillCutinUI(UnitVector[index]);
		ResetBoard();
	}
	else if (UnitVector[index]->getName() == "Jin(Ship)2")
	{
		//uimanager.showSkillCutinUI(UnitVector[index]);
	}
	else if (UnitVector[index]->getName() == "Jin(Ship)3")
	{
		Submarine* s1 = dynamic_cast<Submarine*>(UnitVector[index]);
		if (s1 != nullptr && s1->getDraw() == true)
		{
			vector<glm::vec2> skillarea = s1->ActiveSkill3(TypeArr, SizeX, SizeY);
			s1->addBuff(BUFF_ATK, 50, 2);
			for (int i = 0; i < skillarea.size(); i++)
			{
				int posX = skillarea[i].x;
				int posY = skillarea[i].y;
				int index = GetUnitindex(posX, posY);
				if (index == -1)
				{
					continue;
				}
				Character* c1 = dynamic_cast<Character*>(UnitVector[index]);
				if (c1 != nullptr && c1->getDraw() == true)
				{
					c1->addBuff(BUFF_ATK, 50, 2);
				}
			}
			s1->setSubCD(1,3);
			uimanager.showSkillCutinUI(UnitVector[index]);
			ResetBoard();
		}
	}
	Enemy* e1 = dynamic_cast<Enemy*>(UnitVector[index]);
	if (e1 != nullptr && e1->getDraw() == true)
	{
		if (e1->getName() == "Stingray1")
		{
			UnitVector[GetUnitindex(e1->getTargetpos().x,e1->getTargetpos().y)];
		}
		else if (e1->getName() == "Shark1")
		{
			UnitVector[GetUnitindex(e1->getTargetpos().x, e1->getTargetpos().y)];
		}
		else if (e1->getName() == "Normal1")
		{
			UnitVector[GetUnitindex(e1->getTargetpos().x, e1->getTargetpos().y)];
		}
		else if (e1->getName() == "Worm1")
		{
			UnitVector[GetUnitindex(e1->getTargetpos().x, e1->getTargetpos().y)];
		}
		else if (e1->getName() == "Stingray2")
		{
			UnitVector[GetUnitindex(e1->getTargetpos().x, e1->getTargetpos().y)];
		}
		else if (e1->getName() == "Shark2")
		{
			UnitVector[GetUnitindex(e1->getTargetpos().x, e1->getTargetpos().y)];
		}
		else if (e1->getName() == "Normal2")
		{
			UnitVector[GetUnitindex(e1->getTargetpos().x, e1->getTargetpos().y)];
		}
		else if (e1->getName() == "Worm2")
		{
			UnitVector[GetUnitindex(e1->getTargetpos().x, e1->getTargetpos().y)];
		}
		FinalBoss* boss = dynamic_cast<FinalBoss*>(e1);
		if (boss != nullptr && boss->getDraw() == true)
		{
			for (int i=0;i<boss->getHitbox().size();i++)
			{
				if (TypeArr[(int)boss->getHitbox()[i].x][(int)boss->getHitbox()[i].y] == CHARACTER)
				{
					Character* c1 = dynamic_cast<Character*>(UnitVector[GetUnitindex(boss->getHitbox()[i].x, boss->getHitbox()[i].y)]);
					c1->setHp(c1->getHp() - 100);
				}
				int x = boss->getHitbox()[i].x, y = boss->getHitbox()[i].y;
				AtkBoardFXArr[x][y]->setDraw(true);
				AtkBoardFXArr[x][y]->SetAnimationLoop(0, 0, 7, 150);
			}
			boss->setskillcooldownturn(2);
		}
	}
}

void Board::ClickHandle(int type, int x, int y,int& state ,Camera* camera)
{
	if (type == MOUSEMOVE)
	{
		ClickMoveHandle(x, y,state, camera);
	}
	else if (type == MOUSELEFT)
	{
		ClickLeftHandle(x,y,state,camera);
	}
	else if (type == MOUSERIGHT)
	{
		ClickRightHandle(x,y, state, camera);
	}
}

void Board::ClickMoveHandle(int x, int y,int& state, Camera* camera)
{
	if (state != LS_SELECTED)
	{
		Cursor->SetPosition(BoardArr[x][y]->GetPosition());
		if(mainstate == BS_NORMAL)
		{ 

		}
		else if(mainstate == BS_MOVE)
		{
			for (int j = 0; j < SizeY; j++)
			{
				for (int i = 0; i < SizeX; i++)
				{
					if (TempArr[i][j] == 1)
					{
						TempBoardArr[i][j]->SetColor(0, 0, 1, 0.5f);
					}
					else
					{
						if (VisionArr[i][j] == VS_FAR)
						{
							TempBoardArr[i][j]->SetColor(0.0, 0.0, 0.0, 1.0);
						}
						else if (VisionArr[i][j] == VS_MIDDLE)
						{
							TempBoardArr[i][j]->SetColor(0.0, 0.0, 0.0, 0.5);
						}
						else if (VisionArr[i][j] == VS_CLOSE)
						{
							TempBoardArr[i][j]->SetColor(0.0, 0.0, 0.0, 0.0);
						}
					}
				}
			}
			if (TempArr[x][y] == 1)
			{
				vector<glm::vec2> path;
				path = UnitOpenSpaceAstar(*this, AT_ATK, GetSelectedUnit()->getAtkdis(), glm::vec2(x, y));
				for (int i = 0; i < path.size(); i++)
				{
					int pathX = path[i].x;
					int pathY = path[i].y;
					if (SpecialBlockArr[pathX][pathY].type == SB_GATE)
					{
						continue;
					}
					if (TempArr[pathX][pathY] != 1)
					{
						TempBoardArr[pathX][pathY]->SetColor(1, 0, 0, 0.5f);
					}
				}
			}
		}
		else if (mainstate == BS_ATTACK)
		{
			if (prevmouseonblock != glm::vec2(x,y))
			{
				if (TempArr[x][y] == 1 && TypeArr[x][y] == ENEMY)
				{
					Enemy* e1 = dynamic_cast<Enemy*>(UnitVector[GetUnitindex(x, y)]);
					if (e1->getinVision())
					{
						int hit = GetSelectedUnit()->getBuffEffect(BUFF_HIT);
						int damage = GetSelectedUnit()->getAtk() - e1->getDef();
						if (damage > e1->getHp())
						{
							damage = e1->getHp();
						}
						uimanager.showProfileUI(UnitVector[GetUnitindex(x, y)], hit, damage);
						prevmouseonblock = glm::vec2(x, y);
					}
				}
				else
				{
					prevmouseonblock = glm::vec2(x, y);
				}
			}
		}
	}
}

void Board::ClickLeftHandle(int x, int y,int& state ,Camera* camera)
{
	if (mainstate == BS_NORMAL)
	{
		if (TypeArr[x][y] == CHARACTER)
		{
			Character* c1 = dynamic_cast<Character*>(UnitVector[GetUnitindex(x, y)]);
			if (c1 != nullptr && c1->getisDown())
			{
				return;
			}
			SelectedUnitX = x;
			SelectedUnitY = y;
			state = LS_SELECTED;
			camera->SetCameraPosition(UnitVector[GetUnitindex(x, y)]->GetPosition().x, UnitVector[GetUnitindex(x, y)]->GetPosition().y);
			camera->SetEnable(false);
			camera->CameraAdjust();
			uimanager.showMoveMenuUI(UnitVector[GetUnitindex(x, y)],camera);
			uimanager.showProfileUI(UnitVector[GetUnitindex(x, y)]);
			for (int j = 0; j < SizeY; j++)
			{
				for (int i = 0; i < SizeX; i++)
				{
					GridObj[i][j]->SetColor(0.0, 0.0, 0.0, 0.3);
				}
			}
		}
		else if(TypeArr[x][y] == ENEMY && UnitVector[GetUnitindex(x, y)]->getDraw() == true)
		{
			Enemy* e1 = dynamic_cast<Enemy*>(UnitVector[GetUnitindex(x, y)]);
			if (e1->getinVision())
			{
				uimanager.showProfileUI(UnitVector[GetUnitindex(x, y)]);
			}
		}
		else
		{
			ResetBoard();
		}
	}
	else if(mainstate == BS_MOVE && TempArr[x][y] == 1)
	{
		UnitMove(SelectedUnitX, SelectedUnitY,x,y);
	}
	else if (mainstate == BS_ATTACK && TempArr[x][y] == 1)
	{
		UnitAtk(SelectedUnitX, SelectedUnitY, x, y);
	}
	else if (mainstate == BS_SKILLSINGLE && TempArr[x][y] == 1)
	{
		int index = GetUnitindex(SelectedUnitX, SelectedUnitY);
		Submarine* s1 = dynamic_cast<Submarine*>(UnitVector[index]);
		if (s1 != nullptr && s1->getDraw() == true)
		{
			if (TypeArr[x][y] == CHARACTER)
			{
				UnitVector[GetUnitindex(x, y)]->setHp(UnitVector[GetUnitindex(x, y)]->getHp() + 100);
				s1->setSubCD(0, 3);
				uimanager.showSkillCutinUI(s1);
				ResetBoard();
			}
		}
	}
	else if (mainstate == BS_ANIM || mainstate == BS_ENEMYANIM || mainstate == BS_ENEMY || mainstate == BS_ATKANIM)
	{

	}
	else
	{
		ResetBoard();
	}
}

void Board::ClickRightHandle(int x, int y, int& state, Camera* camera)
{
	if (mainstate != BS_ANIM && mainstate != BS_ENEMYANIM && mainstate != BS_ENEMY && mainstate != BS_ATKANIM)
	{
		if (ResetBoard())
		{
			state = LS_NORMAL;
		}
	}
}

void Board::StartPlayerTurn()
{
	uimanager.showPhaseChange(CHARACTER);
	turncount++;
	for (int i = 0; i < UnitVector.size(); i++)
	{
		UnitVector[i]->SetAnimation(IDLE);

		Character* c1 = dynamic_cast<Character*>(UnitVector[i]);
		if (c1 != nullptr && c1->getDraw() == true)
		{
			c1->setAtkcost(1);
			c1->setMovecost(1);
			if (c1->getisDown())
			{
				glm::vec2 currpos = c1->getBoardPosition();
				bool getrevive = false;
				for (int j = -1; j <= 1; j++)
				{
					for (int i = -1; i <= 1; i++)
					{
						int index = GetUnitindex(currpos.x + i, currpos.y + j);
						if (index != -1)
						{
							Character* c2 = dynamic_cast<Character*>(UnitVector[index]);
							Submarine* s2 = dynamic_cast<Submarine*>(UnitVector[index]);
							if ((c2 != nullptr && c2->getDraw() == true) || (s2 != nullptr && s2->getDraw() == true))
							{
								if ((c2 != c1) && c1->getDownTurn() != 3)
								{
									c1->setisDown(false);
									c1->setHp(c1->getMaxHp() * 0.2);
									c1->setOxygen(3);
									getrevive = true;
								}
							}

						}
					}
				}
				if (getrevive == false)
				{
					if (c1->getDownTurn() <= 0)
					{
						c1->setDraw(false);
						TypeArr[(int)c1->getBoardPosition().x][(int)c1->getBoardPosition().y] = FLOOR;
					}
					c1->SetAnimationLoop(2, 1, 1, 100);
				}
			}
		}
		Submarine* s1 = dynamic_cast<Submarine*>(UnitVector[i]);
		if (s1 != nullptr && s1->getDraw() == true)
		{
			s1->setAtkcost(1);
			s1->setMovecost(1);
			cam->SetCameraPosition(s1->GetPosition().x, s1->GetPosition().y);
		}
		Enemy* e1 = dynamic_cast<Enemy*>(UnitVector[i]);
		if (e1 != nullptr && e1->getDraw() == true)
		{
			UnitVector[i]->SetAnimation(WALK);
			e1->setActive(true);
		}
		UnitVector[i]->PassTurn();
	}

	VisionUpdate();
	ResetBoard();
	StatusUpdate();
	mainstate = BS_NORMAL;
	//cout << "Startplayerturn " << turncount << endl;
}
void Board::StartEnemyTurn()
{
	enemywait = 0;
	triedtowalk = false;
	uimanager.closeMoveMenuUI();
	uimanager.closeProfileUI();
	uimanager.showPhaseChange(ENEMY);
	for (int i = 0; i < UnitVector.size(); i++)
	{
		Character* c1 = dynamic_cast<Character*>(UnitVector[i]);
		if (c1 != nullptr && c1->getDraw() == true)
		{
			c1->setOxygen(c1->getOxygen() - 2);
		}
		Enemy* e1 = dynamic_cast<Enemy*>(UnitVector[i]);
		if (e1 != nullptr && e1->getDraw() == true)
		{
			e1->setskillcooldownturn(e1->getskillcooldownturn()-1);
		}
	}
	for (int i = 0; i < UnitVector.size(); i++)
	{
		Submarine* s1 = dynamic_cast<Submarine*>(UnitVector[i]);
		if (s1 != nullptr && s1->getDraw() == true)
		{
			if (GoalArr[(int)s1->getBoardPosition().x][(int)s1->getBoardPosition().y] == 1)
			{
				GameData::GetInstance()->gGameStateNext = GameState::GS_WIN;
			}
			vector<glm::vec2> O2area = s1->HealOxygen(1);
			for (int j = 0; j < O2area.size(); j++)
			{
				int index = GetUnitindex(O2area[j].x, O2area[j].y);
				if (index != -1)
				{
					Character* c1 = dynamic_cast<Character*>(UnitVector[index]);
					if (c1 != nullptr && c1->getDraw() == true)
					{
						int totaloxygen = c1->getOxygen() + 5;
						if (totaloxygen >= 10)
						{
							totaloxygen = 10;
						}
						c1->setOxygen(totaloxygen);
					}
				}
			}
		}
	}
	for (int i = 0; i < UnitVector.size(); i++)
	{
		Character* c1 = dynamic_cast<Character*>(UnitVector[i]);
		if (c1 != nullptr && c1->getDraw() == true)
		{
			int posX = c1->getBoardPosition().x;
			int posY = c1->getBoardPosition().y;
			if (SpecialBlockArr[posX][posY].type != SB_NORMAL)
			{
				if (SpecialBlockArr[posX][posY].type == SB_O2)
				{
					int totaloxygen = c1->getOxygen() + SpecialBlockArr[(int)c1->getBoardPosition().x][(int)c1->getBoardPosition().y].effect;
					if (totaloxygen >= 10)
					{
						totaloxygen = 10;
					}
					c1->setOxygen(totaloxygen);
				}
				else if (SpecialBlockArr[posX][posY].type == SB_ATK)
				{
					c1->addBuff(BUFF_ATK, SpecialBlockArr[posX][posY].effect, SpecialBlockArr[posX][posY].duration);
				}
				else if (SpecialBlockArr[posX][posY].type == SB_HEAL)
				{
					c1->setHp(c1->getHp() + SpecialBlockArr[posX][posY].effect);
				}
				else if (SpecialBlockArr[posX][posY].type == SB_LOS)
				{
					c1->addBuff(BUFF_LOS, SpecialBlockArr[posX][posY].effect, SpecialBlockArr[posX][posY].duration);
				}
				else if (SpecialBlockArr[posX][posY].type == SB_HIT)
				{
					c1->addBuff(BUFF_HIT, SpecialBlockArr[posX][posY].effect, SpecialBlockArr[posX][posY].duration);
				}
			}
		}
	}

	for (int i = 0; i < UnitVector.size(); i++)
	{
		Character* c1 = dynamic_cast<Character*>(UnitVector[i]);
		if (c1 != nullptr && c1->getDraw() == true)
		{
			if (c1->getOxygen() <= 0) {
				c1->setHp(c1->getHp() - 50);
			}
		}
	}

	//cout << "enemyturn " << turncount << endl;
	mainstate = BS_ENEMY;
}
void Board::EnemyAction()
{
	for (int i = 0; i < UnitVector.size(); i++)
	{
		Enemy* e1 = dynamic_cast<Enemy*>(UnitVector[i]);
		if (e1 != nullptr && e1->getDraw() == true)
		{
			if (e1->wait == true)
			{
				continue;
			}
			if (e1->getActive() == false) 
			{ 
				continue;
			}
			if (e1->getFoundPlayer() == false)
			{
				continue;
			}

			EnemyAstar2(*this, e1);

			if (e1->getEnemypath().size() > 0)
			{
				int index;
				int toX, toY;
				if (e1->getEnemypath()[0] == glm::vec2(-1, -1))
				{
					e1->wait = true;
					enemywait++;
					return;
				}
				if (e1->getEnemypath().size() > e1->getMove() + e1->getAtkdis()) // so far
				{
					toX = e1->getEnemypath()[e1->getMove()].x;
					toY = e1->getEnemypath()[e1->getMove()].y;
					index = e1->getMove();
				}
				else if (e1->getEnemypath().size() <= e1->getMove() + e1->getAtkdis()) // in range
				{
					toX = e1->getEnemypath()[e1->getEnemypath().size() - e1->getAtkdis()].x;
					toY = e1->getEnemypath()[e1->getEnemypath().size() - e1->getAtkdis()].y;
					index = e1->getEnemypath().size() - e1->getAtkdis();
				}
				TypeArr[(int)e1->getBoardPosition().x][(int)e1->getBoardPosition().y] = FLOOR;
				while (TypeArr[toX][toY] == ENEMY || TypeArr[toX][toY] == CHARACTER || SpecialBlockArr[toX][toY].type == SB_GATE)
				{
					toX = e1->getEnemypath()[index].x;
					toY = e1->getEnemypath()[index].y;
					index--;
				}
				SelectedUnitX = e1->getBoardPosition().x;
				SelectedUnitY = e1->getBoardPosition().y;
				UnitMove(e1->getBoardPosition().x, e1->getBoardPosition().y, toX, toY);
				return;
			}
			else if (e1->getEnemypath().size() == 0)
			{
				SelectedUnitX = e1->getBoardPosition().x;
				SelectedUnitY = e1->getBoardPosition().y;
				UnitMove(e1->getBoardPosition().x, e1->getBoardPosition().y, e1->getBoardPosition().x, e1->getBoardPosition().y);
				return;
			}
		}
	}
	int waitcount = 0;
	for (int i = 0; i < UnitVector.size(); i++)
	{
		Enemy* e1 = dynamic_cast<Enemy*>(UnitVector[i]);
		if (e1 != nullptr && e1->getDraw() == true)
		{
			if (e1->wait == true)
			{
				e1->wait = false;
				waitcount++;
			}
		}
	}
	if (enemywait >= waitcount)
	{
		if (triedtowalk == false)
		{
			triedtowalk = true;
			return;
		}
		StartPlayerTurn();
	}
}
void Board::PassTurn()
{
	if (mainstate == BS_ENEMY)
	{
		mainstate = BS_NORMAL;
		StartPlayerTurn();
	}
	else
	{
		mainstate = BS_ENEMY;
		StartEnemyTurn();
	}
}

void Board::OpenSpace(int type, int sign, int x, int y)
{
	vector<glm::vec2> path;
	path = UnitOpenSpaceAstar(*this, type, sign, glm::vec2(x, y));
	bool isSub = false;
	Submarine* s1 = dynamic_cast<Submarine*>(UnitVector[GetUnitindex(x, y)]);
	if (s1 != nullptr && s1->getDraw() == true)
	{
		isSub = true;
	}
	for (int i = 0 ; i < path.size() ; i++)
	{
		int pathX = path[i].x;
		int pathY = path[i].y;
		if (isSub == true && TypeArr[pathX][pathY] == UNIT)
		{
			continue;
		}
		if (SpecialBlockArr[pathX][pathY].type == SB_GATE)
		{
			continue;
		}
		if (type == AT_MOVE)
		{
			if (pathX == x && pathY == y)
			{
				continue;
			}
			TempBoardArr[pathX][pathY]->SetColor(0, 0, 1, 0.5f);
			TempArr[pathX][pathY] = 1;
		}
		else if (type == AT_ATK)
		{
			if (TypeArr[pathX][pathY] == CHARACTER)
			{
				continue;
			}
			TempBoardArr[pathX][pathY]->SetColor(1, 0, 0,0.5f);
			TempArr[pathX][pathY] = 1;
		}
		
	}
}
void Board::UnitMove(int fromx, int fromy, int tox, int toy)
{
	int index = GetUnitindex(fromx, fromy);

	TypeArr[fromx][fromy] = FLOOR;
	Enemy* e1 = dynamic_cast<Enemy*>(UnitVector[index]);
	if (e1 != nullptr && e1->getDraw() == true && VisionArr[tox][toy] != VS_CLOSE)
	{
		e1->SetPosition(glm::vec3((-GameEngine::GetInstance()->GetWindowWidth() / 2.0f) + tox * 100 + 100 / 2.0f,
			(-GameEngine::GetInstance()->GetWindowHeight() / 2.0f) + toy * 100 + 100 / 2.0f, 0.0f));

		e1->setBoardPosition(tox, toy);
		TypeArr[tox][toy] = ENEMY;
		UnitVector[index]->SetAnimation(WALK);
		UnitVector[index]->setBoardPosition(tox, toy);
		Enemy* e1 = dynamic_cast<Enemy*>(UnitVector[index]);
		if (e1 != nullptr && e1->getDraw() == true)
		{
			TypeArr[tox][toy] = ENEMY;
			if (e1->getEnemypath().size() <= e1->getAtkdis() || e1->getEnemypath().size() == 0)
			{
				bool skip = false;
				if (e1->getskillcooldownturn() <= 0)
				{
					SelectedUnitX = e1->getBoardPosition().x;
					SelectedUnitY = e1->getBoardPosition().y;
					UseSelectedUnitSkill();
					FinalBoss* boss = dynamic_cast<FinalBoss*>(e1);
					if (boss != nullptr && boss->getDraw() == true)
					{
						uimanager.setatkanimcount(100);
						skip = true;
					}
				}
				if (skip == false)
				{
					UnitAtk(e1->getBoardPosition().x, e1->getBoardPosition().y, e1->getTargetpos().x, e1->getTargetpos().y);
				}
				else
				{
					mainstate = BS_ENEMYATKANIM;
				}
			}
			e1->setActive(false);
		}
		curranimwalkindex = 0;
		return;
	}
	else if (e1 != nullptr && e1->getDraw() == true && VisionArr[tox][toy] == VS_CLOSE)
	{
		e1->setinVision(true);
		e1->getminiHpbar(0)->setDraw(true);
		e1->getminiHpbar(1)->setDraw(true);
	}
	if (mainstate == BS_ENEMY)
	{
		mainstate = BS_ENEMYANIM;
	}
	else
	{
		mainstate = BS_ANIM;
	}
	vector<glm::vec2> walkingpath = Astar(*this,AT_MOVE,glm::vec2(fromx,fromy),glm::vec2(tox,toy));
	if (walkingpath.size() > 1) // walking anim
	{
		UnitVector[index]->SetAnimation(WALK);
		UnitVector[index]->setWalkingAnimPath(walkingpath);
	}
	else //not moving
	{
		UnitVector[index]->SetAnimation(IDLE);
		int index = GetUnitindex(SelectedUnitX, SelectedUnitY);
		int tox = UnitVector[index]->getBoardPosition().x;
		int toy = UnitVector[index]->getBoardPosition().y;
		UnitVector[index]->setBoardPosition(tox, toy);
		Character* c1 = dynamic_cast<Character*>(UnitVector[index]);
		Enemy* e1 = dynamic_cast<Enemy*>(UnitVector[index]);
		Submarine* s1 = dynamic_cast<Submarine*>(UnitVector[index]);
		if (c1 != nullptr && c1->getDraw() == true)
		{
			c1->setMovecost(c1->getMovecost() - 1);
			TypeArr[tox][toy] = CHARACTER;
			VisionUpdate();
		}
		else if (e1 != nullptr && e1->getDraw() == true)
		{
			UnitVector[index]->SetAnimation(WALK);
			TypeArr[tox][toy] = ENEMY;
			if (e1->getEnemypath().size() <= e1->getAtkdis() || e1->getEnemypath().size() == 0)
			{
				bool skip = false;
				if (e1->getskillcooldownturn() <= 0)
				{
					SelectedUnitX = e1->getBoardPosition().x;
					SelectedUnitY = e1->getBoardPosition().y;
					UseSelectedUnitSkill();
					FinalBoss* boss = dynamic_cast<FinalBoss*>(e1);
					if (boss != nullptr && boss->getDraw() == true)
					{
						uimanager.setatkanimcount(100);
						skip = true;
					}
				}
				if (skip == false)
				{
					UnitAtk(e1->getBoardPosition().x, e1->getBoardPosition().y, e1->getTargetpos().x, e1->getTargetpos().y);
				}
				else
				{
					mainstate = BS_ENEMYATKANIM;
				}
			}
			else
			{
				mainstate = BS_ENEMY;
			}
			e1->setActive(false);
		}
		else if (s1 != nullptr && s1->getDraw() == true)
		{
			s1->setMovecost(s1->getMovecost() - 1);
			TypeArr[tox][toy] = CHARACTER;
			VisionUpdate();
		}
		if (mainstate == BS_ENEMYANIM || mainstate == BS_ENEMYATKANIM || mainstate == BS_ENEMY)
		{
			
		}
		else
		{
			mainstate = BS_NORMAL;
			ResetBoard();
		}
		curranimwalkindex = 0;
	}
	
	for (int i = 0; i < SizeX; i++)
	{
		for (int j = 0; j < SizeY; j++)
		{
			if (VisionArr[i][j] == VS_FAR)
			{
				TempBoardArr[i][j]->SetColor(0.0, 0.0, 0.0, 1.0);
			}
			else if (VisionArr[i][j] == VS_MIDDLE)
			{
				TempBoardArr[i][j]->SetColor(0.0, 0.0, 0.0, 0.5);
			}
			else if (VisionArr[i][j] == VS_CLOSE)
			{
				TempBoardArr[i][j]->SetColor(0.0, 0.0, 0.0, 0.0);
			}
			TempArr[i][j] = 0;
		}
	}
}
void Board::UnitAtk(int fromx, int fromy, int tox, int toy)
{
	int hitter = GetUnitindex(fromx, fromy);
	int gothit = GetUnitindex(tox, toy);

	if (TypeArr[tox][toy] == CHARACTER || TypeArr[tox][toy] == ENEMY)
	{
		if (mainstate == BS_ENEMY || mainstate == BS_ENEMYANIM)
		{
			mainstate = BS_ENEMYATKANIM;
		}
		else
		{
			mainstate = BS_ATKANIM;
		}
		cam->SetCameraPosition(UnitVector[hitter]->GetPosition().x, UnitVector[hitter]->GetPosition().y);
		int x = rand() % 100 + 1; //1 - 100
		if (UnitVector[gothit]->getEva() - UnitVector[hitter]->getBuffEffect(BUFF_HIT) <= x) // HIT
		{
			int damage = UnitVector[hitter]->getAtk() - UnitVector[gothit]->getDef();
			if (damage < 0) { damage = 0; }
			UnitVector[gothit]->setHp(UnitVector[gothit]->getHp() - damage);
			uimanager.showCutinUI(true, UnitVector[hitter], UnitVector[gothit]);
		}
		else // MISS
		{
			uimanager.showCutinUI(false, UnitVector[hitter], UnitVector[gothit]);
		} 

		Character* c1 = dynamic_cast<Character*>(UnitVector[hitter]);
		if (c1 != nullptr && c1->getDraw() == true)
		{
			c1->setAtkcost(c1->getAtkcost() - 1);
			ResetBoard();
		}
		Submarine* s1 = dynamic_cast<Submarine*>(UnitVector[hitter]);
		if (s1 != nullptr && s1->getDraw() == true)
		{
			s1->setAtkcost(s1->getAtkcost() - 1);
			ResetBoard();
		}
	}
}
bool Board::ResetBoard()
{
	if (mainstate == BS_ANIM)
	{
		return false;
	}
	uimanager.closeMoveMenuUI();
	uimanager.closeProfileUI();
	StatusUpdate();
	
	for (int j = 0; j < SizeY; j++)
	{
		for (int i = 0; i < SizeX; i++)
		{
			GridObj[i][j]->SetColor(0.0, 0.0, 0.0, 0.1);
			if (SpecialBlockArr[i][j].type == SB_GATE)
			{
				EffectBoardArr[i][j]->setDraw(true);
				TypeArr[i][j] = MOUNT;
			}
			else if(SpecialBlockArr[i][j].type == SB_BUTTON)
			{
				if (EffectBoardArr[i][j]->GetFxtype() == SB_GATE)
				{
					EffectBoardArr[i][j]->setPath("../Resource/Texture/mapsprite/Button1.png", 1, 1);
				}
			}
		}
	}

	for (int k = 0; k < UnitVector.size(); k++)
	{
		Enemy* e1 = dynamic_cast<Enemy*>(UnitVector[k]);
		if (e1 != nullptr && e1->getDraw() == true)
		{
			int posx = e1->getBoardPosition().x;
			int posy = e1->getBoardPosition().y;
			int fardistance = EnemyFindclosestPlayer(UnitVector,e1);
			if (fardistance <= 4 && e1->getFoundPlayer() == false)
			{
				e1->setFoundPlayer(true);
			}
			if (VisionArr[posx][posy] == VS_CLOSE)
			{
				e1->setinVision(true);
				e1->getminiHpbar(0)->setDraw(true);
				e1->getminiHpbar(1)->setDraw(true);
			}
			else
			{
				e1->setinVision(false);
				e1->getminiHpbar(0)->setDraw(false);
				e1->getminiHpbar(1)->setDraw(false);
			}
		}
		Character* c1 = dynamic_cast<Character*>(UnitVector[k]);
		Submarine* s1 = dynamic_cast<Submarine*>(UnitVector[k]);
		bool isPlayer = false;
		if ((c1 != nullptr && c1->getDraw() == true) || (s1 != nullptr && s1->getDraw() == true)){ isPlayer = true; }
		if (isPlayer)
		{
			int posx = UnitVector[k]->getBoardPosition().x;
			int posy = UnitVector[k]->getBoardPosition().y;
			vector<int> buttonhit;
			bool change = false;
			if (SpecialBlockArr[posx][posy].type == SB_KEY)
			{
				EffectBoardArr[posx][posy]->SetFxtype(SB_NORMAL);
				EffectBoardArr[posx][posy]->setDraw(false);
				for (int j = 0; j < SizeY; j++)
				{
					for (int i = 0; i < SizeX; i++)
					{
						if (SpecialBlockArr[i][j].type == SB_DOOR && SpecialBlockArr[i][j].effect == SpecialBlockArr[posx][posy].effect)
						{
							TypeArr[i][j] = FLOOR;
							EffectBoardArr[i][j]->setPath("../Resource/Texture/mapsprite/Door3.png", 1, 1);
							SpecialBlockArr[i][j].type = SB_NORMAL;
						}
					}
				}
				SpecialBlockArr[posx][posy].type == SB_NORMAL;
				VisionUpdate();
			}
			else if (SpecialBlockArr[posx][posy].type == SB_BUTTON)
			{
				EffectBoardArr[posx][posy]->setPath("../Resource/Texture/mapsprite/Button2.png", 1, 1);
				EffectBoardArr[posx][posy]->SetFxtype(SB_GATE);
				for (int j = 0; j < SizeY; j++)
				{
					for (int i = 0; i < SizeX; i++)
					{
						if (SpecialBlockArr[i][j].type == SB_GATE && SpecialBlockArr[i][j].effect == SpecialBlockArr[posx][posy].effect)
						{
							EffectBoardArr[i][j]->setDraw(false);
							TypeArr[i][j] = FLOOR;
						}
					}
				}
				VisionUpdate();
			}
		}
	}

	for (int i = 0; i < SizeX; i++)
	{
		for (int j = 0; j < SizeY; j++)
		{
			if (VisionArr[i][j] == VS_FAR)
			{
				TempBoardArr[i][j]->SetColor(0.0, 0.0, 0.0, 1.0);
			}
			else if (VisionArr[i][j] == VS_MIDDLE)
			{
				TempBoardArr[i][j]->SetColor(0.0, 0.0, 0.0, 0.5);
			}
			else if (VisionArr[i][j] == VS_CLOSE)
			{
				TempBoardArr[i][j]->SetColor(0.0, 0.0, 0.0, 0.0);
			}
			TempArr[i][j] = 0;
		}
	}
	ActionCheck();
	SelectedUnitX = -1;
	SelectedUnitY = -1;
	mainstate = BS_NORMAL;

	return true;
}

void Board::StatusUpdate()
{
	for (int i = 0; i < UnitVector.size(); i++)
	{
		if (UnitVector[i]->getHp() <= 0)
		{
			Character* c = dynamic_cast<Character*>(UnitVector[i]);
			if (c != nullptr && c->getDraw() == true)
			{
				if (c->getLifeLeft() > 0 && c->getisDown() == false)
				{
					c->setLifeLeft(c->getLifeLeft() - 1);
					c->setisDown(true);
					c->setDownTurn(3);
				}
				else if (c->getLifeLeft() <= 0 && c->getisDown() == false)
				{
					TypeArr[(int)c->getBoardPosition().x][(int)c->getBoardPosition().y] = FLOOR;
					c->setDraw(false);
				}
			}
			Enemy* e = dynamic_cast<Enemy*>(UnitVector[i]);
			if (e != nullptr && e->getDraw() == true)
			{
				enemycount--;
				UnitVector[i]->setDraw(false);
				TypeArr[(int)UnitVector[i]->getBoardPosition().x][(int)UnitVector[i]->getBoardPosition().y] = FLOOR;
			}
			Submarine* s = dynamic_cast<Submarine*>(UnitVector[i]);
			if (s != nullptr && s->getDraw() == true)
			{
				GameData::GetInstance()->gGameStateNext = GameState::GS_LOSE;
			}
		}
		Character* c1 = dynamic_cast<Character*>(UnitVector[i]);
		if (c1 != nullptr && c1->getDraw() == true)
		{
			if (c1->getMovecost() <= 0)
			{
				c1->SetColorOffset(-0.15f,-0.15f,-0.15f);
			}
			else
			{
				c1->SetColorOffset(0.0f, 0.0f, 0.0f);
			}
			if (c1->getisDown())
			{
				c1->SetAnimationLoop(2, 1, 1, 100);
				c1->SetColorOffset(-0.2, -0.2, -0.2);
			}
		}
		Submarine* s1 = dynamic_cast<Submarine*>(UnitVector[i]);
		if (s1 != nullptr && s1->getDraw() == true)
		{
			if (s1->getMovecost() <= 0)
			{
				s1->SetColorOffset(-0.15f, -0.15f, -0.15f);
			}
			else
			{
				s1->SetColorOffset(0.0f, 0.0f, 0.0f);
			}
		}
	}
	uimanager.setEnemyNumber(enemycount);
}
void Board::VisionUpdate()
{
	for (int i = 0; i < SizeX; i++)
	{
		for (int j = 0; j < SizeY; j++)
		{
			if (VisionArr[i][j] == VS_MIDDLE || VisionArr[i][j] == VS_CLOSE)
			{
				VisionArr[i][j] = VS_MIDDLE;
			}
			else
			{
				VisionArr[i][j] = VS_FAR;
			}
			if (GameDataStorage::GetInstance()->getOpenvision())
			{
				VisionArr[i][j] = VS_CLOSE;
			}
		}
	}

	vector<glm::vec2> visionin;
	vector<glm::vec2> visionout;

	for (int i = 0; i < UnitVector.size(); i++)
	{
		Character* c1 = dynamic_cast<Character*>(UnitVector[i]);
		if (c1 != nullptr && c1->getDraw() == true && c1->getisDown() == false)
		{
			vector<glm::vec2> vision;
			vision = UnitOpenSpaceAstar(*this, AT_ATK, c1->getLOSout(), c1->getBoardPosition());
			for (int i =0;i<vision.size();i++)
			{
				int distance = abs(vision[i].x - c1->getBoardPosition().x) + abs(vision[i].y - c1->getBoardPosition().y);
				if (distance <= c1->getLOSin())
				{
					visionin.push_back(vision[i]);
				}
				else
				{
					visionout.push_back(vision[i]);
				}
			}
		}
		Submarine* s1 = dynamic_cast<Submarine*>(UnitVector[i]);
		if (s1 != nullptr && s1->getDraw() == true)
		{
			vector<glm::vec2> vision;
			vision = UnitOpenSpaceAstar(*this, AT_ATK, s1->getLOSout(), s1->getBoardPosition());
			for (int i = 0; i < vision.size(); i++)
			{
				float distance = abs(vision[i].x - s1->getBoardPosition().x) + abs(vision[i].y - s1->getBoardPosition().y);
				if (distance <= s1->getLOSin())
				{
					visionin.push_back(vision[i]);
				}
				else
				{
					visionout.push_back(vision[i]);
				}
			}
		}
	}
	glm::vec2 next[4] = { glm::vec2(1,0), glm::vec2(-1,0) ,glm::vec2(0,1) ,glm::vec2(0,-1) };
	int x, y;

	for (int i=0;i<visionout.size();i++) 
	{
		x = visionout[i].x;
		y = visionout[i].y;
		if (VisionArr[x][y] == VS_FAR || VisionArr[x][y] == VS_MIDDLE)
		{
			VisionArr[x][y] = VS_MIDDLE;
		}
		for (int j = 0; j < 4; j++)
		{
			x = visionout[i].x + next[j].x;
			y = visionout[i].y + next[j].y;
			if (x < SizeX && x >= 0 && y < SizeY && y >= 0)
			{
				if (TypeArr[x][y] == MOUNT)
				{
					VisionArr[x][y] = VS_MIDDLE;
				}
			}
		}
	}

	for (int i = 0; i < visionin.size(); i++)
	{
		x = visionin[i].x;
		y = visionin[i].y;
		VisionArr[x][y] = VS_CLOSE;
		for (int j = 0; j < 4; j++)
		{
			x = visionin[i].x + next[j].x;
			y = visionin[i].y + next[j].y;
			if (x < SizeX && x >= 0 && y < SizeY && y >= 0)
			{
				if (TypeArr[x][y] == MOUNT)
				{
					VisionArr[x][y] = VS_CLOSE;
				}
			}
		}
	}
}

void Board::SubO2Update()
{
	for (int j = 0; j < SizeY; j++)
	{
		for (int i = 0; i < SizeX; i++)
		{
			if (EffectBoardArr[i][j]->GetFxtype() == FXB_O2)
			{
				EffectBoardArr[i][j]->SetColor(0, 0, 0, 0);
				EffectBoardArr[i][j]->SetFxtype(FXB_NORMAL);
				EffectBoardArr[i][j]->setDraw(false);
			}
		}
	}
	for (int i = 0; i < UnitVector.size(); i++)
	{
		Submarine* s1 = dynamic_cast<Submarine*>(UnitVector[i]);
		if (s1 != nullptr && s1->getDraw() == true)
		{
			if (GoalArr[(int)s1->getBoardPosition().x][(int)s1->getBoardPosition().y] == 1)
			{
				GameData::GetInstance()->gGameStateNext = GameState::GS_WIN;
			}
			vector<glm::vec2> O2area = s1->HealOxygen(1);
			for (int j = 0; j < O2area.size(); j++)
			{
				int posx = O2area[j].x, posy = O2area[j].y;
				int index = GetUnitindex(O2area[j].x, O2area[j].y);
				if (posx < 0 || posy < 0 || posx >= SizeX || posx >= SizeY)
				{
					continue;
				}
				if (EffectBoardArr[posx][posy]->GetFxtype() == FXB_NORMAL)
				{
					EffectBoardArr[posx][posy]->SetMode(1);
					EffectBoardArr[posx][posy]->SetColor(0, 0, 0, 1);
					EffectBoardArr[posx][posy]->setPath("../Resource/Texture/CharacterSprite/SubmarineO2fx.png", 1, 9);
					EffectBoardArr[posx][posy]->SetAnimationLoop(0, 0, 9, 100);
					EffectBoardArr[posx][posy]->SetFxtype(FXB_O2);
					EffectBoardArr[posx][posy]->setDraw(true);
				}
			}
		}
	}
}
void Board::ActionCheck()
{
	bool ready = false;
	for (int i = 0; i < UnitVector.size(); i++)
	{
		Submarine* s1 = dynamic_cast<Submarine*>(UnitVector[i]);
		if (s1 != nullptr && s1->getDraw() == true)
		{
			if (s1->getMovecost() > 0 && s1->getMovecost() > 0)
			{
				ready = true;
			}
		}
		Character* c1 = dynamic_cast<Character*>(UnitVector[i]);
		if (c1 != nullptr && c1->getDraw() == true)
		{
			if (c1->getMovecost() > 0 && c1->getMovecost() > 0)
			{
				ready = true;
			}
		}
	}
	if (ready == false)
	{
		uimanager.showPassTurnSBUI();
	}
	else
	{
		uimanager.closePassTurnSBUI();
	}
}
void Board::OpenPauseUI()
{
	uimanager.showPauseUI();
}
void Board::CloseMenuUI()
{
	uimanager.closeMoveMenuUI();
}
void Board::ClosePauseUI()
{
	uimanager.closePauseUI();
}
int& Board::GetBoardstate()
{
	return mainstate;
}
void Board::SetBoardType(int type, int x, int y)
{
	TypeArr[x][y] = type;
}
int Board::GetUnitindex(int posx, int posy)
{
	for (int i=0;i< UnitVector.size(); i++)
	{
		if (UnitVector[i]->getDraw() == true)
		{
			if ((int)UnitVector[i]->getBoardPosition().x == posx && (int)UnitVector[i]->getBoardPosition().y == posy)
			{
				return i;
			}
		}
		FinalBoss* boss = dynamic_cast<FinalBoss*>(UnitVector[i]);
		if (boss != nullptr && boss->getDraw() == true)
		{
			for (int j = 0; j < boss->getHurtbox().size(); j++)
			{
				if (boss->getHurtbox()[j].x == posx && boss->getHurtbox()[j].y == posy)
				{
					return i;
				}
			}
		}
	}
	return -1;
}
void Board::SetMainState(int state)
{
	mainstate = state;
}
int Board::GetTypeArr(int x,int y)
{
	return TypeArr[x][y];
}
int** Board::GetWholeTypeArr()
{
	return TypeArr;
}
GameObject* Board::GetBoardArr(int x, int y)
{
	return BoardArr[x][y];
}
int Board::GetMainState()
{
	return mainstate;
}
Unit* Board::GetUnitVector(int index)
{
	return UnitVector[index];
}
int Board::GetAmountUnit()
{
	return UnitVector.size();
}
int Board::GetSizeX()
{
	return SizeX;
}
int Board::GetSizeY()
{
	return SizeY;
}
UIManager& Board::GetUIManager()
{
	return uimanager;
}
Unit* Board::GetSelectedUnit()
{
	return UnitVector[GetUnitindex(SelectedUnitX,SelectedUnitY)];
}