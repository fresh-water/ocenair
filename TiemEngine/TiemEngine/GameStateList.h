#pragma once

enum GameState
{
	// list of all game states 
	GS_KMUTT = 0,
	GS_MAINMENU,
	GS_CHARACTERSELECT,
	GS_CUTSCENE1,
	GS_LEVEL1,
	GS_LEVEL2,
	GS_LEVEL3,
	GS_LEVEL4,
	GS_LEVELSELECT,

	// special game state. Do not change
	GS_RESTART,
	GS_QUIT,
	GS_NONE,
	GS_WIN,
	GS_LOSE
};