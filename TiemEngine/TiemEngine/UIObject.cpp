
#include "UIObject.h"
#include "GameEngine.h"
#include "SquareMeshVbo.h"
#include "GameDataStorage.h"



UIObject::UIObject()
{
	color = glm::vec3(0.0, 0.0, 0.0);
	coloroffset = glm::vec3(0.0, 0.0, 0.0);
}


UIObject::~UIObject()
{
}

void UIObject::SetActiveFn(void(*fn)(UIObject* button))
{
	ActiveFn = fn;
}

void UIObject::ActiveFunction()
{
	ActiveFn(this);
}

void UIObject::SetColor(float r, float g, float b)
{
	color = glm::vec3(r, g, b);
	alpha = 1.0f;
}
void UIObject::SetColor(float r, float g, float b, float a)
{
	color = glm::vec3(r, g, b);
	alpha = a;
}
glm::vec3 UIObject::GetColor()
{
	return color;
}

void UIObject::SetColorOffset(float r, float g, float b)
{
	coloroffset = glm::vec3(r, g, b);
}
glm::vec3 UIObject::GetColorOffset()
{
	return coloroffset;
}

void UIObject::SetTexture(string path)
{
	//texture = GameEngine::GetInstance()->GetRenderer()->LoadTexture(path);
	texture = GameDataStorage::GetInstance()->gettexturestorage(path);
}

void UIObject::SetMode(int mode)
{
	Mode = mode;
}
void UIObject::SetClickable(bool click)
{
	Clickable = click;
}
bool UIObject::GetClickable()
{
	return Clickable;
}
void UIObject::SetClickOnAnim(bool click)
{
	clickOnAnim = click;
}
void UIObject::SetDraw(bool draw)
{
	Draw = draw;
}
bool UIObject::GetDraw()
{
	return this->Draw;
}

void UIObject::setAnotherButton(UIObject* another)
{
	anotherUI = another;
}
UIObject* UIObject::getAnotherButton()
{
	return anotherUI;
}

bool UIObject::OnMouseStay(int x, int y)
{
	glm::vec3 size = this->GetSize();
	glm::vec3 pos = this->GetPosition();
	int winheight = GameEngine::GetInstance()->GetWindowHeight();
	int winwidth = GameEngine::GetInstance()->GetWindowWidth();
	float top = (pos.y * (winheight / 2.0f)) + abs((size.y / 2.0f) * (winheight / 2.0f));
	float bot = (pos.y * (winheight / 2.0f)) - abs((size.y / 2.0f) * (winheight / 2.0f));
	float left = (pos.x * (winwidth / 2.0f)) - abs((size.x / 2.0f) * (winwidth / 2.0f));
	float right = (pos.x * (winwidth / 2.0f)) + abs((size.x / 2.0f) * (winwidth / 2.0f));
	if (y >= bot && y <= top && x >= left && x <= right && Draw == true && Clickable == true)
	{
	/*	if (this->buttontype == UI_OPTION) {
			this->SetTexture("../Resource/Texture/UIOption4.png");
		}*/
		//if (this->buttontype == UI_START) {
		//	this->setSprite("../Resource/Texture/UIPlay1.png", 1, 1, 0, 0);
		//}
		/*if (this->buttontype == UI_EXIT) {
			this->setSprite("../Resource/Texture/UIExit1.png", 1, 1, 0, 0);
		}*/
		//if (this->buttontype == UI_BACK_OPTION) {
		//	this->SetTexture("../Resource/Texture/menuback2.png");
		//	this->SetSize(0.285, -0.269);
		//}
		/*if (this->buttontype == UI_LEVEL_BAR) {
			this->SetTexture("../Resource/Texture/missionbar2.png");
		}*/
		//else{
			if (!clickOnAnim)
			{
				this->SetColorOffset(-0.2, -0.2, -0.2);
			}
			else
			{
				this->SetColorOffset(-0.0, -0.0, -0.0);
				this->setSprite(this->texture, 2, 1, UIANIM_MOUSEON, 0);
			}
		//}
		
		//this->setSprite
		return true;
	}
	else if(Clickable == false)
	{
		return false;
	}
	else
	{
		/*if (this->buttontype == UI_OPTION) {
			this->SetTexture("../Resource/Texture/UIOption2.png");
		}*/
		//if (this->buttontype == UI_START) {
		//	this->setSprite("../Resource/Texture/UIPlay2.png", 1, 1, 0, 0);
		//}
		/*if (this->buttontype == UI_EXIT) {
			this->setSprite("../Resource/Texture/UIExit2.png", 1, 1, 0, 0);
		}*/
		//if (this->buttontype == UI_BACK_OPTION) {
		//	this->SetTexture("../Resource/Texture/menuback1.png");
		//	this->SetSize(0.25, -0.20);
		//}
		/*if (this->buttontype == UI_LEVEL_BAR) {
			this->SetTexture("../Resource/Texture/missionbar1.png");
		}*/
		if (!clickOnAnim)
		{
			this->SetColorOffset(-0.0, -0.0, -0.0);
		}
		else
		{
			this->SetColorOffset(-0.0, -0.0, -0.0);
			this->setSprite(this->texture, 2, 1, UIANIM_NORMAL, 0);
		}
		return false;
	}
}

bool UIObject::ClickOn(int x, int y)
{
	glm::vec3 size = this->GetSize();
	glm::vec3 pos = this->GetPosition();
	int winheight = GameEngine::GetInstance()->GetWindowHeight();
	int winwidth = GameEngine::GetInstance()->GetWindowWidth();
	float top = (pos.y * (winheight / 2.0f)) + abs((size.y / 2.0f) * (winheight / 2.0f));
	float bot = (pos.y * (winheight / 2.0f)) - abs((size.y / 2.0f) * (winheight / 2.0f));
	float left = (pos.x * (winwidth / 2.0f)) - abs((size.x / 2.0f) * (winwidth / 2.0f));
	float right = (pos.x * (winwidth / 2.0f)) + abs((size.x / 2.0f) * (winwidth / 2.0f));
	if (y >= bot && y <= top && x >= left && x <= right && Draw == true)
	{
		if (Clickable == true)
		{
			if (buttontype == UI_NORMAL)
			{
				ActiveFn(this);
			}
			else if (buttontype == UI_SOUND_SLIDER)
			{
				ActiveFn(anotherUI);
			}
			else if (buttontype == UI_SPEED_1 || buttontype == UI_SPEED_2 || buttontype == UI_SPEED_3) {
				ActiveFn(this);
			}
			else if (buttontype == UI_SKIP_ON || buttontype == UI_SKIP_OFF) {
				ActiveFn(this);
			}
			else if (buttontype == UI_START) {
				GameData::GetInstance()->gGameStateNext = GameState::GS_LEVELSELECT;
			}
			/*else if (buttontype == UI_LEVEL_BAR) {
				ActiveFn(this);
			}*/
			else 
			{
				
			}
		}
		return true;
	}
	return false;
}

bool UIObject::ClickOn(int x, int y,int& levelstate,int& boardstate)
{
	glm::vec3 size = this->GetSize();
	glm::vec3 pos = this->GetPosition();
	int winheight = GameEngine::GetInstance()->GetWindowHeight();
	int winwidth = GameEngine::GetInstance()->GetWindowWidth();
	float top = (pos.y * (winheight / 2.0f)) + abs((size.y / 2.0f) * (winheight / 2.0f));
	float bot = (pos.y * (winheight / 2.0f)) - abs((size.y / 2.0f) * (winheight / 2.0f));
	float left = (pos.x * (winwidth / 2.0f)) - abs((size.x / 2.0f) * (winwidth / 2.0f));
	float right = (pos.x * (winwidth / 2.0f)) + abs((size.x / 2.0f) * (winwidth / 2.0f));
	if (y >= bot && y <= top && x >= left && x <= right && Draw == true)
	{
		if (Clickable == true)
		{
			if (buttontype == UI_NORMAL)
			{
				ActiveFn(this);
			}
			else
			{
				cout << "Click ON Button: " << buttontype << endl;
			}
		}
		return true;
	}
	levelstate = LS_NORMAL;
	return false;
}

void UIObject::Render(glm::mat4 globalModelTransform)
{
	if (Draw == false)
	{
		return;
	}

	SquareMeshVbo* squareMesh = dynamic_cast<SquareMeshVbo*> (GameEngine::GetInstance()->GetRenderer()->GetMesh(SquareMeshVbo::MESH_NAME));

	GLuint modelMatixId = GameEngine::GetInstance()->GetRenderer()->GetModelMatrixAttrId();
	GLuint colorId = GameEngine::GetInstance()->GetRenderer()->GetColorUniformId();
	GLuint alphaId = GameEngine::GetInstance()->GetRenderer()->GetAlphaUniformId();
	GLuint renderModeId = GameEngine::GetInstance()->GetRenderer()->GetModeUniformId();
	GLuint offsetId = GameEngine::GetInstance()->GetRenderer()->GetOffsetUniformId();

	if (modelMatixId == -1) {
		cout << "Error: Can't perform transformation " << endl;
		return;
	}
	if (renderModeId == -1) {
		cout << "Error: Can't set renderMode in ImageObject " << endl;
		return;
	}
	if (alphaId == -1) {
		cout << "Error: Can't set ALPHA in ImageObject " << endl;
		return;
	}
	if (offsetId == -1) {
		cout << "Error: Can't set OFFSET in ImageObject " << endl;
		return;
	}

	vector <glm::mat4> matrixStack;

	glm::mat4 currentMatrix = this->getTransform();

	if (squareMesh != nullptr) {

		currentMatrix = globalModelTransform * currentMatrix;
		glUniformMatrix4fv(modelMatixId, 1, GL_FALSE, glm::value_ptr(currentMatrix));
		glUniform3f(colorId, color.x, color.y, color.z);
		glUniform3f(offsetId, coloroffset.x, coloroffset.y, coloroffset.z);
		glUniform1f(alphaId, this->alpha);
		glUniform1i(renderModeId, Mode);
		glBindTexture(GL_TEXTURE_2D, texture);
		if (isAnim) { 
			squareMesh->AdjustTexcoord(uv);
		}
		else {
			squareMesh->ResetTexcoord();
		}
		squareMesh->Render();
	}
}

void UIObject::setButtontype(int type)
{
	buttontype = type;
}

int UIObject::getButtontype()
{
	return buttontype;
}

void UIObject::Update(float deltaTime)
{
	timeCount += deltaTime;
	if (timeCount > animationTime)
	{
		this->NextAnimation();
		this->GenUV();
		timeCount = 0;
	}


}
void UIObject::GenUV()
{
	uv[0] = currentColumn / (columnMax * 1.0f);
	uv[1] = currentRow / (rowMax * 1.0f);

	uv[2] = (currentColumn + 1) / (columnMax * 1.0f);
	uv[3] = currentRow / (rowMax * 1.0f);

	uv[4] = (currentColumn + 1) / (columnMax * 1.0f);
	uv[5] = (currentRow + 1) / (rowMax * 1.0f);

	uv[6] = currentColumn / (columnMax * 1.0f);
	uv[7] = (currentRow + 1) / (rowMax * 1.0f);
}
void UIObject::setSprite(string fileName, int maxrow, int maxcolumn, int currrow, int currcol) {
	this->columnMax = maxcolumn;
	this->rowMax = maxrow;

	this->startRow = currrow;
	this->startColumn = currcol;
	this->currentColumn = currcol;
	this->currentRow = currrow;

	//this->texture = GameEngine::GetInstance()->GetRenderer()->LoadTexture(fileName);
	this->texture = GameDataStorage::GetInstance()->gettexturestorage(fileName);
	isAnim = true;

	GenUV();
}
void UIObject::setSprite(unsigned int tex, int maxrow, int maxcolumn, int currrow, int currcol) {
	this->columnMax = maxcolumn;
	this->rowMax = maxrow;

	this->startRow = currrow;
	this->startColumn = currcol;
	this->currentColumn = currcol;
	this->currentRow = currrow;

	this->texture = tex;
	isAnim = true;

	GenUV();
}
void UIObject::SetAnimationLoop(int startRow, int startColumn, int howManyFrame, int delayBetaweenFrame)
{
	this->startRow = startRow;
	this->startColumn = startColumn;
	this->loopMax = howManyFrame;
	this->loopCount = 0;
	this->animationTime = delayBetaweenFrame;

	currentColumn = startColumn;
	currentRow = startRow;

	GenUV();
}
void UIObject::NextAnimation()
{
	loopCount++;
	if (loopCount < loopMax)
	{
		currentColumn++;
		if (currentColumn == columnMax)
		{
			currentColumn = 0;
			currentRow++;
			if (currentRow == rowMax)
			{
				currentRow = 0;
			}
		}
	}
	else if(Loop == false)
	{
		Draw = false;
	}
	else
	{
		currentRow = startRow;
		currentColumn = startColumn;
		loopCount = 0;
	}

}
void UIObject::setPath(string fileName, int row, int column) {
	this->columnMax = column;
	this->rowMax = row;
	//texture = GameEngine::GetInstance()->GetRenderer()->LoadTexture(fileName);
	texture = GameDataStorage::GetInstance()->gettexturestorage(fileName);
	isAnim = true;
}
void UIObject::setLoop(bool fact) {
	this->Loop = fact;
}
