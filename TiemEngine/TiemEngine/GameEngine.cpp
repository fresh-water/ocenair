
#include "GameEngine.h"

GameEngine* GameEngine::instance = nullptr;

GameEngine::GameEngine()
{
	renderer = nullptr;
}

GameEngine * GameEngine::GetInstance()
{
	if (instance == nullptr) {
		instance = new GameEngine();
	}
	return instance;
}

GLRenderer * GameEngine::GetRenderer()
{
	return this->renderer;
}

void GameEngine::Init(int width, int height)
{
	winWidth = width;
	winHeight = height;
	renderer = new GLRenderer(width, height);
	renderer->InitGL("../Resource/Shader/vertext.shd", "../Resource/Shader/fragment.shd");
	ae.init();
	SetDrawArea(-3, 3, -3, 3);
	SetBackgroundColor(0.0f, 0.0f, (102/255.0f));
	//SetBackgroundColor(0.0f, 0.0f, 0.0f);
}

void GameEngine::Render(vector<DrawableObject*> renderObjects)
{
	this->GetRenderer()->Render(renderObjects);
}

void GameEngine::SetDrawArea(float left, float right, float bottom, float top)
{
	renderer->SetOrthoProjection(left, right, bottom, top);
}

void GameEngine::SetBackgroundColor(float r, float g, float b)
{
	renderer->SetClearColor(r, g, b);
}

void GameEngine::AddMesh(string name, MeshVbo* mesh)
{
	renderer->AddMesh(name, mesh);
}

void GameEngine::ClearMesh()
{
	renderer->ClearMesh();
}

int GameEngine::GetWindowWidth()
{
	return winWidth;
}

int GameEngine::GetWindowHeight()
{
	return winHeight;
}

int GameEngine::GetDeltaTime()
{
	return deltaTime;
}

void GameEngine::SetDeltaTime(int dt)
{
	deltaTime = dt;
}
void GameEngine::playMusic(string name)
{
	mu = ae.loadMusic(name);
	mu.volume(getMusicSound());
	mu.play(-1);
}
void GameEngine::playSound(string name)
{
	se = ae.loadSoundEffect(name);
	se.volume(getEffectSound());
	se.play();
}

void GameEngine::setMasterSound(int vol) 
{ 
	this->masterSound = vol;
	mu.volume((masterSound / 128.0f) * musicSound);
	se.volume((masterSound / 128.0f) * effectSound);
}
void GameEngine::setMusicSound(int vol)
{
	this->musicSound = vol;
	mu.volume((masterSound / 128.0f) * musicSound);
}
void GameEngine::setEffectSound(int vol)
{
	this->effectSound = vol; 
	se.volume((masterSound / 128.0f) * effectSound);
}
void GameEngine::setSpeedLevel(int speed) {
	this->curSpeedlevel = speed;
}
int GameEngine::getSpeedLevel() {
	return curSpeedlevel;
}
void GameEngine::setSkipLevel(bool skip) {
	this->skip = skip;
}
bool GameEngine::getSkipLevel() {
	return skip;
}
int GameEngine::getMasterSound(){ return this->masterSound; }
int GameEngine::getMusicSound(){ return (this->masterSound / 128.0f) * this->musicSound; }
int GameEngine::getEffectSound(){ return (this->masterSound / 128.0f) * this->effectSound; }
int GameEngine::getRealMusicSound() { return this->musicSound; }
int GameEngine::getRealEffectSound() { return this->effectSound; }

float GameEngine::ConvertX(int x)
{
	float result;
	result = ((x - winWidth /2.0f)* winWidth)/ winWidth;
	return result;
}

float GameEngine::ConvertY(int y)
{
	float result;
	result = ((winHeight / 2.0f - y) * winHeight) / winHeight;
	return result;
}
bool GameEngine::getIsMouseDown() {
	return isMouseDown;
}
void GameEngine::setIsMouseDown(bool flag) {
	isMouseDown = flag;
}
glm::vec2 GameEngine::getCurMousePos() {
	return curMousePos;
}
void GameEngine::setCurMousePos(int x, int y) {
	curMousePos = glm::vec2(x, y);
}
float GameEngine::setCurrAnimspeed()
{
	if (curSpeedlevel == 1)
	{
		return 250.0f;
	}
	else if (curSpeedlevel == 2)
	{
		return 150.0f;
	}
	else if (curSpeedlevel == 3)
	{
		return 80.0f;
	}
}
