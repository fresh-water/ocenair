#include "Unit.h"
#include "Astar.h"

//argument
vector<glm::vec2> Unit::ActiveSkill(int** typearr, int sizex, int sizey)
{
	vector<glm::vec2> skillarea;
	cout << "IN Unit: " << endl;
	cout << this->getName() << " use skill" << endl;
	return skillarea;
}
void Unit::Init(string name, int hp, int maxhp, int atk, int atkdis, int move, int x, int y, int eva, int def, int cd, string tex) {
	this->name = name;
	this->hp = hp;
	this->maxhp = maxhp;
	this->atk = atk;
	this->atkdis = atkdis;
	this->move = move;
	this->boardpos = glm::vec2(x, y);
	this->eva = eva;
	this->def = def;
	this->cd = cd;
	this->texture = tex;
	for (int i = 0;i<2;i++)
	{
		miniHpbar[i] = new GameObject;
		miniHpbar[i]->SetSize(80, 7);
		miniHpbar[i]->SetMode(0);
	}
	miniHpbar[0]->SetColor(0, 0.5f, 0, 0.7f);
	miniHpbar[1]->SetColor(0, 1, 0, 0.7f);
}
string Unit::getName() { return name; }
void Unit::setName(string name) { this->name = name; }
int Unit::getHp() 
{
	if (hp > 0)
	{
		return hp;
	}
	return 0;
}
void Unit::setHp(int hp) {
	if (hp > 0)
	{
		if (hp >= maxhp)
		{
			this->hp = maxhp;
		}
		else
		{
			this->hp = hp;
		}
	}
	else
	{
		this->hp = 0;
	}
	updateMiniHpbar(); 
}
int Unit::getAtk() { return atk + getBuffEffect(BUFF_ATK); }
void Unit::setAtk(int atk) {this->atk = atk;}
int Unit::getDef() { return this->def + getBuffEffect(BUFF_DEF); }
void Unit::setDef(int def) { this->def = def; }
int Unit::getEva() { return this->eva + getBuffEffect(BUFF_EVA); }
void Unit::setEva(int eva) { this->eva = eva; }
int Unit::getAtkdis(){return atkdis;}
void Unit::setAtkdis(int atkdis){this->atkdis = atkdis;}
void Unit::setMove(int move) {this->move = move;}
int Unit::getMove() {return move + getBuffEffect(BUFF_MOVEDIS);}
int Unit::getRow() { return row; }
void Unit::setRow(int row) { this->row = row; }
int Unit::getCol() { return col; }
void Unit::setCol(int col) { this->col = col; }
int Unit::getMaxHp(){return maxhp;}
void Unit::setCD(int cd) { this->cd = cd; }
int Unit::getCD() { return cd; }
string Unit::getatktexture() { return this->attacktexture; }
void Unit::setatktexture(string tex) { this->attacktexture = tex; }
void Unit::setBoardPosition(int posx, int posy) {
	boardpos.x = posx;
	boardpos.y = posy;
}
glm::vec2 Unit::getBoardPosition() {
	return boardpos;
}
void Unit::PassTurn() {
	vector<Buff> tempBuff;
	for(int i=0;i<buff.size();i++)
	{ 
		buff[i].decreaseDuration(1);
		if (buff[i].getDuration() > 0)
		{
			tempBuff.push_back(buff[i]);
		}
	}
	this->buff = tempBuff;
	this->cd = this->cd - 1;
}
string Unit::getTexture()
{
	return texture;
}
void Unit::updateMiniHpbar()
{
	if (hp <= 0)
	{
		miniHpbar[0]->setDraw(false);
		miniHpbar[1]->setDraw(false);
		return;
	}
	else
	{
		miniHpbar[0]->setDraw(true);
		miniHpbar[1]->setDraw(true);
	}

	float radio = hp / (maxhp * 1.0f);
	miniHpbar[1]->SetSize(miniHpbar[0]->GetSize().x * radio, miniHpbar[0]->GetSize().y);
	float diff = (miniHpbar[0]->GetSize().x - miniHpbar[1]->GetSize().x) / 2.0f;
	miniHpbar[1]->SetPosition(miniHpbar[0]->GetPosition());
	miniHpbar[1]->SetPosition(glm::vec3(miniHpbar[1]->GetPosition().x - diff, miniHpbar[1]->GetPosition().y, miniHpbar[1]->GetPosition().z));
}

GameObject* Unit::getminiHpbar(int index)
{
	return miniHpbar[index];
}

//ini image
void Unit::SetColor(float r, float g, float b) {
	GameObject::SetColor(r, g, b);
}
void Unit::SetPosition(glm::vec3 newPosition)
{
	glm::vec3 currentoffset[2] = { miniHpbar[0]->GetPosition() - GetPosition(),miniHpbar[1]->GetPosition() - GetPosition() };
	DrawableObject::SetPosition(newPosition);
	for (int i=0;i<2;i++)
	{
		miniHpbar[i]->SetPosition(newPosition + currentoffset[i]);
	}
}
void Unit::SetMode(int mode) {
	GameObject::SetMode(mode);
}
void Unit::SetTexture(string path) {
	GameObject::SetTexture(path);
}
void Unit::SetAnimation(int anim) {
	if (anim == WALK || anim == IDLE)
	{
		this->SetAnimationLoop(anim, 0, 4, 100);
	}
	else if(anim == HURT)
	{
		this->SetAnimationLoop(2, 0, 1, 100);
	}
	else if (anim == DOWN)
	{
		this->SetAnimationLoop(2, 1, 1, 100);
	}
	else if (anim == SKILL)
	{
		this->SetAnimationLoop(2, 2, 1, 100);
	}
}

void Unit::setWalkingAnimPath(vector<glm::vec2> vec)
{
	walkinganimpath = vec;
}
vector<glm::vec2> Unit::getWalkingAnimPath()
{
	return walkinganimpath;
}

Unit::Unit(string name, int hp, int maxhp, int atk,int atkdis, int move, int x, int y, int eva, int def, int cd, string tex) {
	this->name = name;
	this->hp = hp;
	this->maxhp = maxhp;
	this->atk = atk;
	this->atkdis = atkdis;
	this->move = move;
	this->boardpos = glm::vec2(x,y);
	this->eva = eva;
	this->def = def;
	this->cd = cd;
	this->texture = tex;
	//////////////////
}
void Unit::addBuff(int type, int effect, int duration)
{
	Buff b(type,effect,duration);
	bool same = false;
	for (int i = 0; i < buff.size(); i++)
	{
		if (buff[i].getType() == type)
		{
			buff[i] = b;
			same = true;
		}
	}
	if (!same) { buff.push_back(b); }
}
int Unit::getBuffEffect(int type) 
{
	for (int i = 0; i < buff.size(); i++) 
	{
		if (buff[i].getType() == type)
		{
			return buff[i].getEffect();
		}
	}
	return 0;
}

vector<Buff> Unit::getBuffVector() {
	return buff;
}
Unit::Unit() {
	this->hp = 0;
	this->maxhp = 0;
	this->atk = 0;
	this->atkdis = 0;
	this->move = 0;
	this->boardpos = glm::vec3(0, 0, 0);
	this->eva = 0;
	this->def = 0;
	this->cd = 0;
	this->row = 0;
	this->col = 0;
	this->name = "none";
	this->texture = "nopath";
}