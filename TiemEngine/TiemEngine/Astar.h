#ifndef ASTAR_H
#define ASTAR_H
#include <string>
#include "Node.h"
#include "Board.h"
#include "EnumList.h"

using namespace std;


vector<glm::vec2> Astar(int** typearr, int sizex, int sizey, int type, glm::vec2 start, glm::vec2 end);
vector<glm::vec2> Astar(Board board, int type, glm::vec2 start, glm::vec2 end);
vector<glm::vec2> UnitOpenSpaceAstar(int** typearr, int sizex, int sizey, int type, int distant, glm::vec2 pos);
vector<glm::vec2> UnitOpenSpaceAstar(Board board, int type, int distant, glm::vec2 pos);
void EnemyAstar2(Board board, Enemy* enemy);
int EnemyFindclosestPlayer(vector<Unit*> unitarr, Enemy* enemy);
vector<glm::vec2> EnemyAstar(Board board,int hitdistance, glm::vec2 start, glm::vec2 end);

#endif