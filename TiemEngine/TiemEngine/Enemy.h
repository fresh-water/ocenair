#ifndef ENEMY_H
#define ENEMY_H
#include "Unit.h"
#include <string>

using namespace std;
class Enemy : public Unit {
private:
	string target;
	glm::vec2 targetpos;
	vector<glm::vec2> path;
	int skillcooldownturn = 0;
	bool inVision;
	bool Active = false;
	bool FoundPlayer = true;

public:

	bool wait = false;

	virtual vector<glm::vec2> ActiveSkill(int** typearr, int sizex, int sizey);
	int getskillcooldownturn();
	void setskillcooldownturn(int amount);

	string getEnemyType();
	void setEnemyTargetpos(glm::vec2 targetpos);
	glm::vec2 getTargetpos();
	void clearpath();
	void setEnemypath(vector <glm::vec2> path);
	vector<glm::vec2> getEnemypath();
	void followTarget();
	virtual void readCharacterData(string filename,int index);

	void setFoundPlayer(bool fact);
	bool getFoundPlayer();
	virtual void setActive(bool fact);
	virtual bool getActive();

	void setinVision(bool fact);
	bool getinVision();
	virtual void Render(glm::mat4 globalModelTransform);

	Enemy(string name, int hp, int maxhp, int atk, int atkdis, int move, int x, int y, int eva, int def, int cd, string tex);
	Enemy();
};
#endif