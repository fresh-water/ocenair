#include "LevelKMUTT.h"

void LevelKMUTT::LevelLoad()
{
	SquareMeshVbo* square = new SquareMeshVbo();
	square->LoadData();
	GameEngine::GetInstance()->AddMesh(SquareMeshVbo::MESH_NAME, square);
}

void LevelKMUTT::LevelInit()
{
	maincamera = new Camera;
	maincamera->Init(GameEngine::GetInstance()->GetWindowWidth(), GameEngine::GetInstance()->GetWindowHeight());
	maincamera->SetEnable(false);

	currindex = 1;
	numofimage = 1;
	curralpha = 0.0f;

	UIObject * bg = new UIObject();
	bg->SetMode(0);
	bg->SetDraw(true);
	bg->SetSize(2, -2);
	bg->SetPosition(glm::vec3(0.0f, 0.0f, 0.0f));
	bg->SetClickable(false);
	bg->SetColorOffset(1,1,1);
	UIList.push_back(bg);

	for (int i = 0;i<1;i++)
	{
		cutsceneImage[i] = new UIObject();
		cutsceneImage[i]->SetMode(1);
		cutsceneImage[i]->SetDraw(true);
		cutsceneImage[i]->SetClickable(false);
		UIList.push_back(cutsceneImage[i]);
	}

	cutsceneImage[0]->SetSize(2.0, -2.0);
	cutsceneImage[0]->SetPosition(glm::vec3(0.0f, 0.0f, 0.0f));
	cutsceneImage[0]->SetColor(0.0f, 0.0f, 0.0f,0.0f);
	cutsceneImage[0]->SetTexture("../Resource/Texture/logodddt.png");
	cutsceneImage[0]->SetDraw(true);
}

void LevelKMUTT::LevelUpdate()
{
	maincamera->CameraDrawArea();
	int deltaTime = GameEngine::GetInstance()->GetDeltaTime();
	for (DrawableObject* obj : objectsList) {
		obj->Update(deltaTime);
	}
	if (curralpha < 2.0f)
	{
		curralpha += 1 / 100.0f;
		cutsceneImage[0]->SetColor(0.0f, 0.0f, 0.0f, curralpha);
	}
	else
	{
		GameData::GetInstance()->gGameStateNext = GameState::GS_MAINMENU;
	}
}

void LevelKMUTT::LevelDraw()
{
	GameEngine::GetInstance()->Render(objectsList);
	GameEngine::GetInstance()->GetRenderer()->RenderUI(UIList);
}

void LevelKMUTT::LevelFree()
{
	for (DrawableObject* obj : objectsList) {
		if (obj != nullptr)
		{
			delete obj;
		}
	}
	objectsList.clear();
	for (DrawableObject* obj : UIList) {
		delete obj;
	}
	UIList.clear();
}

void LevelKMUTT::LevelUnload()
{
	GameEngine::GetInstance()->ClearMesh();
}

void LevelKMUTT::HandleKey(char key)
{
	switch (key)
	{
	case 'a':
		break;
	case 'd':
		break;
	case 'w':
		break;
	case 'I':
		break;
	case 'O':
		break;

	case 'E': GameData::GetInstance()->gGameStateNext = GameState::GS_QUIT; ; break;
	case 'r': GameData::GetInstance()->gGameStateNext = GameState::GS_RESTART; ; break;
	case 'e': GameData::GetInstance()->gGameStateNext = GameState::GS_WIN; ; break;		//GS_WIN/GS_LOSE scene loading
	case 's': GameData::GetInstance()->gGameStateNext = GameState::GS_MAINMENU; ; break;
	case 'S':
		break;
	}
}

void LevelKMUTT::HandleMouse(int type, int x, int y)
{
	int scrolling = x;
	int realX, realY;
	realX = GameEngine::GetInstance()->ConvertX(x);
	realY = GameEngine::GetInstance()->ConvertY(y);

	if (type == MOUSEMOVE)//move
	{
		HandleMouseMove(realX, realY);
	}
	else if (type == MOUSELEFT)//left
	{
		HandleMouseLeft(realX, realY);
	}
	else if (type == MOUSERIGHT)//right
	{
		HandleMouseRight(realX, realY);
	}
	else if (type == MOUSESCROLL)//scroll
	{
		HandleMouseScroll(scrolling);
	}

}

void LevelKMUTT::HandleMouseLeft(int x, int y)
{
	//if (currindex >= numofimage)
	//{
	//	GameData::GetInstance()->gGameStateNext = GameState::GS_MAINMENU;
	//	currindex = numofimage - 1;
	//}
	//cutsceneImage[currindex]->SetDraw(true);
	//currindex++;
}
void LevelKMUTT::HandleMouseRight(int x, int y)
{
	
}
void LevelKMUTT::HandleMouseMove(int x, int y)
{
	for (int i = UIList.size() - 1; i >= 0; i--)
	{
		UIObject* ui1 = dynamic_cast<UIObject*>(UIList[i]);
		if (ui1 != nullptr)
		{
			if (ui1->OnMouseStay(x, y))
			{

			}
		}
	}
}
void LevelKMUTT::HandleMouseScroll(int amount)
{

}

