#include "Submarine.h"
#include "Astar.h"

void Submarine::Init(string name, int hp, int maxhp, int atk, int atkdis, int move, int x, int y, int eva, int def, int cd, string tex,
	int losin, int losout)
{
	Unit::Init(name, hp, maxhp, atk, atkdis, move, x, y, eva, def, cd, tex);
	this->LOSin = losin;
	this->LOSout = losout;
	this->atkcost = 0;
	this->movecost = 0;
	this->SubCD[0] = 0;
	this->SubCD[1] = 0;
}
vector<glm::vec2> Submarine::ActiveSkill1(int** typearr, int sizex, int sizey)
{
	vector<glm::vec2> skillarea;
	glm::vec2 unitpos = this->getBoardPosition();
	skillarea.push_back(unitpos);
	return skillarea;
}
vector<glm::vec2> Submarine::ActiveSkill2(int** typearr, int sizex, int sizey)
{
	glm::vec2 unitpos = this->getBoardPosition();
	vector<glm::vec2> skillarea = UnitOpenSpaceAstar(typearr, sizex, sizey, AT_ATK, 2, unitpos);
	return skillarea;
}
vector<glm::vec2> Submarine::ActiveSkill3(int** typearr, int sizex, int sizey)
{
	vector<glm::vec2> skillarea;
	glm::vec2 unitpos = this->getBoardPosition();
	for (int j = -2; j <= 2; j++)
	{
		for (int i = -2; i <= 2; i++)
		{
			skillarea.push_back(glm::vec2(unitpos.x + i, unitpos.y + j));
		}
	}
	return skillarea;
}
void Submarine::PassTurn()
{
	Unit::PassTurn();
	for (int i=0;i<2;i++)
	{
		this->SubCD[i]--;
	}
}
int Submarine::getSubCD(int index)
{
	return this->SubCD[index];
}
void Submarine::setSubCD(int index, int amount)
{
	this->SubCD[index] = amount;
}
int Submarine::getHp() {
	return Unit::getHp();
}
int Submarine::getAtk() {
	return Unit::getAtk();
}
int Submarine::getAtkcost(){
	return atkcost;
}
int Submarine::getMovecost(){
	return movecost;
}
void Submarine::setAtkcost(int amount)
{
	this->atkcost = amount;
}
void Submarine::setMovecost(int amount)
{
	this->movecost = amount;
}
int Submarine::getLOSin() {return LOSin;}
int Submarine::getLOSout() {return LOSout;}
void Submarine::setLOS(int losin, int losout) {
	this->LOSin = losin;
	this->LOSout = losout;
}
void Submarine::readCharacterData(string filename, int index){
	//����	 MaxHp	Attack	MoveDistance  AtkDistance	�ź(%max100)	def	oxygen	�����ͧ���ǧ�	�����ͧ���ǧ�͡	Texture
	string line;
	int atk, movedis, eva, def,atkdis,maxhp,row,column;
	string oxygen;
	string name;
	string texture, atktexture;
	int LOSin, LOSout;
	ifstream MyFile(filename);
	if (!MyFile.is_open()) {
		cout << "Error" << endl;
		return;
	}      
	for (int i=0;i<index;i++)
	{
		getline(MyFile, line);
	}
	MyFile >> name;
	MyFile >> maxhp;
	MyFile >> atk;
	MyFile >> movedis;
	MyFile >> atkdis;
	MyFile >> eva;
	MyFile >> def;
	MyFile >> oxygen;
	MyFile >> LOSin;
	MyFile >> LOSout;
	MyFile >> texture;

	Init(name,maxhp,maxhp,atk,atkdis,movedis,0,0,eva,def,0,texture,LOSin,LOSout);

	MyFile >> row;
	MyFile >> column;
	MyFile >> atktexture;

	this->setRow(row);
	this->setCol(column);
	this->setatktexture(atktexture);

	this->setPath(this->getTexture(), row, column);
	this->SetAnimationLoop(0, 0, 8, 100);

	MyFile.close();
}
vector<glm::vec2> Submarine::HealOxygen(int distant){

	vector<glm::vec2> O2area;
	int posx = getBoardPosition().x;
	int posy = getBoardPosition().y;
	O2area.push_back(glm::vec2(posx + 1, posy));
	O2area.push_back(glm::vec2(posx - 1, posy));
	O2area.push_back(glm::vec2(posx, posy + 1));
	O2area.push_back(glm::vec2(posx, posy - 1));

	O2area.push_back(glm::vec2(posx + 1, posy + 1));
	O2area.push_back(glm::vec2(posx - 1, posy + 1));
	O2area.push_back(glm::vec2(posx + 1, posy - 1));
	O2area.push_back(glm::vec2(posx - 1, posy - 1));
	return O2area;
}
void Submarine::SetAnimation(int anim) {
	this->SetAnimationLoop(0, 0, 8, 100);
}
Submarine::Submarine(string name, int hp, int maxhp, int atk, int atkdis, int move, int x, int y, int eva, int def, int cd, string tex,
	int losin ,int losout) : Unit(name,hp,maxhp,atk, atkdis,x,y,move,eva,def,cd,tex) {
	this->LOSin = losin;
	this->LOSout = losout;
	this->atkcost = 0;
	this->movecost = 0;
}
Submarine::Submarine(): Unit(){

}