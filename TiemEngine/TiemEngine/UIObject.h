#pragma once

#include <vector>
#include "GLRenderer.h"
#include "GameEngine.h"
#include "GameObject.h"
#include "DrawableObject.h"
#include "gtc/matrix_transform.hpp"
#include "gtc/type_ptr.hpp"
#include <string>
#include "Enumlist.h"

using namespace std;

class UIObject :public DrawableObject
{
	glm::vec3 color;
	glm::vec3 coloroffset;
	float alpha = 1.0f;
	unsigned int texture;
	int Mode = 0;//mode 0 = color , mode = 1 texture
	bool Clickable = false;
	bool Draw = false;
	bool clickOnAnim = false;
	int buttontype;

	UIObject* anotherUI;
	void(*ActiveFn)(UIObject* button);

	int rowMax;
	int columnMax;
	int currentRow;
	int currentColumn;
	float uv[8];
	int startRow;
	int startColumn;
	int loopMax;
	int loopCount;
	int animationTime;
	int timeCount;

	bool isAnim = false;
	bool Loop = true;

public:
	bool selected = false;
	UIObject();
	~UIObject();
	virtual void SetActiveFn(void(*fn)(UIObject* button));
	virtual void ActiveFunction();
	virtual void SetColor(float r, float g, float b);
	virtual void SetColor(float r, float g, float b, float a);
	glm::vec3 GetColor();
	virtual void SetColorOffset(float r, float g, float b);
	glm::vec3 GetColorOffset();
	virtual void SetMode(int mode);
	virtual void SetDraw(bool draw);
	virtual bool GetDraw();
	virtual void SetClickable(bool click);
	virtual bool GetClickable();

	virtual void SetClickOnAnim(bool click);

	virtual bool OnMouseStay(int x, int y);
	virtual bool ClickOn(int x, int y);
	virtual bool ClickOn(int x, int y, int& levelstate, int& boardstate);

	virtual void SetTexture(string path);
	virtual void Render(glm::mat4 globalModelTransform);

	virtual void setButtontype(int type);
	virtual int getButtontype();

	virtual void setAnotherButton(UIObject* another);
	virtual UIObject* getAnotherButton();

	virtual void Update(float deltaTime);
	virtual void GenUV();
	virtual void setSprite(string fileName, int maxrow, int maxcolumn, int currrow, int currcol);
	virtual void setSprite(unsigned int tex, int maxrow, int maxcolumn, int currrow, int currcol);
	virtual void SetAnimationLoop(int startRow, int startColumn, int howManyFrame, int delayBetaweenFrame);
	virtual void NextAnimation();
	virtual void setPath(string fileName, int row, int column);
	virtual void setLoop(bool fact);
};