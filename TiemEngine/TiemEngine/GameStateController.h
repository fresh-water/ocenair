#pragma once

#include "GameEngine.h"
#include "GameStateList.h"
#include "GameData.h"
#include "Level.h"
#include "LevelTest.h"
#include "LevelMainMenu.h"
#include "LevelCharacterSelect.h"
#include "Level3.h"
#include "LevelTemp.h"
#include "SceneLose.h"
#include "SceneWin.h"
#include "LevelCutsceneTest.h"
#include "LevelBoard.h"
#include "LevelMapSelect.h"
#include "LevelKMUTT.h"

using namespace std;
class GameStateController
{

public:

	
	Level* currentLevel;

	GameStateController();
	void Init(GameState gameStateInit);
	void Update();
};