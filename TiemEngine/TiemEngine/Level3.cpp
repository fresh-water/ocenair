#include "Level3.h"

const int BOARDSIZEROW = 40;
const int BOARDSIZECOLUMN = 40;
const float CAMERASPEED = 10;

GameObject* allobj[BOARDSIZEROW][BOARDSIZECOLUMN];

float currSize = 100.0f;

void Level3::LevelLoad()
{
	SquareMeshVbo* square = new SquareMeshVbo();
	square->LoadData();
	GameEngine::GetInstance()->AddMesh(SquareMeshVbo::MESH_NAME, square);

	//cout << "Load Level" << endl;
}

void Level3::LevelInit()
{
	maincamera = new Camera;
	maincamera->Init(GameEngine::GetInstance()->GetWindowWidth(), GameEngine::GetInstance()->GetWindowHeight(),
					- GameEngine::GetInstance()->GetWindowWidth() / 2, BOARDSIZEROW * currSize -GameEngine::GetInstance()->GetWindowWidth() / 2,
					- GameEngine::GetInstance()->GetWindowHeight() / 2, BOARDSIZECOLUMN * currSize - GameEngine::GetInstance()->GetWindowHeight() / 2);

	//double currposx, currposy;
	for (int i = 0; i < BOARDSIZEROW; i++)
	{
		for (int j = 0; j < BOARDSIZECOLUMN; j++)
		{
			allobj[i][j] = new GameObject();
			if ((i+j)%2 == 0)
			{
				allobj[i][j]->SetColor(0.0, 0.0, 0.0);
			}
			else 
			{
				allobj[i][j]->SetColor(1.0, 1.0, 1.0);
			}
			allobj[i][j]->SetSize(currSize, currSize);
			allobj[i][j]->SetPosition(glm::vec3((-maincamera->GetWindowWidth()/2.0f) + i* allobj[i][j]->GetSize().x + allobj[i][j]->GetSize().x/2.0f,
											(-maincamera->GetWindowHeight()/2.0f) +j* allobj[i][j]->GetSize().y + allobj[i][j]->GetSize().y / 2.0f, 0.0f));
			objectsList.push_back(allobj[i][j]);
		}
	}

	GameObject* p1 = new GameObject();
	p1->SetSize(-100.0f,-100.0f);
	p1->SetPosition(allobj[0][0]->GetPosition());
	//p1->SetTexture("../Resource/Texture/character2.png");
	p1->SetMode(1);
	objectsList.push_back(p1);

	player = p1;

	//cout << "Init Level" << endl;
}

void Level3::LevelUpdate()
{
	maincamera->CameraDrawArea();
	//cout << "Update Level" << endl;
}

void Level3::LevelDraw()
{
	GameEngine::GetInstance()->Render(objectsList);
	//cout << "Draw Level" << endl;
}

void Level3::LevelFree()
{
	for (DrawableObject* obj : objectsList) {
		delete obj;
	}
	objectsList.clear();
	//cout << "Free Level" << endl;
}

void Level3::LevelUnload()
{
	GameEngine::GetInstance()->ClearMesh();
	//cout << "Unload Level" << endl;
}

int Level3::WorldtoBoardX(float x)
{
	int result;
	float fresult;
	fresult = (float)(x + (maincamera->GetCenter().x * maincamera->GetZoomSize()) + ((maincamera->GetWindowWidth()*maincamera->GetZoomSize())/2.0f))/maincamera->GetZoomSize();
	fresult /= currSize;
	//cout << "ABS X = " << fresult << endl;
	result = floorf(fresult);
	return result;
}
int Level3::WorldtoBoardY(float y)
{
	int result;
	float fresult;
	fresult = (float)(y + (maincamera->GetCenter().y * maincamera->GetZoomSize()) + ((maincamera->GetWindowHeight() * maincamera->GetZoomSize()) / 2.0f)) / maincamera->GetZoomSize();
	fresult /= currSize;
	//cout << "ABS Y = " << fresult << endl;
	result = floorf(fresult);
	return result;
}

void Level3::HandleKey(char key)
{

	switch (key)
	{
		case 'a': 
			maincamera->CameraMove(0, -10);
			break;
		case 'd': 
			maincamera->CameraMove(0, 10);
			break;
		case 'w':
			maincamera->CameraMove(10, 0);
			break;
		case 's':
			maincamera->CameraMove(-10, 0);
			break;
		case 'I':
			maincamera->SetCameraPosition(0,0);
			break;
		case 'O':
			maincamera->CameraSetZoom(100);
			break;

		case 'E': GameData::GetInstance()->gGameStateNext = GameState::GS_QUIT; ; break;
		case 'r': GameData::GetInstance()->gGameStateNext = GameState::GS_RESTART; ; break;
		case 'e': GameData::GetInstance()->gGameStateNext = GameState::GS_LEVEL1; ; break;
	}
}

void Level3::HandleMouse(int type, int x, int y)
{
	int scrolling = x;
	int realX, realY;
	realX = GameEngine::GetInstance()->ConvertX(x);
	realY = GameEngine::GetInstance()->ConvertY(y);

	if (type == MOUSEMOVE)//move
	{
		//cout << "wtfff" << endl;
		HandleMouseMove(realX, realY);
	}
	else if (type == MOUSELEFT)//left
	{
		//HandleMouseMove(x, y);
		HandleMouseLeft(realX, realY);
	}
	else if (type == MOUSERIGHT)//right
	{
		//ocout << "right" << endl;
		HandleMouseRight(realX, realY);
	}
	else if (type == MOUSESCROLL)//scroll
	{
		HandleMouseScroll(scrolling);
	}
	
}

void Level3::HandleMouseLeft(int x, int y)
{
	float realX = (float)x;
	float realY = (float)y;

	//cout << "beforeboardX = " << realX << " , beforeboardY = " << realY << endl;
	//cout << "CenterCamera X = " << maincamera->GetCenter().x << " , Y = " << maincamera->GetCenter().y << endl;
	//cout << "SizeCamera X = " << maincamera->GetWindowWidth() << " , Y = " << maincamera->GetWindowHeight() << endl;
	//cout << "ZoomCamera = " << maincamera->GetZoomSize() << endl;

	int BX, BY;//board X,Y

	BX = WorldtoBoardX(realX);
	BY = WorldtoBoardY(realY);

	//cout << "BX = " << BX << " , BY = " << BY << endl;

	if (BX >= 0 && BX < BOARDSIZEROW && BY >= 0 && BY < BOARDSIZECOLUMN)
	{
		player->SetPosition(allobj[BX][BY]->GetPosition());
		//allobj[BX][BY]->SetColor(1.0, 0.0, 0.0);
	}
}
void Level3::HandleMouseRight(int x, int y)
{
	for (int i = 0; i < BOARDSIZEROW; i++)
	{
		for (int j = 0; j < BOARDSIZECOLUMN; j++)
		{
			if ((i + j) % 2 == 0)
			{
				allobj[i][j]->SetColor(0.0, 0.0, 0.0);
			}
			else
			{
				allobj[i][j]->SetColor(1.0, 1.0, 1.0);
			}
		}
	}
}
void Level3::HandleMouseMove(int x, int y)
{	
	if (y > (maincamera->GetWindowHeight()/2.0f - 50.0f))
	{
		//cout << "up" << endl;
		maincamera->CameraMove(CAMERASPEED, 0);
	}
	else if (y < -(maincamera->GetWindowHeight() / 2.0f - 50.0f))
	{
		//cout << "down" << endl;
		maincamera->CameraMove(-CAMERASPEED, 0);
	}
	if (x > (maincamera->GetWindowWidth() / 2.0f - 50.0f))
	{
		//cout << "right" << endl;
		maincamera->CameraMove(0, CAMERASPEED);
	}
	else if (x < -(maincamera->GetWindowWidth() / 2.0f - 50.0f))
	{
		//cout << "left" << endl;
		maincamera->CameraMove(0, -CAMERASPEED);
	}
}
void Level3::HandleMouseScroll(int amount)
{
	if (amount > 0) //scroll up
	{
		maincamera->CameraSetZoom(maincamera->GetZoomPercent() - 10.0f);
	}
	else if (amount < 0)//scroll down
	{
		maincamera->CameraSetZoom(maincamera->GetZoomPercent() + 10.0f);
	}
}

