#ifndef CHARACTER_H
#define CHARACTER_H

#include "Unit.h"

using namespace std;

enum classes {
	ASSAULT = 0,
	GUNNER,
	TANK,
	MEDIC,
	SCOUT,
	ENGINEER
};
class Character : public Unit {
private:
	int oxygen;
	int LOSin;	//LOS = line of sight
	int LOSout;
	int movecost;
	int atkcost;
	int lifeleft = 1;
	int downturn = 0;
	bool isDown = false;
	string skillicon;
	GameObject* miniO2bar[2];
public:

	void Init(string name, int hp, int maxhp, int atk, int atkdis, int move, int x, int y, int eva, int def, int cd, string tex,
		int oxygen, int losin, int losout);

	vector<glm::vec2> ActiveSkill(int** typearr, int sizex, int sizey);
	void skillApply(int select);

	int getHp();
	int getMaxHp();
	int getAtk();
	int getAtkcost();
	int getMovecost();

	int getLifeLeft();
	void setLifeLeft(int life);
	bool getisDown();
	void setisDown(bool fact);
	int getDownTurn();
	void setDownTurn(int turn);

	void setAtkcost(int amount);
	void setMovecost(int amount);
	int getOxygen();
	void setOxygen(int oxy);
	int getLOSin();
	int getLOSout();
	void setLOS(int losnin, int losout);
	void PassTurn();
	void readCharacterData(string filename,int index);

	GameObject* getminiO2bar(int index);

	virtual void getBuff();
	virtual void updateMiniHpbar();
	virtual void SetPosition(glm::vec3 newPosition);

	Character(string name, int hp, int maxhp, int atk, int atkdis, int move, int x, int y, int eva, int def, int cd, string tex,
		int oxygen, int losin, int losout);
	Character();
};
#endif